﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gold : MonoBehaviour
{
    public int _gold;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _gold = staticinfo.Instance.getGold();
        text.text = _gold.ToString();
    }
}
