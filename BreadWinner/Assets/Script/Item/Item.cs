﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
   public enum Type
    {

    Ammo, Coin , Heart , Weapon ,Ingredient
    }

    public Type type;
    public int value;

    public string Name;
    public string Explain;
    public string property;
    public Sprite image;
    public GameObject prefab;

    public ItemComming ItemComming;

    public float alldulability;
    public float dulability;
    public int Gold;
    public int buyGold;

}
