﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class purchase : MonoBehaviour
{
    public enum item_name {potion ,potions ,  atilWeapon, dragonWeapon, anger, comme};
    int price;


    public void Potions()
    {
        whatType(item_name.potions);
        purchaseItem(item_name.potions);
    }
    public void Potion()
    {
        whatType(item_name.potion);
        purchaseItem(item_name.potion);
    }
    public void atilWeapon()
    {
        whatType(item_name.atilWeapon);
        purchaseItem(item_name.atilWeapon);
    }
    public void dragonWeapon()
    {
        whatType(item_name.dragonWeapon);
        purchaseItem(item_name.dragonWeapon);
    }
    public void anger()
    {
        whatType(item_name.anger);
        purchaseItem(item_name.anger);
    }
    public void comme()
    {
        whatType(item_name.comme);
        purchaseItem(item_name.comme);
    }



    public void whatType(item_name _in)
    {
        switch (_in)
        {
            case item_name.potion:
                price = 100;
                break;
            case item_name.potions:
                price = 1000;
                break;
            case item_name.atilWeapon:
                price = 300;
                break;
            case item_name.dragonWeapon:
                price = 500;
                break;
            case item_name.anger:
                price = 100;
                break;
            case item_name.comme:
                price = 100;
                break;
        }
    }
    public void purchaseItem(item_name _in)
    {
        
        int hasGold = staticinfo.Instance.getGold();
        if (hasGold >= price)
        {
            hasGold -= price;
            staticinfo.Instance.setGold(hasGold);

            switch (_in)
            {
                case item_name.potion:
                    staticinfo.Instance.plusheart(1);
                    break;
                case item_name.potions:
                    staticinfo.Instance.plusheart(10);
                    break;
                case item_name.atilWeapon:
                    staticinfo.Instance.getatil();
                    break;
                case item_name.dragonWeapon:
                    staticinfo.Instance.getdrangon();
                    break;
                case item_name.anger:
                    staticinfo.Instance.anger();
                    break;
                case item_name.comme:
                    staticinfo.Instance.commendation();
                    break;
            }



        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
