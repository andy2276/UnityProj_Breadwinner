﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemDrop : MonoBehaviour
{
    public int hp = 30;

    Item item;
    public GameObject coinPrefab;
    public GameObject ingredientPrefab;

    public Sprite coinImage = null;
    public Sprite stoneImage;
    public Sprite woodImage;//원목(목재)
    public Sprite ropeImage;
 
    public Sprite treeImage; //나무
    public Sprite coalImage; //석탄
    public Sprite quartzImage; //석영

    public Sprite woolImage; //털실
    public Sprite leadImage; //납
    public Sprite copperImage; //동 , 구리

    public GameObject itemObj;

    // Start is called before the first frame update
    void Start()
    {
        item = ingredientPrefab.GetComponent<Item>();
     //   coinPrefab.SetActive(false);
       // ingredientPrefab.SetActive(false);
    }

    void setItemRandom()
    {
        int rand = Random.Range(0, 10);

        //rand = 1;
        switch (rand) {
            case 0:
                { // 코인
                    setItemInfo(Item.Type.Ingredient,"coin", coinImage, coinPrefab);
                //    coinPrefab.SetActive(true);
                    itemObj = coinPrefab;
                }
                break;
            case 1:
                { // 원목
                    setItemInfo(Item.Type.Ingredient,"wood" ,woodImage, ingredientPrefab);
                   // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 2:
                {//rope
                    setItemInfo(Item.Type.Ingredient, "rope", ropeImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 3:
                {//tree
                    setItemInfo(Item.Type.Ingredient, "tree", treeImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 4:
                {//coal
                    setItemInfo(Item.Type.Ingredient, "coal", coalImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 5:
                {//quartz
                    setItemInfo(Item.Type.Ingredient, "quartz", quartzImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 6:
                {//woolImage
                    setItemInfo(Item.Type.Ingredient, "wool", woolImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 7:
                {//lead
                    setItemInfo(Item.Type.Ingredient, "lead", leadImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 8:
                {//copper
                    setItemInfo(Item.Type.Ingredient, "copper", copperImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;
            case 9:
                {//stone
                    setItemInfo(Item.Type.Ingredient, "stone", stoneImage, ingredientPrefab);
                    // ingredientPrefab.SetActive(true);
                    itemObj = ingredientPrefab;
                    Debug.Log("setinfo");
                }
                break;

        }

    }
    void setItemInfo(Item.Type _type ,string _name ,Sprite _image , GameObject _prefab   )
    {
        item.Name = _name;
        item.image = _image;
        item.prefab = _prefab;
        item.type = _type;
    }

    //드랍 명령이 들어오면, 아이템 정보를 랜덤으로 설정해서 객체를 만든다. 
    public void Drop(Vector3 _pos,bool isRandom=true)
    {
        if (isRandom)
        {
            setItemRandom();
            // itemObj.transform.position = new Vector3(_pos.x, _pos.y, _pos.z);
            Instantiate(itemObj, new Vector3(_pos.x, _pos.y, _pos.z), Quaternion.identity);

     //       Debug.Log(itemObj.transform.position.y);
        }
        else
        {

        }
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            hp -= 10;
            Drop(new Vector3(0, 1, 0));
        }
        if (hp <= 0)
        {
            
        }
    }
}
