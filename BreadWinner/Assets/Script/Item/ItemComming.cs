﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComming : MonoBehaviour
{
    public bool isComing;

    public GameObject player;

    public float movePower = 0;
    private float DisappearTime = 0f;
    // Start is called before the first frame update

    public void Comming()
    {
        if (isComing == true)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            movePower += 0.1f;
            transform.position = Vector3.Lerp(transform.position, player.transform.position, movePower);
            gameObject.GetComponent<Collider>().isTrigger = true;
        }
        else
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            movePower = 0f;
            gameObject.GetComponent<Collider>().isTrigger = false;
        }
    }

    public void Disappear()
    {
        if (gameObject.GetComponent<Item>().type == Item.Type.Ingredient)
        {
            DisappearTime += Time.deltaTime;
            if (DisappearTime > 10f)
            {
                Destroy(gameObject);
            }
        }
        //else if (gameObject.GetComponent<Item>().type == Item.Type.Coin)
    }
    // Update is called once per frame
    void Update()
    {
        Comming();
        Disappear();
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.tag == "item")
        {
            Debug.Log("item");
        }
    }
}

