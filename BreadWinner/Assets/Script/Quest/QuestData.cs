﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestData 
{
    public string questName;
    public int[] npcID; //그 퀘스트와 연관되어있는 NPC ID를 저장하는 INT 배열. 

    public QuestData(string name, int[] npc)
    {
        questName = name;
        npcID = npc;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
