﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkManager : MonoBehaviour
{
    //key , value 
    Dictionary<int, string[]> talkData;

    private void Awake()
    {
        talkData = new Dictionary<int, string[]>();
        GenerateData();
    }

    void GenerateData()
    {
        //  talkData.Add(1000, new string[] { "오호!", "자네가 우리마을을 구할 용사인가?" });
        //  talkData.Add(100, new string[] { "판때기다" });
        //  talkData.Add(200, new string[] { "흥미롭지 않다." });
        //  talkData.Add(2000, new string[] { "혹시 모르니까~" });


        //퀘스트 ID + npc 번호
        talkData.Add(10 + 1000, new string[] { "드디어 왔군! ",
                                               "우선 집을 하나 건설해보도록하지."});
        talkData.Add(11 + 1000, new string[] { "잘했네. 앞으로도 동일한 방법으로 건설이 가능하니 염두해두게 ",
                                               "다음에 일이생기면 다시 부르지. "});


        talkData.Add(20 + 2000, new string[] { "바쁘다 바뻐", " 주민 건물을 5개 이상 건설하세요" });

        talkData.Add(21 + 2000, new string[] { "점차 모습을 되찾아가는군요. "," 이번엔 건물을 10개 이상건설하고 찾아오세요" });

        talkData.Add(30 + 4000, new string[] { "당신이 바로 그 용사님이시군요", " 우리 마을을 되살려주실 거라고 믿어요" });

        talkData.Add(40 + 5000, new string[] { "점차 번영해 나갈 마을의 모습이 기대됩니다.", " 저도 도움이 될수 있다면 좋겠네요" });


        //        talkData.Add(20 + 5000, new string[] { " 나무를 찾았다" });


    }

    //id : 키값, talkindex : 출력할 문자열 
    public string GetTalk(int id , int talkIndex)
    {
        //대사를 모두 출력했을때 NULL 반환 
        if (talkIndex == talkData[id].Length)
            return null;
        else
            return talkData[id][talkIndex];
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
