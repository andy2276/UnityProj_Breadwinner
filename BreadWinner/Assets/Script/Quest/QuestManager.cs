﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    Dictionary<int, QuestData> questList;   //questData 클래스 멤버변수 : 퀘스트 이름, 그 퀘스트와 연관되어있는 NPC ID를 저장하는 INT 배열. 
  
    public int questID; //지금 진행중인 퀘스트의 아이디 
    public int questActionIndex; //퀘스트하나의 npc 대화 순서 강제하는 인덱스 .

    int mayorQuestID;
    int administQuestId;
    int citizen01ID;
    int citizen02ID;



    public GameObject[] questObject;

    private void Awake()
    {
        questList = new Dictionary<int, QuestData>();
        GenerateData();
        questID = 10;
        mayorQuestID = 10;
        administQuestId = 20;
         citizen01ID = 30;
         citizen02ID = 40;
    }
    void GenerateData()
    { //QuestData : 퀘스트 id, 퀘스트 해당하는 npc 이름 
        questList.Add(10, new QuestData("마을사람과 대화하기-순서대로", new int[] { 1000, 1000 }));

        questList.Add(20, new QuestData("나무로 만든 칼 제작하기", new int[] { 2000, 2000 }));

        questList.Add(30, new QuestData("시민과 대화01", new int[] { 4000 }));

        questList.Add(40, new QuestData("시민과 대화02", new int[] { 5000 }));

    }
    //npc id를 받고 퀘스트 번호를 반환하는 함수 
    //npcid를 알려주면, questid에는 리스트중 몇번째 퀘스트인지를 저장한다. 
    public int GetQuestTalkIndex(int npcId)
    {
        switch (npcId)
        {
            case 1000:
                {
                    questID = mayorQuestID;
                }
                break;
            case 2000:
                {
                    questID = administQuestId;
                }
                break;
            case 4000:
                {
                    questID = citizen01ID;
                }
                break;
            case 5000:
                {
                    questID = citizen02ID;
                }
                break;
            default:
                break;
        }
        //그리고 퀘스트 아이디를 반환한다. 
        return questID;

        //return questID + questActionIndex;
        //퀘스트 아이디 + 퀘스트 대화 순서 = 퀘스트 대화 ID ; 
    }
    //퀘스트 대화 순서를 올리는 함수 
    public void checkQuest(int id)
    {

        nextQuest(id);

      //  // 모든 대사묶음을 출력했고, 해당NPC가 해당퀘스트의 진행순서가 맞았을때 다음퀘스트진행을 할수있다. 
      //  if (id == questList[questID].npcID[questActionIndex])
      //  {
      //      questActionIndex++; //리스트중 몇번째 npc와 대화중인지를 저장하는 변수 (npc의 값이 아닌 몇번째인지)

      //  }
      //  // quest obj control - ex) on , off 
      ////  controlObject();


      //  //대화의 끝에 도달하였을때 다음 퀘스트가 실행되도록 한다. 
      //  if (questActionIndex == questList[questID].npcID.Length) //마지막 npc까지 대화를 했을경우 대화종료 
      //  {
      //      nextQuest(id);
      //  }

    //    return questList[questID].questName;
    }
    
    void nextQuest(int id)
    {
        questID += 1; //퀘스트 아이디는 10, 20,30 ...
                       // questID += 10; //퀘스트 아이디는 10, 20,30 ...

        questActionIndex = 0; //0번의 npc 부터 대화하도록 초기화

        switch (id)
        {
            case 1000:
                {
                    mayorQuestID = questID;
                }break;
            case 2000:
                {
                    administQuestId = questID;
                }break;
            case 4000:
                {
                    citizen01ID = questID;
                }
                break;
            case 5000:
                {
                    citizen02ID = questID;
                }
                break;

            default:
                break;
        }
    }

    //void controlObject()
    //{
    //    switch (questID) {
    //        case 10:
    //            if(questActionIndex == 2)
    //            {
    //                questObject[0].SetActive(true);
    //            }
    //            break;
    //        case 20:
    //            if(questActionIndex ==1)
    //            {
    //                questObject[0].SetActive(false);
    //            }
    //            break;
    //    }

    //}


}
