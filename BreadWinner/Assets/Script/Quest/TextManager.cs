﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    public QuestManager questManager;
    public TalkManager talkManager;

    public GameObject talkPanel;
    public Text talkText;
    GameObject npc;

    public bool isAction;
   
    public int talkIndex; 



    void Start()
    {

        if(talkPanel != null)
        {
            talkPanel.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
