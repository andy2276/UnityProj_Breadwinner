﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement3D : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 5.0f;
    [SerializeField]
    private float jumpForce = 3.0f;
    private float gravity = -9.81f;
    private Vector3 moveDirection;

    [SerializeField]
    private Transform cameraTransform;
    private CharacterController CharacterController;

    private void Awake()
    {
        CharacterController = GetComponent<CharacterController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(CharacterController.isGrounded == false)
        {
            Debug.Log("땅0");

            moveDirection.y += gravity * Time.deltaTime;
        }
        else
        {
            Debug.Log("땅x");
        }

        CharacterController.Move(moveDirection * moveSpeed * Time.deltaTime);
    }

    public void moveTo(Vector3 direction)
    {
        //중력적용할때 y축값이 변경되지 않기위해 하단의 코드를 사용 
        //moveDirection = direction;

        //카메라 적용 전 
        // moveDirection = new Vector3(direction.x, moveDirection.y, direction.z);

        //카메라 적용 후 
        Vector3 movedis = cameraTransform.rotation * direction;
        moveDirection = new Vector3(movedis.x, moveDirection.y, movedis.z);

    }

    public void JumpTo()
    {

        Debug.Log(CharacterController.isGrounded);
        if (CharacterController.isGrounded == true)
        {
            moveDirection.y = jumpForce;
        }
        else
        {
            Debug.Log("점프안됨");

        }
    }
}
