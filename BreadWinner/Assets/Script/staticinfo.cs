﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class staticinfo : MonoBehaviour
{
   
    private static staticinfo instance = null;
    private int gold = 1000;
    private int heart = 0;
    private int playerHp = 300;


    private bool hasAnger= false;
    private bool hascommendation= false;

    private bool drangon = false;
    private bool atil = false;

    void Awake()
    {
        if (null == instance)
        {
            instance = this;
              DontDestroyOnLoad(this.gameObject);
        }
        else
        {
             Destroy(this.gameObject);
        }
    }

    public int getGold() { return gold;   }
    public void setGold(int _gold) {  gold = _gold;  }

    public int getheart() { return heart; }
    public void setheart(int _in) { heart = _in; }
    public void plusheart(int _in) { heart += _in; }

    public int getplayerHp() { return playerHp; }
    public void setplayerHp(int _in) { playerHp += _in; }

    public void anger() { hasAnger = true; }
    public void commendation() { hascommendation = true; }

    public bool getAnger() { return hasAnger; }
    public bool getcommendation() { return hascommendation; }

    public void getdrangon() { drangon = true; }
    public void getatil() { atil = true; }


    public static staticinfo Instance
    {
        get
        {
            if (null == instance)
            {
                return null;
            }
            return instance;
        }
    }
}
