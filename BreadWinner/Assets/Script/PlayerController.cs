﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private KeyCode jumpKeyCode = KeyCode.Space;
    [SerializeField]
    private CameraController cameraController;
    private Movement3D movement3D;

    private Animator animator;

    public TextManager textManager;
    GameObject scanObj;

    private void Awake()
    {
        movement3D = GetComponent<Movement3D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        RaycastHit hit;
        //  bool = 

        //  Debug.DrawRay(transform.position, transform.forward, Color.red, Mathf.Infinity);

        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, LayerMask.GetMask("NPC")))
        {
            scanObj = hit.collider.gameObject;
            //   Debug.Log(name + "충돌");
        }
        else
        {
            //   Debug.Log(name + "비충돌");
            scanObj = null;
        }
    }

    void Update()
    {
        //스페이스바 입력시 y축상향 
        if (Input.GetKeyDown(jumpKeyCode))
        {
            movement3D.JumpTo();
            if (scanObj != null)
            {

       //         textManager.Action(scanObj);
            }
        }


        if (!textManager.isAction)
        {

            float x = Input.GetAxisRaw("Horizontal");
            float z = Input.GetAxisRaw("Vertical");

            movement3D.moveTo(new Vector3(x, 0, z));

        
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            cameraController.RotateTo(mouseX, mouseY);


            if (Input.GetKeyDown(KeyCode.I))
            {
                // animator.Play("Idle");
                animator.SetFloat("moveSpeed", 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                animator.Play("run");
                animator.SetFloat("moveSpeed", 5.0f);

            }
        }
        else
        {

        }



    }
}
