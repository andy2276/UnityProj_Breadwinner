﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public bool isAction;
    int talkIndex;

    //컴포넌트--------------------------
    public QuestManager questManager;
    public KeyManager keyManager;
    public TalkManager talkManager;

    //필요한 public 오브젝트 ------------
    public GameObject talkPanel;
    public Text talkText;
    public GameObject scanObj;

    public Text nameText;

    RaycastHit hitInfo;

    GameObject npc;

    // text image ui 활성화 시켜준다. 
    // 충돌한 오브젝트를 받아서 talk함수를 호출한다.
    public void Action(GameObject _npc)
    {
       npc = _npc;
       ObjData objData = npc.GetComponent<ObjData>();
       Talk(objData.id, objData.isNPC);
       // talkText.text = "이것의 이름은" + npc.name + "이라고 한다.";
                  
        talkPanel.SetActive(isAction);
        nameText.text = _npc.name;
    }
    void Talk(int id, bool isNpc)
    {

        //NPC가 가진 퀘스트 리스트중 현재 몇번째 퀘스트인지를 받아온후 , 
        //출력해야할 대사를 받아온다. 
        int questTalkIndex = questManager.GetQuestTalkIndex(id);
        string talkData = talkManager.GetTalk(questTalkIndex+ id, talkIndex);

//        string talkData = talkManager.GetTalk(id + questTalkIndex , talkIndex);

        //대사를 모두 출력했을 경우 , 인덱스를 증가시킨다
        if (talkData == null)
        {
            isAction = false;
            talkIndex = 0;
            //questManager.checkQuest(id); //퀘스트 대화가 끝나면, 인덱스를 증가시킨다
            return;
        }

        if (isNpc)
        {
            talkText.text = talkData;
        }
        //else
        //{
        //    talkText.text = talkData;
        //}

        isAction = true;
        talkIndex++;
    }

    void Start()
    {
     // DontDestroyOnLoad(this);
        isAction = false;
        if(talkPanel != null)
        {
            talkPanel.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        TryAction();
    }

    private void TryAction()
    {
        //1. 현재 마우스 포지션을 받아와서 
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //2. 물체와 충돌감지 , 
        if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
        {
            //3. 그 물체가 npc라면, 
            if (hitInfo.transform.CompareTag("npc"))
            {
                //4. 그리고 마우스왼쪽버튼을 클릭했다면 
                if (Input.GetButtonDown("Fire1"))
                {
                    Action(hitInfo.transform.gameObject);
                   
                }
            }
            else
            {

            }

        }
    }
}
