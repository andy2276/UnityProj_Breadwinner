﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Shop : MonoBehaviour
{
    public Inventory inventory;
    public Item testItem;
    int Gold;
    enum Shop_weapon { NAME01, NAME02};
    enum shop_type { store, weapon ,skill};
    public GameObject obj;
    ObjData _objData;

    public bool store;
    public bool weapon;
    public bool skill;

    public bool smith;
    public bool general;
    public bool merchant;

    public bool shopOpen;
    public Image image;
    //퀘스트가 끝났으면 -> shopOpen = true ;
    // 캐릭터를 클릭하면 해당 ui true 로 바꿔주기, 

    RaycastHit hitInfo;
    // Start is called before the first frame update
    void Start()
    {
        shopOpen = false;
        image.gameObject.SetActive(false);

        //      _objData = obj.GetComponent<ObjData>();
        //       _objData.id;
    }
    public void setOpenShop() {
        image.gameObject.SetActive(true);
    }
    public void setcloseShop()
    {
        if (shopOpen)
        {
            image.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
            {
                if (hitInfo.transform.CompareTag("npc"))
                {
                    if (store && hitInfo.transform.name == "상인")
                    {
                        setOpenShop();
                    }
                    else if (weapon && hitInfo.transform.name == "대장장이")
                    {
                        setOpenShop();
                    }
                    else if (skill && hitInfo.transform.name == "장군")
                    {
                        setOpenShop();
                    }
                    Debug.Log("name is : " + hitInfo.transform.name);
                    //    if((store && merchant)||(weapon && smith)||(skill && general))
                    //    {

                    //    Debug.Log("name is : "+ hitInfo.transform.name);
                    //    }
                    //}
                }
            }
        }
        
//    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
        //{
        //    Debug.Log("point");
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        if (hitInfo.transform.CompareTag("npc"))
        //        {
        //            Debug.Log("shopopen");

        //            setOpenShop();
        //        }
        //    }
        //}


        //if (shopOpen)
        //{
        //    image.gameObject.SetActive(true);
        //}
        //else
        //{
        //    shopOpen = false;
        //    image.gameObject.SetActive(false);
        //}
    }
    void canPurchase(Shop_weapon type)
    {
        if (weapon)
        {
            if (type == Shop_weapon.NAME01)
            {
                if (Gold >= 200)
                {
                    Gold -= 200;
                }
            }
            else if (type == Shop_weapon.NAME02)
            {
                if (Gold >= 300)
                {
                    Gold -= 300;
                }
            }
        }
       else if (skill)
        {

        }
        else if (store)
        {

        }
    }

    public void purchase()
    {
        inventory.Acquireitem(testItem, 1);
    }

}
