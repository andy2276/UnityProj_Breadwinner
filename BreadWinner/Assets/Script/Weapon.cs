﻿using System.Collections;
using System.Collections.Generic;
using DamageSystem;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum Type { Melee , Range };

    public Type type;
    public int damage; // 데미지 
    public float rate; // 공속
    public BoxCollider meleeArea;//공격범위 
    public TrailRenderer traileffect;
    //  지민추가
    [SerializeField] DamageSystem.Attacker m_attacker = null;

    public Attacker Attacker { get => m_attacker; set => m_attacker = value; }

    public void Use()
    {
        if(type == Type.Melee)
        {
            StopCoroutine("Swing");
            StartCoroutine("Swing");
//            Swing();
        }
    }
   IEnumerator Swing()
    {
        yield return new WaitForSeconds(0.1f); //0.1초 대기 
        if(m_attacker != null)
        {
            m_attacker.AttackerStart();
        }
        meleeArea.enabled = true;
        traileffect.enabled = true;

        yield return new WaitForSeconds(0.3f); //0.1초 대기 
        meleeArea.enabled = false;
        
        yield return new WaitForSeconds(0.3f); //0.1초 대기 
        traileffect.enabled = false;
        if (m_attacker != null)
        {
            m_attacker.AttackerEnd();
        }

        //1 
        //  yield return null; //1프레임 대기 
        //2     
        //yield return new WaitForSeconds(0.1); //0.1초 대기 

        //yield break ; // 코루틴 탈출 
        //yield 결과를 내는 키워드 
    }
    //use() <- 메인루틴 
    //swing() : 서브루틴
    //

    //코루틴이면
    //use와 swing이 한번에 실행된다. 
}
