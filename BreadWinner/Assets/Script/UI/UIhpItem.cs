﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIhpItem : MonoBehaviour
{
    public hpbar _hpbar;
    public Inventory inventory;


    public Image hpItemImage;
    public Text TextCount;
    int count;

    bool kDown;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void inputKey()
    {
        kDown = Input.GetKeyDown(KeyCode.K);

        //if (Input.GetKeyDown(KeyCode.K))
        //{
        //    kDown = Input.GetKeyDown(KeyCode.K);
        //}

    }
    // Update is called once per frame
    void Update()
    {
        inputKey();

        if (inventory != null)
        {
            count = inventory.findObject(Item.Type.Heart);
        }
        else
        {
            count = 0;
        }
        
        if(count != 0)
        {
            hpItemImage.gameObject.SetActive(true);
            TextCount.text = count.ToString();

            if (kDown)
            {
                inventory.useItem(Item.Type.Heart);
               // _hpbar.hpUp(0.5f);
            }
        }
        else
        {
            TextCount.text = " ";
            hpItemImage.gameObject.SetActive(false);
        }
    }
}
