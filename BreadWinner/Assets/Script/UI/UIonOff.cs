﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIonOff : MonoBehaviour
{
    public Image hpui;
    public Image skillanger;
    public Image skillcommen;
    public Image weapon01;
    public Image weapon02;
    public Image weapon03;

    public Image talkOn;
    bool isOn;
    // Start is called before the first frame update
    void Start()
    {
        isOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (talkOn.IsActive())
        {
            isOn = true;
            hpui.gameObject.SetActive(false);
            skillanger.gameObject.SetActive(false);
            skillcommen.gameObject.SetActive(false);
            weapon01.gameObject.SetActive(false);
            weapon02.gameObject.SetActive(false);
            weapon03.gameObject.SetActive(false);

        }

        if(isOn)
        {
            if (!talkOn.IsActive())
            {
                Debug.Log("ui true");
                hpui.gameObject.SetActive(true);
                skillanger.gameObject.SetActive(true);
                skillcommen.gameObject.SetActive(true);
                weapon01.gameObject.SetActive(true);
                weapon02.gameObject.SetActive(true);
                weapon03.gameObject.SetActive(true);
            }
            isOn = false;
        }
    }
}
