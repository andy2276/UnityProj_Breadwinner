﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Slot: MonoBehaviour
{
    public Item item;
    public int itemCount; //획득한 아이템 개수 
    public Image itemImage; //아이템 이미지

    public Text text_Count;
    public GameObject Image_Count;

    //image==========================================
    enum Ingredient { coin, stone, wood, rope,tree , coal, quartz, wool ,lead, copper };
    public Sprite coinImage = null;
    public Sprite stoneImage;
    public Sprite woodImage;//원목(목재)
    public Sprite ropeImage;

    public Sprite treeImage; //나무
    public Sprite coalImage; //석탄
    public Sprite quartzImage; //석영

    public Sprite woolImage; //털실
    public Sprite leadImage; //납
    public Sprite copperImage; //동 , 구리

    public Sprite slotImage;


    //  public ItemPickUp itemPickUp;
    //  Item saveitem;
    private void Start()
    {
       //saveitem = new Item();
    }


    private void setColor(float alpha)
    {
        Color color = itemImage.color;
        color.a = alpha;
        itemImage.color = color;
    }

    // 아이템을 처음에 얻을때 사용해주는 함수 
    public void AddItem(Item _item , int _count = 1)
    {
        //테스트용
        if (_item == null)
        {
            itemCount = _count;
            text_Count.text = "12";
            Image_Count.SetActive(true);
        }
        else
        {
            Debug.Log("item : " + _item.name);
            item = _item;
            itemCount = _count;
            itemImage.sprite = item.image;

            //장비를 얻었을때는 숫자를 출력하지 않게
            if (_item.type != Item.Type.Weapon)
            {
                Image_Count.SetActive(true);
                text_Count.text = itemCount.ToString();

            }
            else
            {
                text_Count.text = _count.ToString();
                Image_Count.SetActive(false);


            }
        }
        setColor(1);
    }

    public void AddItem(string _name, int _count = 1)
    {
        if(_name == "wood")
        {
            slotImage = woodImage;
        }
        else if(_name == "coin")
        {
            slotImage = coinImage ;
        }
        else if (_name == "stone")
        {
            slotImage = stoneImage;
        }
        else if (_name == "rope")
        {
            slotImage = ropeImage;
        }
        else if (_name == "tree")
        {
            slotImage = treeImage;
        }
        else if (_name == "coal")
        {
            slotImage = coalImage;
        }
        else if (_name == "quartz")
        {
            slotImage = quartzImage;
        }
        else if (_name == "wool")
        {
            slotImage = woolImage;
        }
        else if (_name == "lead")
        {
            slotImage = leadImage;
        }
        else if (_name == "copper")
        {
            slotImage = copperImage;
        }
        // item = new Item();
        itemCount = _count;

        itemImage.sprite = slotImage;
        text_Count.text = "0";
        Image_Count.SetActive(false);
            setColor(1);
    }



    public void useItem(int count=1)
    {
        itemCount -= count;
        text_Count.text = itemCount.ToString();
        if (itemCount <= 0)
        {
            clearSlot();


        }

    }



    //이미 슬롯에 아이템이 추가되어있을경우, 개수만 증가시켜주는함수.
    public void setSlotCount(int _count)
    {
        itemCount += _count;
        text_Count.text = itemCount.ToString();

        if (itemCount <= 0)
            clearSlot();
      
    }


    public void clearSlot()
    {
        item = null;
        itemCount = 0;
        itemImage.sprite = null;
        setColor(0);

        text_Count.text = "0";
        Image_Count.SetActive(false);
      

    }





}
