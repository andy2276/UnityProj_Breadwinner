﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ItemPickUp : MonoBehaviour
{
    RaycastHit hitInfo;

    public Text PickUpText;
    public int itemSlotCount;

    public Camera cam;

    public LayerMask layerMask;
    public float range = 15;
    void Update()
    {
        TryAction();
        CheckItem();       
    }

    private void TryAction()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray.origin,ray.direction,out hitInfo))
        {
            if (hitInfo.transform.CompareTag("item"))
            {
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    if(itemSlotCount < 20)
                    {
                       // hitInfo.transform.gameObject.GetComponent<ItemComming>().isComing = true;
                    }
                } 
            }

        }
    }


    private void CheckItem()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
        {
           
            if (hitInfo.transform.CompareTag("item"))
            {
                
                ItemInfoAppear();
            }
            else
            {
                InfoDisappear();
            }

        }

    }
        



private void ItemInfoAppear()
    {
       
        PickUpText.gameObject.SetActive(true);
        PickUpText.text = "<" + hitInfo.transform.GetComponent<Item>().Name + ">" + " 획득 " + "[Z]";
    }

    private void InfoDisappear()
    {
        PickUpText.gameObject.SetActive(false);
    }


}
