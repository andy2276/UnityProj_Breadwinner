﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public bool isActive = false;

    [SerializeField] private GameObject inventoryBase;
    [SerializeField] private GameObject slotParent;
    [SerializeField] private GameObject m_slotPref;
    [SerializeField] private GameObject m_ingredient = null;

    [SerializeField] private GameObject m_itemPrefab = null;
    public Slot[] slots;

    //  지민추가
    Dictionary<string, int> m_dicBattleStorage = new Dictionary<string, int>();

    int itemCountMax = 10000;

    public Dictionary<string, int> BattleStorage { get => m_dicBattleStorage; }
    public GameObject SlotParent { get => slotParent; set => slotParent = value; }
    public GameObject SlotPref { get => m_slotPref; set => m_slotPref = value; }
    public GameObject Ingredient { get => m_ingredient; set => m_ingredient = value; }

    // Start is called before the first frame update
    private void Start()
    {
 //     DontDestroyOnLoad(gameObject);
        slots = slotParent.GetComponentsInChildren<Slot>();
        //  slots[0].AddItem(null);
        // slots[1].AddItem(null);
        inventoryBase.SetActive(false);
        InventorySaver.LoadInventory(this);
    }
    void Update()
    {
        TryOpenInventory();
    }
    public void TryOpenInventory()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("i input");
            if (isActive == false)
            {
                Debug.Log("inventory true");
                isActive = true;
                OpenInventory();
            }
            else if (isActive == true)
            {
                isActive = false;
                closeInventory();
            }
        }
    }

    private void OpenInventory()
    {
        inventoryBase.SetActive(true);
    }
    private void closeInventory()
    {
        inventoryBase.SetActive(false);
    }

    public int findObject(Item.Type type)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item != null)
            {
                if (slots[i].item.type == type)
                {
                    return slots[i].itemCount;
                }
            }
        }

        return 0;
    }
    //  지민추가
    public int FindItem(string _name)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item != null)
            {
                if (slots[i].item.Name == _name)
                {
                    return slots[i].itemCount;
                }
            }
        }
        return 0;
    }
    public void AddItemCnt(string _name,int _calcul)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item != null)
            {
                if (slots[i].item.Name == _name)
                {
                    slots[i].itemCount += _calcul;
                    if(slots[i].itemCount < 0)
                    {
                        slots[i].itemCount = 0;
                    }
                    break;
                }
            }
        }
    }
    public void AddItemCnt(string _name,Slot _s)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item != null )
            {
                if (slots[i].item.Name == _name)
                {
                    slots[i].itemCount += _s.itemCount;
                    if (slots[i].itemCount < 0)
                    {
                        slots[i].itemCount = 0;
                    }
                    return;
                }
            }
        }
        for (int i = 0; i < slots.Length; i++)
        {
            if(slots[i].item == null)
            {
                slots[i].item = _s.item;
                slots[i].itemCount = _s.itemCount;
                slots[i].itemImage = _s.itemImage;
                return;
            }
        }
    }
    public void IsFirstItem(Item _tem)
    {
        for(int i = 0; i < slots.Length;++i)
        {
            if(slots[i].item != null && slots[i].item.Name == _tem.Name)
            {
                Destroy(_tem.gameObject);
                return;
            }
        }
        for (int i = 0; i < slots.Length; ++i)
        {
            if(slots[i].item == null)
            {
                slots[i].item = _tem;
                slots[i].item.gameObject.SetActive(false);
                return;
            }
        }
    }
    public void StoreItem(string _name,int _cnt = 1)
    {
        if( m_dicBattleStorage.ContainsKey(_name))
        {
            m_dicBattleStorage[_name] += _cnt;
        }
        else
        {
            m_dicBattleStorage.Add(_name, _cnt);
        }
    }
    public void RewardItem(float _scale)
    {
        string type = "";
        for(int i = 0; i < InventorySaver.ArrItemName.Length; ++i)
        {
            type = InventorySaver.ArrItemName[i];
            if (m_dicBattleStorage.ContainsKey(type))
            {
                float f = m_dicBattleStorage[type];
                f *= _scale;
                m_dicBattleStorage[type] = (int)f;
                AddItemCnt(type, m_dicBattleStorage[type]);
            }
        }
    }
    public void RewardClear()
    {
        m_dicBattleStorage.Clear();
    }
    public void CreateItem(string _name,int _cnt)
    {
        for(int i = 0; i <  slots.Length; ++i)
        {
            if(slots[i].item == null)
            {
                GameObject obj = Instantiate(m_itemPrefab);
                slots[i].item = obj.GetComponent<Item>();
            }

        }
    }



    public void useItem(Item.Type type,int count = 1)
    {

        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item != null)
            {
                if (slots[i].item.type == type)
                {
                    slots[i].useItem(count);
                }
            }
        }

    }
    //이름으로 획득
    public void Acquireitem(string _name, int _count)
    {
        for (int i = 0; i < slots.Length; i++)
        {

            if (slots[i].item != null)
            {

                if (slots[i].item.type != Item.Type.Weapon)
                {

                    if (slots[i].item.Name == _name)
                    {

                        if (slots[i].itemCount < itemCountMax)
                        {
                            slots[i].setSlotCount(_count);

                            return;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < slots.Length; i++)
        {
           
            if (slots[i].item == null)
            {
                slots[i].AddItem(_name, _count);
                return;
            }
        }
    }
    public void Acquireitem(Item _item , int _count)
    {
        Debug.Log(slots.Length);
        for (int i = 0; i< slots.Length; i++)
        {
            
            if (slots[i].item != null)
            {

                if (slots[i].item.type != Item.Type.Weapon)
                {
                  
                    if (slots[i].item.Name == _item.Name)
                    {
                      
                        if (slots[i].itemCount < itemCountMax)
                        {
                       
                            slots[i].setSlotCount(_count);
                       
                            return;
                        }
                    }
                }
            }
        }

        for(int i = 0; i < slots.Length; i++)
        {
            if (slots[i].item == null)
            {
                slots[i].AddItem(_item, _count);
                return;
            }
        }
    }
    void OnDestroy()
    {
        InventorySaver.SaveInventory(this);
    }
   

}
