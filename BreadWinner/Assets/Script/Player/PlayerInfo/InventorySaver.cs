﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class InventorySaver : MonoBehaviour
{
    #region StaticFunc
    static string m_strPath = "Assets/Script/Player/PlayerInfo/";
    static string m_strFileName = "InventorySave.txt";
    static string[] m_arrItemName =
    {
         "coin"
        ,"wood"
        ,"rope"
        ,"tree"
        ,"coal"
        ,"quartz"
        ,"wool"
        ,"lead"
        ,"copper"
        ,"stone"
    };
    //  파일형식 [자원이름](자원개수)
    static Dictionary<string, int> m_dicItems = new Dictionary<string, int>();
    static InventorySaver m_Inst = null;

    public static string[] ArrItemName { get => m_arrItemName; set => m_arrItemName = value; }

    static void WriteItem(string _line,string _name,int _cnt)
    {
        _line += "[" + _name + "](" + _cnt.ToString() + ")";
    }

    static void SaveItem(string _line, string _name,int _cnt)
    {
        for (int i = 0; i < m_arrItemName.Length;++i)
        {
            if(_name == m_arrItemName[i])
            {
                WriteItem(_line, m_arrItemName[i], _cnt);
                break;
            }
        }
       
    }
    static public void SaveInventory(Inventory _inven)
    {
        string fullPath = m_strPath + m_strFileName;
        FileStream f = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        StreamWriter w = new StreamWriter(f, System.Text.Encoding.Unicode);
        string t = "";
        bool itemAllNull = false;
        foreach(var i in _inven.slots)
        {
            if(i.item == null)
            {
                itemAllNull = true;
            }
        }

        for (int i = 0; i <  _inven.slots.Length; ++i)
        {
            if(_inven.slots[i].itemCount > 0 && _inven.slots[i].item.Name != "")
            {
              //  SaveItem(t, _inven.slots[i].item.Name, _inven.slots[i].itemCount);
                t += "[" + _inven.slots[i].item.Name + "](" + _inven.slots[i].itemCount.ToString() + ")";
                w.WriteLine(t);
                t = "";
            }
        }
        if(t == "")
        w.Flush();
        w.Close();
        f.Close();
    }
    static public void LoadInventory(Inventory _inven)
    {
        string fullPath = m_strPath + m_strFileName;
        FileInfo f = new FileInfo(fullPath);

        string[] v;
        string num = "";
        string resType = "";
        bool isType = false;
        bool isNum = false;

        if(f.Exists)
        {
            v = File.ReadAllLines(fullPath);
            if(v.Length > 0)
            {
                for(int type = 0; type < v.Length; ++type)
                {
                    #region Check
                    for (int c = 0; c < v[type].Length;++c)
                    {
                        if(isType)
                        {
                            if(v[type][c] == ']')
                            {
                                isType = false;
                                continue;
                            }
                            resType += v[type][c];
                        }
                        else if(isNum)
                        {
                            if(v[type][c] == ')')
                            {
                                isNum = false;
                                continue;
                            }
                            num = num + v[type][c];
                        }
                        else if(v[type][c] == '[')
                        {
                            isType = true;
                            continue;
                        }
                        else if(v[type][c] == '(')
                        {
                            isNum = true;
                            continue;
                        }
                    }
                    #endregion
                    #region Store
                    if (m_dicItems.ContainsKey(resType))
                    {
                        m_dicItems[resType] += int.Parse(num);
                      
                    }
                    else
                    {
                        m_dicItems.Add(resType, int.Parse(num));
                       
                    }
                    #endregion
                    #region StoreToInventory
                    if(m_dicItems.ContainsKey(resType))
                    {
                        int cnt = m_dicItems[resType];
                        //  여기도 바꿔야함
                        GameObject obj = Instantiate(_inven.Ingredient);
                        Item tem = obj.GetComponent<Item>();
                        if (tem == null)
                        {
                            return;
                        }
                        tem.Name = resType;

                        tem.type = Item.Type.Ingredient;
                        _inven.Acquireitem(tem, cnt);
                        obj.SetActive(false);
                    }
                    resType = "";
                    num = "";
                    #endregion
                }
            }
        }
    }

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
