﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Player : BreadWinner
{
   
    
    //입력 키 값 받는 변수----------- 
    float   hAxis;
    float   vAxis;
    bool    walkDown;
    bool    jumpDown;
    bool    fireDown;   //공격 키 
    bool    getDown;    //아이템 습득 키 
    bool    swap01_q;   //아이템 교체 키
    bool    swap02_w;
    bool    swap03_e;

    bool isAnger;
    bool isCommend;

    public GameObject skillAnger;
    public GameObject skillCommend;


    //  지민 추가
    [SerializeField] bool m_bIsVillage = true;
    float m_fMouseX = 0;
    Vector3 m_v3TargetPos = new Vector3();
    float jumpPower;
    bool isJump;

    public float speed;    //속도
    Vector3 moveVec; //움직임 벡터 

    //공격 관련 변수 -------------------------------

    float fireDelay; //공격 딜레이
    bool isFireReady = true; //공격가능하다. 

    //무기 관련 변수 --------------------------------
    GameObject nearObject;
    Weapon equipWeapon; //플레이어가 현재 장착한 무기 
    int equipWeaponIndex = -1 ; //현재 장착한 무기 인덱스

    public GameObject[] weapons; //플레이어가 소지한 무기 배열
    public bool[] hasWeapons;

    //컴포넌트 --------------------------------------
    Animator anim; //애니메이터 컴포넌트 
    Rigidbody rigid;
    public GameManager gameMgr;
    //아이템--------------------------------------
    public int coin; //게임 처음 시작시 가지고있는 초기화 값 
    public int hpPotion;//게임 처음 시작시 가지고있는 초기화 값 

    public int items;//게임 처음 시작시 가지고있는 초기화 값 

    public int coinMax;
    public int healthMax;
    public int itemsMax;

    //인벤토리---------------
    public Inventory inventory;
    public ItemPickUp itemPickUp;

    //  지민추가
    [SerializeField] private DamageSystem.Defenser m_defencer = null;
    [SerializeField] private List<DamageSystem.Attacker> m_lstAttacker = new List<DamageSystem.Attacker>();


    void DamagedEvent(DamageSystem.DamageInput _data)
    {
        //   DamageInput 여기 안에는
        //  _data.m_fDamage , _data.m_v3AttackDir
        //  이 두가지가있어
        //  
        //  if (!m_bDamaged)
        //  {
        //      if (10 < _input.m_fDamage)
        //      {
        //          BattleAreaMgr.Inst.CreateDamageUpper(transform.position, _input.m_fDamage, Color.red);
        //      }
        //      Status.AiHp -= _input.m_fDamage;
        //      Debug.Log("Damaged!:" + _input.m_fDamage.ToString());
        //      m_bDamaged = true;
        //  }

        Debug.Log("Damaged!! : " + _data.m_fDamage.ToString());
        
    }
    void CopyTo(Player _dst)
    {

        //  공격 관련 변수 -------------------------------

        //  무기 관련 변수 --------------------------------

        //  플레이어가 소지한 무기 배열

        //  컴포넌트 --------------------------------------

        //  아이템--------------------------------------

        //  인벤토리---------------

    }

    void GetInput()
    { //키입력받아서 변수에 저장하기 
        hAxis = Input.GetAxisRaw("Horizontal");

        vAxis = Input.GetAxisRaw("Vertical");

        walkDown = Input.GetButton("Walk");

        fireDown = Input.GetButtonDown("Fire1");

        jumpDown = Input.GetButtonDown("Jump");

        getDown = Input.GetButtonDown("Interation");

        swap01_q = Input.GetButtonDown("swap1");

        swap02_w = Input.GetButtonDown("swap2");

        swap03_e = Input.GetButtonDown("swap3");


        m_fMouseX = Input.GetAxis("Mouse X");
        Debug.Log("key : " + hAxis);
    }

    void Swap()
    {
        if (swap01_q && (!hasWeapons[0] || equipWeaponIndex == 0))
            return;
        else if (swap02_w && (!hasWeapons[1] || equipWeaponIndex == 1))
            return;
        else if (swap03_e && (!hasWeapons[2] || equipWeaponIndex == 2))
            return;

        int weaponIdex = -1;
        if (swap01_q) weaponIdex = 0;
        if (swap02_w) weaponIdex = 1;
        if (swap03_e) weaponIdex = 2;

        if ((swap01_q || swap02_w || swap03_e) && !isJump)
        {
            if (equipWeapon != null)
                equipWeapon.gameObject.SetActive(false);

            equipWeaponIndex = weaponIdex;
            equipWeapon = weapons[weaponIdex].GetComponent<Weapon>();
            if(weaponIdex < m_lstAttacker.Count )
            {
                equipWeapon.Attacker = m_lstAttacker[weaponIdex];
            }
            else
            {
                equipWeapon.Attacker = m_lstAttacker[0];
            }
           
            equipWeapon.gameObject.SetActive(true);
//            weapons[weaponIdex].SetActive(true);
            
        }
    }
    //  지민추가
    void Move2()
    {
        bool bMoveAnim = false;
        float time = Time.deltaTime;
        if(Input.GetMouseButton(1))
        {
            Vector3 m = Input.mousePosition;
            Vector3 dir = (m_v3TargetPos - transform.position).normalized;
            m.z = Vector3.Distance(Camera.main.transform.position, transform.position);
            m_v3TargetPos = Camera.main.ScreenToWorldPoint(m);
            m_v3TargetPos.y = transform.position.y;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), time * speed * 0.5f);
            //   transform.LookAt(m_v3TargetPos,Vector3.up);
            bMoveAnim = true;
        }
        if(vAxis != 0 || hAxis != 0)
        {
            bMoveAnim = true;
        }
        transform.position += transform.forward * vAxis * (walkDown ? 0.3f : 1.0f) * time * speed ;
        transform.position += transform.right * hAxis * (walkDown ? 0.3f : 1.0f) * time  * speed;
        anim.SetBool("isRun", bMoveAnim);
    }
    void Move()
    {
        Debug.Log("Move:");
        //캐릭터 이동시키기
        moveVec = new Vector3(hAxis, 0, vAxis).normalized;

        if (!isFireReady)
            moveVec = Vector3.zero;
        //쉬프트가 눌렸을때 == 걷는다. -> 속도 감소시킴 
        transform.position += moveVec * speed * (walkDown ? 0.3f : 1.0f) * Time.deltaTime;

        //쉬프트를 누르면 걷는것
        anim.SetBool("isRun", moveVec != Vector3.zero);
        anim.SetBool("isWalk", walkDown);
    }
    void Turn()
    {
        //나아가는 방향을 바라본다 (= 회전한다)
        transform.LookAt(transform.position + moveVec);

    }
    void Jump()
    {
        if (jumpDown&& !isJump)
        {
            //ForceMode.Impulse 즉발적인 힘 가하기
            rigid.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
            isJump = true;
        }
    }
    //바닥에 닿았을때

    void Attack()
    {
        if (equipWeapon == null)
            return;
        fireDelay += Time.deltaTime;
        isFireReady = equipWeapon.rate < fireDelay;

        if (fireDown && isFireReady)//! isSwap 
        {
            equipWeapon.Use();
            anim.SetTrigger("DoSwing");
            fireDelay = 0; 
        }
    }

    void Interation()
    {
        if(getDown && nearObject != null& !isJump)
        {
            if(nearObject.tag == "weapon")
            {
                Item item = nearObject.GetComponent<Item>();
                int weaponIndex = item.value;
                hasWeapons[weaponIndex] = true;

                Destroy(nearObject);
            }
        }
    }

    void FreezeRotation()
    {
        rigid.angularVelocity = Vector3.zero;
    }

    private void FixedUpdate()
    {
        FreezeRotation();
    }

    private void Awake()
    {
        jumpPower = 15;
        anim = GetComponentInChildren<Animator>();
        rigid = GetComponent<Rigidbody>();
    }
  
    void Start()
    {
        isAnger = false;
        isCommend = false;

        CopyState();

      //  지민 삭제
      //  DontDestroyOnLoad(gameObject);
        //  여기 등록을 시켜줬어! 근데 방어적 프로그래밍을 해야해서!
        if(m_defencer != null)
        {
            m_defencer.DamagedFunc = DamagedEvent;
        }
        if (inventory == null)
        {
            inventory = BattleAreaMgr.Inst.Inventory;
        }
        //  이거는 원본 에셋을 건들이지 않게 하기 위한거야! 스텟을 넣어놓은게 변화하더라고, 그게 다음에도 그렇게 가더라
    }
    
    
    // Update is called once per frame
    void Update()
    {
        Debug.Log("update");
        GetInput();
        if (!gameMgr.isAction)
        {
            if (m_bIsVillage)
            {
                Move();
                Turn();
            }
            else
            {
                Move2();
            }
            Jump();
        }
      
        Swap();
        Interation();


        Attack();
        //    dir = _transform.forward;
        //  Debug.DrawRay(point1, point2, new Color(0, 1, 0));


        if (jumpDown && nearObject != null)
        {

        }

        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    staticinfo.Instance.setGold(10);
        //    coin +=10;
        //    Debug.Log(staticinfo.Instance.getGold());
        //}

        if (Input.GetKeyDown(KeyCode.M))
        {
            if (!isAnger)
            {
                if (staticinfo.Instance.getAnger())
                {
                    Debug.Log("anger");
                    isAnger = true;
                    skillAnger.gameObject.SetActive(true);
                }
            } 
        else
        {
            Debug.Log("anger no!");

            isAnger = false;
            skillAnger.gameObject.SetActive(false);

        }

        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (!isCommend)
            {
                if (staticinfo.Instance.getcommendation())
                {
                    isCommend = true;
                    skillCommend.gameObject.SetActive(true);
                }
            }
            else
            {
                isCommend = false;
                skillCommend.gameObject.SetActive(false);

            }
        }


        //   Debug.DrawRay(transform.position,transform.up * 10.0f, new Color(1, 0, 0));
    }

    private void OnTriggerStay(Collider other)
    {
      
        if (other.tag == "weapon")
        {
            nearObject = other.gameObject;

            Debug.Log(nearObject.name);
        }
    }
    private void onTriggerExit(Collider other)
    {
       
        if (other.tag == "weapon")
            nearObject = null;
    }

    void isTrigger(Collider other)
    {
       
        if (other.tag == "item")
        {
            Item item = other.GetComponent<Item>();
            switch (item.type)
            {
                case Item.Type.Ammo:

                    break;
                case Item.Type.Coin:
                    coin += item.value;
                    if (coin > coinMax)
                        coin = coinMax;
                    break;
                case Item.Type.Heart:
                    hpPotion++;
                    // health += item.value;
                //    if (health > healthMax)
                //        health = healthMax;
                    break;
                case Item.Type.Ingredient:
                    {
                        //재료일때만 인벤토리에 넣기 .
                        if(!m_bIsVillage)
                        {
                            Item tem = other.gameObject.GetComponent<Item>();
                            inventory.IsFirstItem(tem);
                            inventory.StoreItem(tem.Name);
                            tem = null;
                        }
                        else
                        {
                            inventory.Acquireitem(other.gameObject.GetComponent<Item>(), 1);
                            Destroy(other.gameObject);
                            //                        other.gameObject.SetActive(false);
                        }
                       
                    }
                    break;            

            }

           
        }
    }

    void isCollider(Collision other)
    {
        if (other.gameObject.tag == "item")
        {
            Item item = other.gameObject.GetComponent<Item>();
            switch (item.type)
            {
                case Item.Type.Ammo:

                    break;
                case Item.Type.Coin:
                    coin += item.value;
                    if (coin > coinMax)
                        coin = coinMax;
                    break;
                case Item.Type.Heart:
                    hpPotion++;
                     break;
                case Item.Type.Ingredient:
                    {
                        inventory.Acquireitem(other.gameObject.GetComponent<Item>(), 1);
                        //                        other.gameObject.SetActive(false);
                        Destroy(other.gameObject);
                    }
                    break;

            }

        }
    }


    private void OnTriggerEnter(Collider other)
    {
        isTrigger(other);


    }

    private void OnCollisionEnter(Collision collision)
    {
        isCollider(collision);
        if (getDown)
        {
            Debug.Log("collid");
        }
        if (collision.gameObject.tag == "Floor")
        {
            isJump = false;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
    
        if (collision.gameObject.tag == "npc")
        {
            nearObject = collision.gameObject;
            Debug.Log(nearObject.name);
        }
    }
}
