﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDatas
{
    public GameObject m_playerObj = null;
    public GameObject m_Inventory = null;
    public GameObject m_questInfo = null;
}
