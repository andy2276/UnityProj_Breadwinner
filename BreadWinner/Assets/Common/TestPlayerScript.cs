﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayerScript : BreadWinner
{
    [SerializeField] GameObject m_defencer = null;
    [SerializeField] GameObject m_attacker = null;
    
    DamageSystem.Attacker attack = null;
    bool m_damaged = false;
    bool m_onAttack = false;
    Vector3 m_preMousePos = new Vector3();
    Vector3 m_curMousePos = new Vector3();
    // Start is called before the first frame update
    void DamageFunc(DamageSystem.DamageInput _input)
    {
        m_damaged = true;
        g_AiState.AiHp -= _input.m_fDamage;
    }


    void Start()
    {
        m_defencer.GetComponent<DamageSystem.Defenser>().DamagedFunc = DamageFunc;
        if(m_attacker != null)
        {
            attack = m_attacker.GetComponent<DamageSystem.Attack_Near>();
            m_attacker.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(m_damaged)
        {
            Debug.Log("hit!! " + g_AiState.AiHp.ToString());
            m_damaged = false;
        }
        if (m_onAttack)
        {
            m_onAttack = false;
            attack.AttackerEnd();
            m_attacker.SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            
            if (m_attacker != null && !m_attacker.activeSelf)
            {
                Debug.Log("hitToYou!");
                m_attacker.SetActive(true);
                m_onAttack = true;
                attack.AttackerStart();
            }
        }
        float speed = Time.deltaTime * Status.AiMoveSpeed;
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * speed;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.position += transform.forward * speed* -0.5f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * speed ;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            transform.position += transform.right * speed * -1;
        }
        if(Input.GetMouseButton(1))
        {
            //m_preMousePos = m_curMousePos;
            //m_curMousePos = Input.mousePosition;
            //Vector3 c = m_curMousePos.normalized;
            //Vector3 p = m_preMousePos.normalized;
            //
            //float h = Input.GetAxis("Vertical");
            //Vector3 d = Vector3.Cross(c, p);
            //
            //if(d.y <0)
            //{
            //    transform.Rotate(0, h * speed * -1, 0);
            //}
            //else
            //{
            //    transform.Rotate(0, h * speed , 0);
            //}

            float MouseX = Input.GetAxis("Mouse X");

            transform.Rotate(Vector3.up * speed * 10 * MouseX);


        }

    }
}
