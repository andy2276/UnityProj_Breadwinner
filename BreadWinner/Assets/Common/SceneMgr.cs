﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BT.Enum;

public class SceneMgr : MonoBehaviour
{
    static SceneMgr m_Inst = null;
    static int m_nTest = 0;

    public static SceneMgr Inst
    { get
        {
            if(m_Inst == null)
            {
                m_Inst = new SceneMgr();
               
            }
            return m_Inst;
        }
    }

    private SceneDatas m_battleData = new SceneDatas();
    private PlayerDatas m_playerData = new PlayerDatas();

    public static string g_strNextScene;
    int a = 0;
    [SerializeField] Image m_loadingBar;
    public SceneDatas BattleData { get => m_battleData; set => m_battleData = value; }
    public PlayerDatas PlayerData { get => m_playerData; set => m_playerData = value; }

    // Start is called before the first frame update
    void Start()
    {
        if (m_Inst == null)
        {
            m_Inst = this;
            DontDestroyOnLoad(this);
        }
        StartCoroutine(LoadScene());
    }

    public static void LoadScene(string _scene)
    {
        g_strNextScene = _scene;
        SceneManager.LoadScene("LoadingScene");
    }
    public void InitBattleData()
    {
        m_battleData.IsSelectMap = false;
        m_battleData.Level = 0;
        m_battleData.AreaType = AreaType.End;
    }
    IEnumerator LoadScene()
    {
        yield return null;

        AsyncOperation op = SceneManager.LoadSceneAsync(g_strNextScene);
        op.allowSceneActivation = false;

        float time = 0;
        while(!op.isDone)
        {
            yield return null;

            time += Time.deltaTime;

            if(op.progress < 0.9f)
            {
                m_loadingBar.fillAmount = Mathf.Lerp(m_loadingBar.fillAmount, op.progress, time);
                if(m_loadingBar.fillAmount >= op.progress)
                {
                    time = 0;
                }


            }
            else
            {
                m_loadingBar.fillAmount = Mathf.Lerp(m_loadingBar.fillAmount, 1, time);
                if(m_loadingBar.fillAmount == 1.0f)
                {
                    op.allowSceneActivation = true;
                    yield break;
                }
            }


        }
    }
    public void IsExist()
    {
        Debug.Log(a.ToString() + "이거 존재한다!!");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
