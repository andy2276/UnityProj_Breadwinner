﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum VillageSceneType
{
    Story,Build,Portal
}

public class VillageSceneMgr : MonoBehaviour
{
    static VillageSceneMgr m_Inst = null;
    [SerializeField] private SceneDatas m_sceneData = new SceneDatas();
    [SerializeField] private GameObject m_battleUI = null;
    [SerializeField] private GameObject m_portalUI = null;

    [SerializeField] private bool m_bTest = false;
    //  0
    [SerializeField] private GameObject m_storyCam = null;
    [SerializeField] private List<GameObject> m_lstStoryObj = new List<GameObject>();
    //  1
    [SerializeField] private GameObject m_buildCam = null;
    [SerializeField] private List<GameObject> m_lstBuildObj = new List<GameObject>();
    //  2
    [SerializeField] private GameObject m_portalCam = null;
    [SerializeField] private List<GameObject> m_lstPortalObj = new List<GameObject>();

    [SerializeField] private VillageSceneType m_curScene = VillageSceneType.Story;
    private VillageSceneType m_preScene = VillageSceneType.Story;

    [SerializeField] private List<GameObject> m_lstVillageStopObj = new List<GameObject>();

    public delegate void UpdateStep();
    UpdateStep m_delUpdate = null;

    bool m_waitBuildMgr = false;

    private bool m_bChange = false;
    public static VillageSceneMgr Inst
    {
        get
        {
            if(m_Inst == null)
            {
                m_Inst = new VillageSceneMgr();
            }
            return m_Inst;
        }
    } 

    void StopObjList(List<GameObject> _lst)
    {
        for(int i = _lst.Count -1; i >= 0; --i)
        {
            if(_lst[i] == null)
            {
                Debug.Log("null : "+_lst[i].ToString());
            }
            if(_lst[i].activeSelf)
            {
                _lst[i].SetActive(false);
            }
        }
    }
    void StartObjList(List<GameObject> _lst)
    {
       for(int i = 0; i < _lst.Count;++i)
        {
            if(!_lst[i].activeSelf)
            {
                _lst[i].SetActive(true);
            }
        }
    }
    void CameraOn(GameObject _camera)
    {
        if(_camera != null && _camera.activeSelf == false)
        {
            _camera.SetActive(true);
        }
    }
    void CameraOff(GameObject _camera)
    {
        if (_camera != null && _camera.activeSelf == true)
        {
            _camera.SetActive(false);
        }
    }
    public void ChangeStory()
    {
        StopObjList(m_lstBuildObj);
        StopObjList(m_lstPortalObj);
        StartObjList(m_lstStoryObj);

        BuildMgr.Inst.OnBuildSlot();
        BuildMgr.Inst.BuildOn = false;

        CameraOn(m_storyCam);
        CameraOff(m_buildCam);
        CameraOff(m_portalCam);
    }
    public void ChangeBuild()
    {
        StopObjList(m_lstStoryObj);
        StopObjList(m_lstPortalObj);
        StartObjList(m_lstBuildObj);

        BuildMgr.Inst.OnBuildSlot();
        BuildMgr.Inst.BuildOn = true;

        CameraOn(m_buildCam);
        CameraOff(m_storyCam);
        CameraOff(m_portalCam);
    }
    public void ChangePortal()
    {
        StopObjList(m_lstStoryObj);
        StopObjList(m_lstBuildObj);
        StartObjList(m_lstPortalObj);

        OnPortalUI();
        BuildMgr.Inst.OffBuildSlot();
        BuildMgr.Inst.BuildOn = false;

        CameraOn(m_portalCam);
        CameraOff(m_storyCam);
        CameraOff(m_buildCam);
    }
    // Start is called before the first frame update
    private void Init()
    {
       if(m_battleUI != null)
        {
            m_battleUI.SetActive(false);
        }
       if(m_portalUI != null)
        {
            m_portalUI.SetActive(false);
        }
        
    }
    private void Awake()
    {
        if (m_Inst == null)
        {
            m_Inst = this;
        //    DontDestroyOnLoad(this);
        }
    }
    void Start()
    {
        Init();
        ChangeStory();
    }

    #region BattleUI
    public void OnPortalUI()
    {
        if (m_portalUI != null && m_portalUI.activeSelf == false)
        {
            m_portalUI.SetActive(true);
        }
    }
    public void OffPortalUI()
    {
        if (m_portalUI != null && m_portalUI.activeSelf == true)
        {
            m_portalUI.SetActive(false);
        }
    }

    //  그 맵을 클릭했을때의 정보 겟
    public void SetAreaType(BT.Enum.AreaType _type)
    {
        m_sceneData.AreaType = _type;
    }

    public void OnBattleUI()
    {
        if(m_battleUI != null && m_battleUI.activeSelf == false )
        {
            m_battleUI.SetActive(true);
        }
    }
    public void OffBattleUI()
    {
        if (m_battleUI != null && m_battleUI.activeSelf == true)
        {
            m_battleUI.SetActive(false);
        }
    }
    public void InitSceneData()
    {
        m_sceneData.AreaType = BT.Enum.AreaType.End;
        m_sceneData.Level = 0;
        m_sceneData.IsSelectMap = false;
    }
    public void DownBattleUILevel()
    {
        m_sceneData.Level -= 1;
        if (m_sceneData.Level < 0)
        {
            m_sceneData.Level = 0;
        }
    }
    public void UpBattleUILevel()
    {
        m_sceneData.Level += 1;
    }
    public void OffVillageSceneMgr()
    {
        if(gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
    }
    public void OnVillageSceneMgr()
    {
        if (gameObject.activeSelf == false)
        {
            gameObject.SetActive(true);
        }
        if(BattleAreaMgr.IsInst())
        {
            BattleAreaMgr.Inst.OffBattleAreaMgr();
        }
    }
    public void GoBattleArea()
    {
        PlayerMgr.Inst.TransmissionInfo();
        TransmissionInfo();
        SceneMgr.Inst.BattleData.IsSelectMap = true;
        if (BattleAreaMgr.IsInst())
        {
            BattleAreaMgr.Inst.OnBattleAreaMgr();
        }
        SceneMgr.LoadScene("DungeonScene");
    }
    public void CancelBattleArea()
    {
        OffBattleUI();
        InitSceneData();
    }
    public void BackBattleAreaSelect()
    {
        CancelBattleArea();
        OffPortalUI();
        ChangeStory();
    }
    //  
    public void TransmissionInfo()
    {
        SceneMgr.Inst.BattleData.CopyData(m_sceneData);
    }
    //  정보를 여기서 끄고 저장하는거는 이동할때만 즉 빌리지씬메니저가 꺼질때만 저장을 한다.
    private void OnEnable()
    {
        if(BuildMgr.IsInstance())
        {
            if (BuildMgr.IsInstance())
            {
                BuildMgr.Inst.PlayBuildMgr();
            }
            if (m_bTest)
            {
                m_delUpdate = TestUpdate;
            }
            else
            {
                m_delUpdate = NormalUpdate;
            }
        }
        else
        {
            m_delUpdate = WaitBuildMgrUpdate;
        }
    }
    private void OnDisable()
    {
        if(BuildMgr.IsInstance())
        {
            BuildMgr.Inst.StopBuildMgr();
        }
    }
    #endregion
    // Update is called once per frame
    void WaitBuildMgrUpdate()
    {
        BuildMgr.Inst.PlayBuildMgr();
        if (m_bTest)
        {
            m_delUpdate = TestUpdate;
        }
        else
        {
            m_delUpdate = NormalUpdate;
        }
    }
    void NormalUpdate()
    {
        if (m_bChange)
        {
            switch (m_curScene)
            {
                case VillageSceneType.Story:
                    ChangeStory();
                    break;
                case VillageSceneType.Build:
                    ChangeBuild();
                    break;
                case VillageSceneType.Portal:
                    ChangePortal();
                    break;
                default:
                    break;
            }
            m_bChange = false;
        }
    }
    void TestUpdate()
    {
        if (m_bTest)
        {
            if (Input.GetKeyUp("1"))
            {
                m_curScene = VillageSceneType.Story;
                m_bChange = true;
            }
            else if (Input.GetKeyUp("2"))
            {
                m_curScene = VillageSceneType.Build;
                m_bChange = true;
            }
            else if (Input.GetKeyUp("3"))
            {
                m_curScene = VillageSceneType.Portal;
                m_bChange = true;
            }
        }
        if (m_bChange)
        {
            switch (m_curScene)
            {
                case VillageSceneType.Story:
                    ChangeStory();
                    break;
                case VillageSceneType.Build:
                    ChangeBuild();
                    break;
                case VillageSceneType.Portal:
                    ChangePortal();
                    break;
                default:
                    break;
            }
            m_bChange = false;
        }
    }


    void Update() => m_delUpdate?.Invoke();
}
