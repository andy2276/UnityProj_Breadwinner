//	test tessllation

struct vtxIn
{
	float4 v4Vertex 	: 	POSITION;
	float3 v3Normal 	: 	NORMAL;
	float4 v3Tangent	:	TANGENT;
};

struct vtxOut
{
	float4 v4Vertex 	: 	SV_POSITION;
	float3 v3Normal 	: 	NORMAL;
	float4 v3Tangent	:	TANGENT;
};

struct TessellationFactors
{
	float fEdge[3] : SV_TessFactor;
	float fInside : SV_InsideTessFactor;
};



vtxOut tessVertex(vtxIn _in)
{
	vtxOut output = (vtxOut)0.f;
	
	output.v4Vertex  = _in.v4Vertex;
	output.v3Normal  = _in.v3Normal;
	output.v3Tangent = _in.v3Tangent;
	return output;
}
float _fTessellationUniform;

TessellationFactors patchConstantFunc(InputPatch<vtxIn,3> _patch)
{
    TessellationFactors output = (TessellationFactors) 0.0f;
    output.fEdge[0] = _fTessellationUniform;
    output.fEdge[1] = _fTessellationUniform;
    output.fEdge[2] = _fTessellationUniform;
    output.fInside = _fTessellationUniform;
    return output;
}

[UNITY_domain("tri")]
[UNITY_outputcontrolpoints(3)]
[UNITY_outputtopology("triangle_cw")]
[UNITY_partitioning("integer")]
[UNITY_patchconstantfunc("patchConstantFunc")]
vtxIn HS_Grass(InputPatch<vtxIn,3> _patch,uint _id : SV_OutputControlPointID )
{
    return _patch[_id];
}

[UNITY_domain("tri")]
vtxOut DS_Grass(TessellationFactors _factor,OutputPatch<vtxIn,3> _patch,float3 _domainCoord : SV_DomainLocation)
{
    vtxIn v = (vtxIn) 0.0f;
#define DS_PROGRAM_INTERPOLATE(_fieldName)\
    v._fieldName = _patch[0]._fieldName * _domainCoord.x + \
    v._fieldName = _patch[1]._fieldName * _domainCoord.y + \
    v._fieldName = _patch[2]._fieldName * _domainCoord.z;
    
    DS_PROGRAM_INTERPOLATE(v4Vertex);
    DS_PROGRAM_INTERPOLATE(v3Normal);
    DS_PROGRAM_INTERPOLATE(v3Tangent);
    
    return tessVertex(v);
}