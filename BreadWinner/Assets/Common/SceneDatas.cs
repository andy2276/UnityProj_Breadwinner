﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SceneDatas
{
    BT.Enum.AreaType m_eAreaType = BT.Enum.AreaType.End;
    int m_nLevel = 0;
    bool m_bSelectMap = false;

    public BT.Enum.AreaType AreaType { get => m_eAreaType; set => m_eAreaType = value; }
    public int Level { get => m_nLevel; set => m_nLevel = value; }
    public bool IsSelectMap { get => m_bSelectMap; set => m_bSelectMap = value; }

    public void CopyData(SceneDatas _scr)
    {
        m_eAreaType = _scr.AreaType;
        m_nLevel = _scr.m_nLevel;
    }
    
}
