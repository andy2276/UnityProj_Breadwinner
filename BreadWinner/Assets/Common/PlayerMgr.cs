﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMgr : MonoBehaviour
{
    static PlayerMgr m_Inst = null;
    static string[] m_arrAnimNames = {
                     "IdleBattle","SenseSomethingRPT","WalkFWD","WalkBWD","WalkRight","WalkLeft",
                    "RunFWD","Attack01","Attack02","GetHit","Die"
                };
    public static PlayerMgr Inst
    { get
        {
            if (m_Inst == null)
            {
                m_Inst = new PlayerMgr();
            }
            return m_Inst;
        }
    }

    [SerializeField] PlayerDatas m_playerDatas = new PlayerDatas();

    PlayerDatas m_curPlayerData = null;

    //  이걸 현제 플레이어로 변경을 하자
    public GameObject Player { get => m_playerDatas.m_playerObj; set => m_playerDatas.m_playerObj = value; }
    public AIStatus PlayerStatus { get { return m_playerDatas.m_playerObj.GetComponent<Player>().Status; } }

    public static string[] ArrAnimNames { get => m_arrAnimNames; }
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        if (m_Inst == null)
        {
            m_Inst = this;
            DontDestroyOnLoad(this);
        }
    }

    public void TransmissionInfo()
    {
        SceneMgr.Inst.PlayerData = m_playerDatas;
    }
    public void SavePlayer()
    {


    }
    public void LoadPlayer()
    {
        
    }
    
}
