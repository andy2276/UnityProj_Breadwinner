﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadWinner : MonoBehaviour
{
    public delegate void TimeTic();

    [SerializeField] private AIStatus g_AiStateOrigin = null;
    protected AIStatus g_AiState = null;
    public AIStatus StateOrigin { get => g_AiStateOrigin; set => g_AiStateOrigin = value; }
    public AIStatus Status { get { return g_AiState; } set { g_AiState = value; } }


    public event TimeTic m_eventCoolTimeToc;
    // Start is called before the first frame update

    public void CopyState()
    {
        if (g_AiStateOrigin != null)
        {
            g_AiState = g_AiStateOrigin.CloneStatus();
            if (g_AiState.ListCoolTime.Count != 0)
            {
                for (int i = 0; i < g_AiState.ListCoolTime.Count; ++i)
                {
                    m_eventCoolTimeToc += g_AiState.ListCoolTime[i].CoolTimeTicToc;
                }
            } 
        }
    }
    public void AddLevel(AIStatus _state)
    {
        g_AiState.AddState(_state);
        g_AiState.AddTime(_state);
    }

    protected void CoolTimeTicToc()
    {
        m_eventCoolTimeToc?.Invoke();
    }

    private void Awake()
    {
        CopyState();
    }
    
}
