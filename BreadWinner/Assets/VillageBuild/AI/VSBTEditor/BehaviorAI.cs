﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorAI : BreadWinner
{
    public delegate void BTReset();
    public delegate BT.Return RunningNode();
    public event BTReset m_eventReset = null;
    public RunningNode m_runningNode = null;
    private BT.Virtual.VSBTNode m_BTRoot = null;
    private List<BT.Virtual.VSBTNode> m_lstNodes;
    private bool m_bCollider = false;
    private bool m_bDeath = false;
    private bool m_bDamaged = false;
    private BT.Return m_eCurState = BT.Return.Success;
    private Animator m_animator = null;
    private bool m_bReset = false;
    [SerializeField] private BT.Virtual.VSBTNode m_curRunningNode = null;
    [SerializeField] private float m_fErrTime = 3;
    [SerializeField] private float m_fcurErrTime = 0;

    [SerializeField] private GameObject g_target = null;
    [SerializeField] private Object g_BTAsset = null;
    [SerializeField] private List<GameObject> m_lstAttacker = new List<GameObject>();
    [SerializeField] private List<GameObject> m_lstDefencer = new List<GameObject>();

    private itemDrop m_drop = null;

    private List<DamageSystem.Attacker> m_atttackerScript = new List<DamageSystem.Attacker>();
    private List<DamageSystem.Defenser> m_defencerScript = new List<DamageSystem.Defenser>();

    public BT.Virtual.VSBTNode BTRoot { get { return m_BTRoot; } set { m_BTRoot = value; } }
    public List<BT.Virtual.VSBTNode> NodeLists { get { return m_lstNodes; } set { m_lstNodes = value; } }
    public bool IsCollider { get { return m_bCollider; } }
    public GameObject Target { get { return g_target; } set { g_target = value; } }

    public bool Death { get => m_bDeath; set => m_bDeath = value; }
    public bool Damaged { get => m_bDamaged; set => m_bDamaged = value; }
    public Animator Animator { get => m_animator; }
    public bool Reset { get => m_bReset; set => m_bReset = value; }
    public BT.Virtual.VSBTNode CurRunningNode { get => m_curRunningNode; set => m_curRunningNode = value; }

    public void RegistRunningNode(RunningNode _func)
    {
        m_runningNode = _func;
    }


    public void RegistResetState(BTReset _callback)
    {
        m_eventReset += _callback;
    }
    public void DeleteResetState(BTReset _callback)
    {
        m_eventReset -= _callback;
    }
    
    // Start is called before the first frame update
    public void DefaultInit()
    {
        m_lstNodes = new List<BT.Virtual.VSBTNode>();
        m_animator = GetComponent<Animator>();
        if (g_BTAsset != null)
        {
            m_BTRoot = BT.VSBTEditor.LoadBTAsset(g_BTAsset, m_lstNodes);
            BehaviorAI ai = gameObject.GetComponent<BehaviorAI>();
            foreach (var n in m_lstNodes)
            {
                n.BTAi = ai;
                n.BTObj = gameObject;
            }
            m_BTRoot.ResetTree(BT.Return.Evaluate);
        }
        if (g_target == null)
        {
            g_target = BattleAreaMgr.Inst.Player;
        }
        if (m_lstAttacker.Count != 0)
        {
            for (int i = 0; i < m_lstAttacker.Count; ++i)
            {
                m_atttackerScript.Add(m_lstAttacker[i].GetComponent<DamageSystem.Attacker>());
                //  만약 충돌이 제대로 안되면 이걸 건들이자
                if (m_lstAttacker[i].activeSelf)
                {
                    m_lstAttacker[i].SetActive(false);
                }
            }

        }
        if (m_lstDefencer.Count != 0)
        {
            for (int i = 0; i < m_lstDefencer.Count; ++i)
            {
                DamageSystem.Defenser d = m_lstDefencer[i].GetComponent<DamageSystem.Defenser>();
                d.DamagedFunc = DamagedFunc;
                m_defencerScript.Add(d);

            }
        }
    }
    
    public void AnimReturnIdle()
    {
        m_animator.SetInteger("State", 0);
    }
    public void AnimPlay(BT.Enum.AnimClipType _clip)
    {
        if (!m_animator.GetCurrentAnimatorStateInfo(0).IsName(PlayerMgr.ArrAnimNames[(int)_clip]))
        {
            m_animator.Play(PlayerMgr.ArrAnimNames[(int)_clip], 0);
        }
    }
    public void AnimTrans(BT.Enum.AnimClipType _clip)
    {
        m_animator.SetInteger(PlayerMgr.ArrAnimNames[(int)_clip], (int)_clip);
    }
    void Start()
    {
        m_drop = GetComponent<itemDrop>();
    }
    public void RegistDefenceFunc(DamageSystem.Defenser.DamagedEvent _func)
    {

    }
    public bool IsCoolTimeOn(int _idx) => Status.ListCoolTime[_idx].IsOnSkill;
    public void UseCoolTime(int _idx) => Status.ListCoolTime[_idx].UseSkill();
    //  디펜스에서 실행될거
    #region Attack!
    public void DamagedFunc(DamageSystem.DamageInput _input)
    {
        if (!m_bDamaged)
        {
            if (10 < _input.m_fDamage)
            {
                BattleAreaMgr.Inst.CreateDamageUpper(transform.position, _input.m_fDamage, Color.red);
            }
            Status.AiHp -= _input.m_fDamage;
            Debug.Log("Damaged!:" + Status.AiHp);
            m_bDamaged = true;
        }
    }
    public void DamagedEnd()
    {
        Debug.Log("damageEnd");
        m_bDamaged = false;
        AnimPlay(BT.Enum.AnimClipType.Idle);
    }
    public void AttackStart(int _n)
    {
        if (m_atttackerScript.Count > _n)
        {
            if (m_lstAttacker[_n].activeSelf == false)
            {
                m_lstAttacker[_n].SetActive(true);
                m_atttackerScript[_n].AttackerStart();
            }

        }
    }
    public void AttackOn(int _n)
    {
        if (m_atttackerScript.Count > _n)
        {
            m_atttackerScript[_n].AttackOn();
        }
    }
    //  디펜스도 그냥 상속받자
    public void AttackEnd(int _n)
    {
        if (m_atttackerScript.Count > _n)
        {
            if (m_lstAttacker[_n].activeSelf == true)
            {
                m_lstAttacker[_n].SetActive(false);
                m_atttackerScript[_n].AttackerEnd();
            }
        }
    }
    public virtual void DeathOn()
    {
        //  여기에 자원을 입력하자
        if(m_drop != null)
        {
            m_drop.Drop(transform.position);
        }
        m_bDeath = true;
    }
    #endregion
    private void ResetBT()
    {
        if (m_eCurState != BT.Return.Running)
        {
            m_BTRoot.ResetTree(BT.Return.Evaluate);
        }
    }
    void ExcuteRunningNode()
    {
        m_eCurState = m_runningNode.Invoke();
        switch (m_eCurState)
        {
            case BT.Return.Evaluate:
                break;
            case BT.Return.Success:
            case BT.Return.Fail:
                m_fcurErrTime = 0;
                m_runningNode = null;

                break;
            case BT.Return.Running:
                if (m_fcurErrTime < m_fErrTime)
                {
                    m_fcurErrTime += Time.deltaTime;
                }
                else
                {
                    m_fcurErrTime = 0;
                    m_runningNode = null;
                }
                break;
            default:
                break;
        }
    }
    protected void BehaviorUpdate()
    {
        CoolTimeTicToc();
        if (m_runningNode != null && m_bDamaged == false && m_bDeath == false)
        {
            ExcuteRunningNode();
            return;
        }
        else
        {
            m_eCurState = m_BTRoot.Evaluate();
            switch (m_eCurState)
            {
                case BT.Return.Evaluate:
                    break;
                case BT.Return.Success:
                case BT.Return.Fail:
                    ResetBT();
                    break;
                case BT.Return.Running:
                    break;
                default:
                    break;
            }

        }

    }
    public Vector3 LookTarget()
    {
        if (g_target != null)
        {
            return (g_target.transform.position - gameObject.transform.position).normalized;
        }
        return gameObject.transform.forward;
    }
    // Update is called once per frame
    void Update()
    {
        if (m_bDeath) return;
        BehaviorUpdate();
    }
}
