﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BT.Enum;
namespace BT
{
    public enum Return
        {
            Evaluate
       , Success
       , Fail
       , Running
        }
    namespace Enum
    {
        public enum NodeCategory
        {
            Composite, Task
        }
        public enum NodeTag
        {
            //                                 BattleArea
            Default, Seq, Sel, Prc, Cdt, Act , BatCdt,BatSpon,BatCenema
        }
    }
    
   
    namespace Virtual
    {
        public abstract class VSBTNode : ScriptableObject
        {

            #region Variable
            #region StaticVars
            private static int g_nNodeID = 0;
            #endregion
            [SerializeField] protected bool m_bRoot = false;
            [SerializeField] protected BT.Virtual.VSBTNode   m_parent = null;
            [SerializeField] protected NodeCategory          m_eNodeCat;
            [SerializeField] protected NodeTag               m_eNodeTag;
            protected Return             m_eState = BT.Return.Evaluate;
            [SerializeField] protected string                m_strTitle;
            [SerializeField] protected string                m_strPath;
            [SerializeField] protected Rect                  m_wndNodeRect;
            [SerializeField] private int                     m_nNodeID;
            protected GameObject            m_gameObj;
            protected BehaviorAI            m_aiProperty;
            protected BattleArea            m_battleArea;
            #endregion

            #region Interface
            public bool                      IsRoot     { get { return m_bRoot; } set { m_bRoot = value; } }
            public  BT.Virtual.VSBTNode      Parent     { get { return m_parent; } set { m_parent = value; } }
            public  NodeCategory             Category   {get{ return m_eNodeCat; }set { m_eNodeCat = value; } }
            public  NodeTag                  NodeTag    { get { return m_eNodeTag; } set { m_eNodeTag = value; } }
            public  BT.Return                State      { get { return m_eState; } set { m_eState = value; } }
            public  string                   Title      { get { return m_strTitle; } set { m_strTitle = value; } }
            public  string                   BTPath     { get { return m_strPath; } set { m_strPath = value; } }
            public  Rect                     NodeRect   { get { return m_wndNodeRect; } set { m_wndNodeRect = value; } }
            public  int                      NodeID     { get { return m_nNodeID; } }
            public  GameObject               BTObj      { get { return m_gameObj; }set { m_gameObj = value; } }
            public  BehaviorAI               BTAi       { get { return m_aiProperty; } set { m_aiProperty = value; } }
            public  BattleArea               BattleArea { get { return m_battleArea; } set { m_battleArea = value; } }

            #endregion

            #region Ctor
            public VSBTNode()
            {
                m_nNodeID = g_nNodeID++;
                if(m_aiProperty != null)
                {
                    m_aiProperty.RegistResetState(ResetState);
                }
            }
            #endregion

            #region Funcs   
            //  함수들
            #region AbstrctFunc
            // AbstrctFunc : 무조건 만들어야 하는것들
            public abstract BT.Virtual.VSBTNode MakeNode(object e);
            public abstract BT.Return Evaluate();
            #endregion
            #region VirtualFunc
            // VirtualFunc : 추가 설정하는 기능들
            public virtual void Init() { }
            public virtual void SortSlot() { }
            public virtual void AddData(int _idx, object _data) { }
            public virtual void DrawCurve() { }
            public virtual void CurrentNode() { }
            #endregion
            #region DefaultFunc
            public void ResetState()
            {
                m_eState = Return.Evaluate;
            }
            public void ResetState(BT.Return _state = BT.Return.Evaluate)
            {
                m_eState = _state;
            }
            public virtual void ResetTree(BT.Return _state = BT.Return.Evaluate)
            {

            }
            #endregion
            #region WndFunc
            //  HelpFunc : 도움이 되는 기능들
            protected void NodeWindow_Default()
            {
                GUILayout.BeginVertical();
              //  GUILayout.Space(m_DefaultHei);
                GUILayout.EndVertical();
                if(m_parent != null)
                {
                    EditorGUILayout.LabelField("parent        [ " + m_parent.Title + " ]",GUILayout.Width(250));
                }
                else
                {
                    EditorGUILayout.LabelField("parent        [ None ]", GUILayout.Width(200));
                }
                
                EditorGUILayout.LabelField("Class          [ " + this.GetType().ToString()+" ]");
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label("Title:",GUILayout.Width(60));
                m_strTitle = GUILayout.TextField(m_strTitle, GUILayout.Width(200));
                EditorGUILayout.EndHorizontal();
            }
            public abstract void DrawWindow();
            public abstract BT.Virtual.VSBTNode CopyNewNode(BT.Virtual.VSBTNode _parent = null);//                              리커전
            public abstract BT.Virtual.VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list,BT.Virtual.VSBTNode _parent = null);//                                   에디터와 소통하는 부분
            public abstract BT.Virtual.VSBTNode SaveBT( List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null);// 세이브
            public abstract BT.Virtual.VSBTNode LoadBT(List<VSBTNode> _list, BT.Virtual.VSBTNode _parent = null);//             로드
            public abstract BT.Virtual.VSBTNode LoadAI(List<VSBTNode> _list, BT.Virtual.VSBTNode _parent = null);
            public virtual void TestTreeDebug(int _floor)
            {
                
            }
            protected void DefaultCopy(BT.Virtual.VSBTNode _clone) //    not parent
            {
                _clone.IsRoot = m_bRoot;
                _clone.m_aiProperty = m_aiProperty;
                _clone.m_gameObj = m_gameObj;
                _clone.m_eNodeCat = m_eNodeCat;
                _clone.m_strTitle = m_strTitle;
                _clone.m_wndNodeRect = m_wndNodeRect;
                _clone.m_eNodeTag = m_eNodeTag;
            }
            public static void ClearNodeList(List<BT.Virtual.VSBTNode> _lst)
            {
                for(int i = _lst.Count -1; i >= 0; --i)
                {
                    DestroyImmediate(_lst[i]);
                }
            }
            #endregion  
            #endregion 
        }
    }
   
}
