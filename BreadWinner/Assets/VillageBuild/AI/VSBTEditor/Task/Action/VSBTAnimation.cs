﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BT.Enum;
using BT.Virtual;

namespace BT
{
    namespace Enum
    {
        public enum AnimClipType
        {
            Idle, Sense, Foward, Back, Right, Left,
            Run, AttackSoft, AttackHard, Hit, Die
        }
        public enum AnimType
        {
            //  one은 작동시키고 안돌아간다
            //  wiat은 작동시키고 기다렸다가 돌아간다
            Once,Wait
        }
    }
    namespace Task
    {
        namespace Action
        {
            public class VSBTAnimation : Virtual.VSBTTask
            {
                static string[] m_arrAnimNames = {

                     "IdleBattle","SenseSomethingRPT","WalkFWD","WalkBWD","WalkRight","WalkLeft",
                    "RunFWD","Attack01","Attack02","GetHit","Die"
                };
                [SerializeField] private AnimType m_eAnimType;
                [SerializeField] private AnimClipType m_eAnimClipType;
                [SerializeField] private AnimClipType m_nReAnimClip = 0;
                [SerializeField] private string m_strAnimParam = "State";
                public override void Init()
                {
                    
                }
                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTAnimation clone = (VSBTAnimation)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_eAnimType = m_eAnimType;
                    clone.m_strAnimParam = m_strAnimParam;
                    clone.m_eAnimClipType = m_eAnimClipType;
                    clone.m_nReAnimClip = m_nReAnimClip;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTAnimation clone = (VSBTAnimation)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_eAnimType = (Enum.AnimType)EditorGUILayout.EnumPopup("AnimType", m_eAnimType, GUILayout.Width(300));
                    m_eAnimClipType = (Enum.AnimClipType)EditorGUILayout.EnumPopup("ClipType", m_eAnimClipType, GUILayout.Width(300));
                    m_nReAnimClip = (Enum.AnimClipType)EditorGUILayout.EnumPopup("BackType", m_nReAnimClip, GUILayout.Width(300));
                    m_strAnimParam = EditorGUILayout.TextField("AnimParam", m_strAnimParam, GUILayout.Width(200));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    if(m_eState != Return.Evaluate)
                    {
                        return m_eState;
                    }
                    if (Enum.AnimType.Once == m_eAnimType)
                    {
                        if (!m_aiProperty.Animator.GetCurrentAnimatorStateInfo(0).IsName(m_arrAnimNames[(int)m_eAnimClipType]))
                        {
                            m_aiProperty.Animator.Play(m_arrAnimNames[(int)m_eAnimClipType], 0);
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return BT.Return.Running;//이거는 나중에
                        }
                    //   m_aiProperty.Animator.SetInteger(m_strAnimParam, (int)m_eAnimClipType);
                        ChangeStatus(Return.Evaluate, Return.Success);
                        return Return.Success;
                    }
                    else if (Enum.AnimType.Wait == m_eAnimType)
                    {
                        if (!m_aiProperty.Animator.GetCurrentAnimatorStateInfo(0).IsName(m_arrAnimNames[(int)m_eAnimClipType]))
                        {
                            m_aiProperty.Animator.SetInteger(m_strAnimParam, (int)m_eAnimClipType);
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return Return.Running;
                        }

                        if (m_aiProperty.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.99999f)
                        {
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return BT.Return.Running;
                        }
                        else
                        {
                            m_aiProperty.Animator.SetInteger(m_strAnimParam, (int)m_nReAnimClip);
                            ChangeStatus(Return.Evaluate, Return.Success);
                            return BT.Return.Success;
                        }
                    }
                    return Return.Fail;
                    
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTAnimation clone = (VSBTAnimation)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTAnimation>();

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTAnimation clone = (VSBTAnimation)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }

                protected override void SetNodeTag() => NodeTag = NodeTag.Act;
            }
        }
    }
}