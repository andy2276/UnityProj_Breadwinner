﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using BT.Enum;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace Task
    {
        namespace Action
        {
            public class VSBTTargetTurn : BT.Virtual.VSBTTask
            {
                [SerializeField] private Vector2 m_v2AngleRange = new Vector2(90, 0);//   이거 -1 ~ 0 ~ 1 인데 이게 그냥 각도로 하고 세타로 바꾸자.
                [SerializeField] private float m_fAccordAngle = 0.1f;
                private Vector2 m_v2RadianRange = new Vector2(0, 0);
                static Vector2 m_v2One = new Vector2(1, 1);

                public VSBTTargetTurn()
                {
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 200;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTTargetTurn clone = (VSBTTargetTurn)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_v2AngleRange = m_v2AngleRange;
                    clone.m_fAccordAngle = m_fAccordAngle;
                    return clone;
                }

                public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
                {
                    VSBTTargetTurn clone = (VSBTTargetTurn)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_v2AngleRange = EditorGUILayout.Vector2Field("AngleRange:", m_v2AngleRange, GUILayout.Width(200));
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("AccordAngle", GUILayout.Width(80));
                    m_fAccordAngle = EditorGUILayout.FloatField(m_fAccordAngle, GUILayout.Width(80));
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();

                }

                public override Return Evaluate()
                {
                    if (m_eState != BT.Return.Evaluate)
                    {
                        return m_eState;
                    }
                    if (m_aiProperty.Target != null)
                    {
                        Vector3 targetPos = m_aiProperty.Target.transform.position;
                        Vector3 mePos = m_aiProperty.transform.position;
                        Vector3 meLook = m_aiProperty.transform.forward;
                        Vector3 lookPos = (targetPos - mePos).normalized;
                        Vector3 lookLerp = new Vector3();
                        m_v2RadianRange = m_v2One -  (m_v2AngleRange * Mathf.Deg2Rad);

                        float turnAngle = Vector3.Dot(meLook, lookPos);
                        if (m_v2RadianRange.x <= turnAngle && turnAngle < m_v2RadianRange.y)
                        {
                            lookLerp = Vector3.Slerp(meLook, lookPos, Time.deltaTime * m_aiProperty.Status.AiMoveSpeed).normalized;
                            m_aiProperty.transform.rotation = Quaternion.LookRotation(lookLerp, Vector3.up);
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return BT.Return.Running;
                        }
                        else
                        {
                            float accord = m_fAccordAngle * Mathf.Deg2Rad;
                            if (m_v2RadianRange.y - accord < turnAngle && turnAngle < m_v2RadianRange.y + accord)
                            {
                                ChangeStatus(BT.Return.Evaluate, BT.Return.Success);
                                return BT.Return.Success;
                            }
                            else if (turnAngle < m_v2RadianRange.x)
                            {
                                ChangeStatus(BT.Return.Evaluate, BT.Return.Fail);
                                return BT.Return.Fail;
                            }
                            else
                            {
                                ChangeStatus(BT.Return.Evaluate, BT.Return.Fail);
                                return BT.Return.Fail;
                            }
                        }
                    }
                    else
                    {
                        ChangeStatus(BT.Return.Evaluate, BT.Return.Fail);
                        return BT.Return.Fail;
                    }
                }
                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTTargetTurn clone = (VSBTTargetTurn)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }
                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTTargetTurn>();
                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTTargetTurn clone = (VSBTTargetTurn)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag() => NodeTag = NodeTag.Act;
            }

        }
    }
}