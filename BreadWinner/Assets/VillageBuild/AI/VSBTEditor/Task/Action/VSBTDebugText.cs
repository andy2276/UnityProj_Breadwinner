﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using BT.Enum;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace Task
    {
        namespace Action
        {
            public class VSBTDebugText : BT.Virtual.VSBTTask
            {
                [SerializeField] string m_strText = "";
                [SerializeField] BT.Return m_eReturn = BT.Return.Success;
                [SerializeField] float m_fRunningTime = 0;
                float m_fCurTime = 0;

                public VSBTDebugText()
                {
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 170;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTDebugText clone = (VSBTDebugText)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_strText = m_strText;
                    clone.m_eReturn = m_eReturn;
                    clone.m_fRunningTime = m_fRunningTime;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTDebugText clone = (VSBTDebugText)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_strText = EditorGUILayout.TextField(m_strText, GUILayout.Width(120));
                    m_eReturn = (Return)EditorGUILayout.EnumPopup(m_eReturn, GUILayout.Width(120));
                    m_fRunningTime = EditorGUILayout.FloatField(m_fRunningTime, GUILayout.Width(70));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    if(BT.Return.Evaluate != m_eState)
                    {
                       
                        return m_eState;
                    }
                    switch (m_eReturn)
                    {
                        case Return.Evaluate:
                        case Return.Success:
                        case Return.Fail:
                            Debug.Log( m_strText + " is "+m_eReturn.ToString());
                            ChangeStatus(Return.Evaluate, m_eReturn);
                            return m_eState;
                        case Return.Running:
                            if(m_fCurTime < m_fRunningTime)
                            {
                                m_fCurTime += Time.deltaTime;
                                Debug.Log(" Runing " + m_strText);
                                return BT.Return.Running;
                            }
                            else
                            {
                                m_fCurTime = 0;
                                Debug.Log( m_strText + " Done ");
                                ChangeStatus(Return.Evaluate, m_eReturn);
                                return Return.Success;
                            }
                            
                    }
                    return m_eState;
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTDebugText clone = (VSBTDebugText)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTDebugText>();

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTDebugText clone = (VSBTDebugText)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag()
                {
                    NodeTag = NodeTag.Act;
                }
            }
        }
    }

}