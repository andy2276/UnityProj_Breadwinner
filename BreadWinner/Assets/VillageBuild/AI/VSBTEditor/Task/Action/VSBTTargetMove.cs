﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;
using BT.Enum;

namespace BT
{
    namespace Task
    {
        namespace Action
        {
            public class VSBTTargetMove : BT.Virtual.VSBTTask
            {
                [SerializeField] private bool m_bDefaultRange = true;    //  true == 가까운 거리까지 false == 공격거리까지
                [SerializeField] private float m_fCompRange = 0;
                [SerializeField] private DefaultRange m_eDefaultRange = DefaultRange.SearchRange;
                public VSBTTargetMove()
                {
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 180;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTTargetMove clone = (VSBTTargetMove)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_bDefaultRange = m_bDefaultRange;
                    clone.m_fCompRange = m_fCompRange;
                    clone.m_eDefaultRange = m_eDefaultRange;
                    return clone;
                }

                public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
                {
                    VSBTTargetMove clone = (VSBTTargetMove)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    m_bDefaultRange = EditorGUILayout.BeginToggleGroup("IsDefaultRange", m_bDefaultRange);
                    m_eDefaultRange = (DefaultRange)EditorGUILayout.EnumPopup(m_eDefaultRange, GUILayout.Width(100));
                    EditorGUILayout.EndToggleGroup();
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("NearRange", GUILayout.Width(80));
                    m_fCompRange = EditorGUILayout.FloatField(m_fCompRange, GUILayout.Width(150));
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();

                    
                }

                public override Return Evaluate()
                {
                    if(m_eState != Return.Evaluate)
                    {
                        return m_eState;
                    }
                    Vector3 targetPos = m_aiProperty.Target.transform.position;
                    Vector3 mePos = m_aiProperty.transform.position;

                    float endDist = 0;
                    if(m_bDefaultRange)
                    {
                        switch (m_eDefaultRange)
                        {
                            case DefaultRange.SearchRange:
                                endDist = m_aiProperty.Status.AiSearchRange;
                                break;
                            case DefaultRange.AttackRange:
                                endDist = m_aiProperty.Status.AiAttackRange;
                                break;
                            case DefaultRange.NearRange:
                                endDist = m_aiProperty.Status.AiNearRange;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        endDist = m_fCompRange;
                    }
                   
                    float targetToDist = Vector3.Distance(mePos,targetPos);

                    if(targetToDist <= endDist)
                    {
                        m_aiProperty.AnimReturnIdle();
                        return ChangeStatus(Return.Evaluate, Return.Success);
                    }
                    else
                    {
                        //  Vector3 targetToDir = (targetPos - mePos).normalized;
                        m_aiProperty.AnimPlay(AnimClipType.Foward);
                        m_gameObj.transform.position = Vector3.Lerp(mePos,targetPos, Time.deltaTime * m_aiProperty.Status.AiMoveSpeed  * 0.1f);
                        m_aiProperty.RegistRunningNode(Evaluate);
                        return Return.Running;
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTTargetMove clone = (VSBTTargetMove)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTTargetMove>();
               
                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTTargetMove clone = (VSBTTargetMove)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag()
                {
                    NodeTag = NodeTag.Act;
                }
                
                
            }
        }

    }
}