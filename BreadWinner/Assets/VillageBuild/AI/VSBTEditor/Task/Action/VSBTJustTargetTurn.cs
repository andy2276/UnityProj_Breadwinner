﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;

namespace BT
{
   
    namespace Task
    {
        namespace Action
        {
            public class VSBTJustTargetTurn : BT.Virtual.VSBTTask
            {
                static Vector2 m_v2One = new Vector2(1, 1);
                [SerializeField] private Vector2 m_v2AngleRange = new Vector2(0,10);
                private Vector2 m_v2RadianRange = new Vector2(0, 0);
                public VSBTJustTargetTurn()
                {
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 170;
                    m_v2RadianRange.x = Mathf.Cos(m_v2AngleRange.x);
                    m_v2RadianRange.y = Mathf.Cos(m_v2AngleRange.y);
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTJustTargetTurn clone = (VSBTJustTargetTurn)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_v2AngleRange = m_v2AngleRange;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTJustTargetTurn clone = (VSBTJustTargetTurn)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_v2AngleRange = EditorGUILayout.Vector2Field("AngleRange:", m_v2AngleRange, GUILayout.Width(200));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    if (m_eState != BT.Return.Evaluate)
                    {
                        return m_eState;
                    }
                    Vector3 meLook = m_gameObj.transform.forward;
                    Vector3 lookPos = (m_aiProperty.Target.transform.position - m_gameObj.transform.position).normalized;
                    Vector3 cross = Vector3.Cross(meLook, lookPos);
                   
                  //  float turnAngle = Vector3.Dot(meLook, lookPos);

                    //m_v2RadianRange.x = Mathf.Cos(m_v2AngleRange.x);
                    //m_v2RadianRange.y = Mathf.Cos(m_v2AngleRange.y);

                    float turnAngle = Vector3.Dot(meLook, lookPos);

                    Vector3 dir = (m_aiProperty.Target.transform.position - m_gameObj.transform.position).normalized;
                    m_aiProperty.gameObject.transform.rotation = Quaternion.Slerp(m_gameObj.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * m_aiProperty.Status.m_fRotateSpeed);

                    return ChangeStatus(BT.Return.Evaluate, BT.Return.Success);
                    //if (m_v2RadianRange.y < turnAngle && turnAngle < m_v2RadianRange.x)
                    //{
                    //    if (cross.y < 0)
                    //    {
                    //        //  왼쪽에 있음
                    //        m_aiProperty.AnimPlay(Enum.AnimClipType.Right);
                    //    }
                    //    else
                    //    {
                    //        m_aiProperty.AnimPlay(Enum.AnimClipType.Left);
                    //        //  오른쪽에 있음
                    //    }
                    //    Debug.Log("회전해라!");
                    //    Vector3 dir = m_aiProperty.Target.transform.position - m_gameObj.transform.position;
                    //    m_aiProperty.gameObject.transform.rotation = Quaternion.Slerp(m_gameObj.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * m_aiProperty.Status.m_fRotateSpeed);
                    //    return BT.Return.Running;
                    //}
                    //else
                    //{
                    //    Debug.Log("성공!");
                    //    //  Idle 로
                    //    m_aiProperty.AnimReturnIdle() ;
                    //    return ChangeStatus(BT.Return.Evaluate, BT.Return.Success);
                    //}
                }
                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTJustTargetTurn clone = (VSBTJustTargetTurn)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTJustTargetTurn>();
              

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTJustTargetTurn clone = (VSBTJustTargetTurn)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag() => NodeTag = Enum.NodeTag.Act;
               
            }
        }
    }
}
