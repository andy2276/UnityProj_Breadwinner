﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;
using BT.Enum;
namespace BT
{
    namespace Enum
    {
        enum MoveDir
        {
            Forward,Right,Back,Left,Custom
        }
    }

    namespace Task
    {
        namespace Action
        {
            //  지정한 방향으로 간다. 거리 혹은 시간 혹은 충돌 중 하나를 결정하게 만드는데, 그중 하나라도 안되면 그 방향으로 안간다
            public class VSBTDirMove : BT.Virtual.VSBTTask
            {
                [SerializeField] bool           m_bDistEnd = false;
                [SerializeField] float          m_fEndDist = 0;
                [SerializeField] bool           m_bTimeEnd = false;
                [SerializeField] float          m_fEndTime = 0;
                [SerializeField] bool           m_bColliderEnd = false;
                [SerializeField] Vector3        m_vInput3MoveDir = new Vector3();
                [SerializeField] MoveDir        m_eMoveDir = MoveDir.Custom;
                private Vector3 m_v3MoveDir = new Vector3();
                private Vector3 m_v3StartPos = new Vector3();
                private bool m_bDistStart = true;
                private float m_fCurTime = 0;

                public VSBTDirMove()
                {
                    m_wndNodeRect.width=(270);
                    m_wndNodeRect.height = (280);
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTDirMove clone = (VSBTDirMove)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_bDistEnd= m_bDistEnd;
                    clone.m_fEndDist=m_fEndDist;  
                    clone.m_bTimeEnd=m_bTimeEnd;
                    clone.m_fEndTime=m_fEndTime;
                    clone.m_bColliderEnd=m_bColliderEnd;    
                    clone.m_vInput3MoveDir=m_vInput3MoveDir;    
                    clone.m_eMoveDir= m_eMoveDir;
                    return clone;
                }

                public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
                {
                    VSBTDirMove clone = (VSBTDirMove)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    GUILayoutOption[] option = { GUILayout.Width(200) };
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.LabelField("DirType", option);
                    m_eMoveDir = (MoveDir)EditorGUILayout.EnumPopup(m_eMoveDir, option);
                    m_vInput3MoveDir = EditorGUILayout.Vector3Field("CustomDir", m_vInput3MoveDir, option);
                    m_bDistEnd =     GUILayout.Toggle(m_bDistEnd,"DistEnd     ", option);
                    m_fEndDist = EditorGUILayout.FloatField("EndDist", m_fEndDist, option);
                    m_bTimeEnd =     GUILayout.Toggle(m_bTimeEnd,"TimeEnd     ", option);
                    m_fEndTime = EditorGUILayout.FloatField("EndTime", m_fEndTime, option);
                    m_bColliderEnd = GUILayout.Toggle(m_bColliderEnd,"ColliderEnd ", option);
                    
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    if (!m_bDistEnd && !m_bTimeEnd && !m_bColliderEnd)
                    {
                        return BT.Return.Fail;
                    }
                    if(m_eState != BT.Return.Evaluate)
                    {
                        return m_eState;
                    }
                    switch (m_eMoveDir)
                    {
                        case MoveDir.Forward:
                            m_v3MoveDir = m_gameObj.transform.forward;
                            break;
                        case MoveDir.Right:
                            m_v3MoveDir = m_gameObj.transform.forward;
                            break;
                        case MoveDir.Back:
                            m_v3MoveDir = m_gameObj.transform.forward;
                            break;
                        case MoveDir.Left:
                            m_v3MoveDir = m_gameObj.transform.forward;
                            break;
                        case MoveDir.Custom:
                            m_v3MoveDir = m_vInput3MoveDir;
                            break;
                        default:
                            break;
                    }
                    Vector3 mePos = m_gameObj.transform.position;
                    m_gameObj.transform.position += m_v3MoveDir * Time.deltaTime * m_aiProperty.Status.AiMoveSpeed;
                    
                    if(m_bColliderEnd)
                    {
                        if(!m_aiProperty.IsCollider)
                        {
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return BT.Return.Running;
                        }
                        else
                        {
                            ChangeStatus(Return.Evaluate, Return.Success);
                            return m_eState;
                        }
                    }
                    if(m_bDistEnd)
                    {
                        if(m_bDistStart)
                        {
                            m_v3StartPos = mePos;
                            m_bDistStart = false;
                        }
                        float dist = Vector3.Distance(m_v3StartPos, mePos);
                        if(dist < m_fEndDist)
                        {
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return BT.Return.Running;
                        }
                        else
                        {
                            m_v3StartPos = Vector3.zero;
                            ChangeStatus(Return.Evaluate, Return.Success);
                            return m_eState;
                        }
                    }
                    if(m_bTimeEnd)
                    {
                        m_fCurTime += Time.deltaTime;
                        if (m_fCurTime < m_fEndTime)
                        {
                            m_aiProperty.RegistRunningNode(Evaluate);
                            return BT.Return.Running;
                        }
                        else
                        {
                            m_fCurTime = 0;
                            ChangeStatus(Return.Evaluate, Return.Success);
                            return m_eState;
                        }
                    }
                    //  이러면 한칸감
                    return BT.Return.Success;
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTDirMove clone = (VSBTDirMove)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTDirMove>();
            

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTDirMove clone = (VSBTDirMove)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag()
                {
                    NodeTag = NodeTag.Act;
                }
            }
        }

    }
}