﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using BT.Enum;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace Task
    {
        namespace Action
        {
            public class VSBTBoolSwitch : BT.Virtual.VSBTTask
            {
                [SerializeField] private bool m_bSwitch = true;
                public VSBTBoolSwitch()
                {
                    m_wndNodeRect.width = 270;
                    m_wndNodeRect.height = 130;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTBoolSwitch clone = (VSBTBoolSwitch)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_bSwitch = m_bSwitch;
                    return clone;
                }

                public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
                {
                    VSBTBoolSwitch clone = (VSBTBoolSwitch)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_bSwitch = GUILayout.Toggle(m_bSwitch, "TurnTrue", GUILayout.Width(150));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    if(m_eState != Return.Evaluate)
                    {
                        return m_eState;
                    }
                    if(m_bSwitch)
                    {
                        ChangeStatus(Return.Evaluate, Return.Success);
                    }
                    else
                    {
                        ChangeStatus(Return.Evaluate, Return.Fail);
                    }
                    return m_eState;
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTBoolSwitch clone = (VSBTBoolSwitch)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTBoolSwitch>();
               

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Action.VSBTBoolSwitch clone = (VSBTBoolSwitch)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag()
                {
                    NodeTag = NodeTag.Act;
                }
            }
        }
    }
}