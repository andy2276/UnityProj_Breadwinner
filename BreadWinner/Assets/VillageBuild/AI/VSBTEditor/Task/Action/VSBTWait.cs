﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;


namespace BT
{
    namespace Task
    {
        namespace Action
        {
            public class VSBTWait : VSBTTask
            {
                [SerializeField] float m_fWaitTime = 0;
                float m_fCurTime = 0;
                public VSBTWait()
                {
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 180;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTWait clone = (VSBTWait)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_fWaitTime = m_fWaitTime;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTWait clone = (VSBTWait)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("WaitTime", GUILayout.Width(70));
                    m_fWaitTime = EditorGUILayout.FloatField(m_fWaitTime, GUILayout.Width(100));
                    EditorGUILayout.EndHorizontal();
                    
                }

                public override Return Evaluate()
                {
                    if(BT.Return.Evaluate != m_eState)
                    {
                        return m_eState;
                    }
                    if(m_fCurTime < m_fWaitTime)
                    {
                        m_fCurTime += Time.deltaTime;
                        m_aiProperty.RegistRunningNode(Evaluate);
                        return BT.Return.Running;
                    }
                    else
                    {
                        m_fCurTime = 0;
                        return ChangeStatus(Return.Evaluate, Return.Success);
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTWait clone = (VSBTWait)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTWait>();
               
                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                VSBTWait clone = (VSBTWait)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag() => NodeTag = Enum.NodeTag.Act;
               
            }


        }
    }
}

