﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace Task
    {
        namespace Condition
        {
            
            public class VSBTCoolTime : VSBTTask
            {
                [SerializeField] int m_nCoolTimeIdx = 0;
                [SerializeField] bool m_bUseCool = true;    //  이걸 만나면 무조건 사용으로 바꿈

                public VSBTCoolTime()
                {
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 170;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTCoolTime clone = (VSBTCoolTime)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_nCoolTimeIdx = m_nCoolTimeIdx;
                    clone.m_bUseCool = m_bUseCool;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTCoolTime clone = (VSBTCoolTime)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_nCoolTimeIdx = EditorGUILayout.IntField("CoolTimeIdx:", m_nCoolTimeIdx, GUILayout.Width(200));
                    m_bUseCool = EditorGUILayout.Toggle("UseCheck", m_bUseCool, GUILayout.Width(200));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {

                    if(m_aiProperty.IsCoolTimeOn(m_nCoolTimeIdx))
                    {
                       
                        if(m_bUseCool)
                        {
                            m_aiProperty.UseCoolTime(m_nCoolTimeIdx);
                        }
                        return Return.Success;
                    }

                    return Return.Fail;
                       
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTCoolTime clone = (VSBTCoolTime)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTCoolTime>();
              

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                VSBTCoolTime clone = (VSBTCoolTime)CopyNewNode(_parent);
                    DefaultTastSave(clone, _parent);
                    _list.Add(clone);
                    return clone;
                }

                protected override void SetNodeTag() => NodeTag = Enum.NodeTag.Cdt;
                
            }

        }
    }
}

