﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;

namespace BT
{
    namespace Task
    {
        namespace Condition
        {
            public class VSBTDeath : Virtual.VSBTTask
            {
                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTDeath clone = (VSBTDeath)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTDeath clone = (VSBTDeath)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                }

                public override Return Evaluate()
                {
                    if(m_aiProperty.Status.AiHp <= 0)
                    {
                        return BT.Return.Success;
                    }
                    else
                    {
                        return BT.Return.Fail;
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTDeath clone = (VSBTDeath)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTDeath>();

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTDeath clone = (VSBTDeath)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }
                protected override void SetNodeTag() => NodeTag = BT.Enum.NodeTag.Cdt;
               
            }

        }
    }

}
