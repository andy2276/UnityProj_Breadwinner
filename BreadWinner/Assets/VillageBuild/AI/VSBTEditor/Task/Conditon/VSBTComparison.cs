﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using BT.Enum;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace Task
    {
        namespace Condition
        {

            public class VSBTComparison : Virtual.VSBTTask
            {
                [SerializeField] private BT.Enum.StatusVariable m_eDest = 0;
                //  <        >       >=      <=      ==      !=
                //  Under,   Over,   More,   less,   Same,   Not
                [SerializeField] private BT.Enum.DstCompScr m_eComp = Enum.DstCompScr.less;
                [SerializeField] private float m_fScr = 0;
                [SerializeField] private bool m_bPercent = false;
                private float m_fOrigin = 0;
                private float m_fPercent = 0;
                private bool m_bFirst = true;

                
                public VSBTComparison()
                {
                    m_wndNodeRect.width = 330;
                    m_wndNodeRect.height = 170;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBTComparison clone = (VSBTComparison)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_eDest = m_eDest;
                    clone.m_eComp = m_eComp;
                    clone.m_fScr = m_fScr;
                    clone.m_bPercent = m_bPercent;
                    return clone;
                }

                public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
                {
                    VSBTComparison clone = (VSBTComparison)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }
                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    EditorGUILayout.BeginVertical();
                    string strComp = "";
                    switch (m_eComp)
                    {
                        case Enum.DstCompScr.Under: strComp = " < "; break;
                        case Enum.DstCompScr.Over: strComp = " > "; break;
                        case Enum.DstCompScr.less: strComp = " <= "; break;
                        case Enum.DstCompScr.More: strComp = " >= "; break;
                        case Enum.DstCompScr.Same: strComp = " == "; break;
                        case Enum.DstCompScr.Not: strComp = " != "; break;
                        default: break;
                    }
                    m_bPercent = GUILayout.Toggle(m_bPercent, "IsPercent", GUILayout.Width(200));
                    string strSrc = m_fScr.ToString();
                    if (m_bPercent)
                    {
                        strSrc += " % ";
                    }
                    
                    EditorGUILayout.LabelField("if ( " + m_eDest.ToString() + strComp + strSrc + " ) ");
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginHorizontal();
                    m_eDest = (StatusVariable)EditorGUILayout.EnumPopup(m_eDest, GUILayout.Width(120));
                    EditorGUILayout.LabelField("Is", GUILayout.Width(15));
                    m_eComp = (DstCompScr)EditorGUILayout.EnumPopup(m_eComp, GUILayout.Width(70));
                    m_fScr = EditorGUILayout.FloatField(m_fScr, GUILayout.Width(70));
                    EditorGUILayout.EndHorizontal();

                }

                public override Return Evaluate()
                {
                    if(BT.Return.Evaluate != m_eState)
                    {
                        return m_eState;
                    }
                    if(m_bFirst)
                    {
                        m_bFirst = false;
                        switch (m_eDest)
                        {
                            case Enum.StatusVariable.fHp: m_fOrigin = m_aiProperty.Status.AiHp; break;
                            case Enum.StatusVariable.fAttackDamage: m_fOrigin = m_aiProperty.Status.AiDamage; break;
                            case Enum.StatusVariable.fMoveSpeed: m_fOrigin = m_aiProperty.Status.AiMoveSpeed; break;
                            case Enum.StatusVariable.fAttackSpeed: m_fOrigin = m_aiProperty.Status.AiAttackSpeed; break;
                            case Enum.StatusVariable.fSerachRange: m_fOrigin = m_aiProperty.Status.AiSearchRange; break;
                            case Enum.StatusVariable.fAttackRange: m_fOrigin = m_aiProperty.Status.AiAttackRange; break;
                            case Enum.StatusVariable.fNearRange: m_fOrigin = m_aiProperty.Status.AiNearRange; break;
                            default: break;
                        }
                        m_fPercent = m_fScr * 0.01f;
                    }
                    float dest = 0;
                    
                    switch (m_eDest)
                    {
                        case Enum.StatusVariable.fHp:dest = m_aiProperty.Status.AiHp;break;
                        case Enum.StatusVariable.fAttackDamage:dest = m_aiProperty.Status.AiDamage;break;
                        case Enum.StatusVariable.fMoveSpeed:dest = m_aiProperty.Status.AiMoveSpeed;break;
                        case Enum.StatusVariable.fAttackSpeed:dest = m_aiProperty.Status.AiAttackSpeed;break;
                        case Enum.StatusVariable.fSerachRange: dest = m_aiProperty.Status.AiSearchRange;break;
                        case Enum.StatusVariable.fAttackRange:dest = m_aiProperty.Status.AiAttackRange;break;
                        case Enum.StatusVariable.fNearRange:dest = m_aiProperty.Status.AiNearRange; break;
                        default: break;
                    }
                    float fEndDst = dest;
                    float fEndScr = m_fScr; 
                    if (m_bPercent)
                    {
                        fEndDst = dest / m_fOrigin;
                        fEndScr = m_fPercent;
                    }
                    bool bBoolean = true;
                    switch (m_eComp)
                    {
                        case Enum.DstCompScr.Under:bBoolean = (fEndDst < fEndScr) ? true : false;break;
                        case Enum.DstCompScr.Over: bBoolean = (fEndDst > fEndScr) ? true : false; break;
                        case Enum.DstCompScr.less: bBoolean = (fEndDst <= fEndScr) ? true : false; break;
                        case Enum.DstCompScr.More: bBoolean = (fEndDst >= fEndScr) ? true : false; break;
                        case Enum.DstCompScr.Same: bBoolean = (fEndDst == fEndScr) ? true : false; break;
                        case Enum.DstCompScr.Not: bBoolean = (fEndDst != fEndScr) ? true : false; break;
                        default: break;
                    }
                    if(bBoolean)
                    {
                        ChangeStatus(Return.Evaluate, Return.Success);
                        return Return.Success;
                    }
                    else
                    {
                        ChangeStatus(Return.Evaluate, Return.Fail);
                        return Return.Fail;
                    }

                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTComparison clone = (VSBTComparison)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTComparison>();

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBTComparison clone = (VSBTComparison)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }

                protected override void SetNodeTag()
                {
                    m_eNodeTag = NodeTag.Cdt;
                }
            }
        }
    }
}