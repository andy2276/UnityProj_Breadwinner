﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;
using BT.Enum;

namespace BT
{
    namespace Task
    {
        namespace Condition
        {
            public class VSBTIsInRange : BT.Virtual.VSBTTask
            {
                [SerializeField] private float m_fCompRange = 0;
                [SerializeField] private bool m_bDefaultRange = true;  //  true == Deafult, false == custom;
                [SerializeField] private DefaultRange m_eDefaultRange = DefaultRange.SearchRange;
                private float m_fInputCompRange = 0;

                public VSBTIsInRange()
                {
                    
                    m_wndNodeRect.width = 300;
                    m_wndNodeRect.height = 180;
                }

                public float CompRange { get { return m_fCompRange; } set { m_fCompRange = value; } }

              
               

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    BT.Task.Condition.VSBTIsInRange clone = (VSBTIsInRange)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultTaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_fCompRange = m_fCompRange;
                    clone.m_bDefaultRange = m_bDefaultRange;
                    clone.m_eDefaultRange = m_eDefaultRange;
                    return clone;
                }

                public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
                {
                    VSBTIsInRange clone = (VSBTIsInRange)CopyNewNode(_parent);
                    _list.Add(clone);
                    
                    return clone;
                }

                public override Return Evaluate()
                {
                    if(m_eState != BT.Return.Evaluate)
                    {
                        return m_eState;
                    }
                    if (m_aiProperty == null || m_aiProperty.Target == null)
                    {
                        ChangeStatus(Return.Evaluate, Return.Fail);
                        return m_eState;
                    }
                    if(m_bDefaultRange)
                    {
                        switch (m_eDefaultRange)
                        {
                            case DefaultRange.SearchRange:
                                m_fCompRange = m_aiProperty.Status.AiSearchRange;
                                break;
                            case DefaultRange.AttackRange:
                                m_fCompRange = m_aiProperty.Status.AiAttackRange;
                                break;
                            case DefaultRange.NearRange:
                                m_fCompRange = m_aiProperty.Status.AiNearRange;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        m_fCompRange = m_fInputCompRange;
                    }
                    Vector3 targetPos = m_aiProperty.Target.transform.position;
                    Vector3 mePos = m_aiProperty.transform.position;
                    float dist = Vector3.Distance(targetPos, mePos);
                    if (dist <= m_fCompRange)
                    {
                        ChangeStatus(Return.Evaluate, Return.Success);
                        return Return.Success;
                    }
                    else
                    {
                        ChangeStatus(Return.Evaluate, Return.Fail);
                        return Return.Fail;
                    }

                    
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Condition.VSBTIsInRange clone = (VSBTIsInRange)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBTIsInRange>();
                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    BT.Task.Condition.VSBTIsInRange clone = (VSBTIsInRange)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }
                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultTaskWnd();
                    GUILayoutOption[] option = { GUILayout.Width(150), GUILayout.ExpandWidth(false) };
                    m_bDefaultRange = EditorGUILayout.BeginToggleGroup("IsDefaultRange", m_bDefaultRange);
                    m_eDefaultRange = (DefaultRange)EditorGUILayout.EnumPopup(m_eDefaultRange, GUILayout.Width(100));
                    EditorGUILayout.EndToggleGroup();
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("CompRange", GUILayout.Width(80));
                    m_fInputCompRange = EditorGUILayout.FloatField(m_fCompRange, option);
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();
                }

                protected override void SetNodeTag() => NodeTag = NodeTag.Cdt;
            }
        }
    }
}