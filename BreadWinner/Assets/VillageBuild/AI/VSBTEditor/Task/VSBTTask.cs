﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BT.Enum;
namespace BT
{
    namespace Virtual
    {
        public abstract class VSBTTask : BT.Virtual.VSBTNode
        {
            [SerializeField] protected bool m_bRepeat = false;

            public VSBTTask()
            {
                m_eNodeCat = NodeCategory.Task;
                SetNodeTag();
            }
            protected abstract void SetNodeTag();
           
            protected void DefaultTastSave(VSBTNode _clone,VSBTNode _parent = null)
            {
                if(_parent != null)
                {
                    BT.Virtual.VSBTComposite parent = (BT.Virtual.VSBTComposite)_parent;
                    _clone.BTPath = parent.BTPath;
                    _clone.hideFlags = HideFlags.None;
                    parent.AddChild(_clone); 
                    AssetDatabase.AddObjectToAsset(_clone, parent.BTPath);
                }
                //  Task는 테스크 대로 세이브 가능하게 에디터를 고쳐야함!
            }

            protected void NodeWindow_DefaultTaskWnd()
            {
                EditorGUILayout.BeginVertical();
                m_bRepeat = GUILayout.Toggle(m_bRepeat, "ReturnRepeat", GUILayout.Width(200));
                EditorGUILayout.EndVertical();
            }
            protected void DefaultTaskCopy(BT.Virtual.VSBTTask _clone)
            {
                
                _clone.m_bRepeat = m_bRepeat;
            }
            protected BT.Return ChangeStatus(BT.Return _KeepStatus,BT.Return _changeStatus)
            {
                if(!m_bRepeat)
                {
                    m_eState = _changeStatus;
                }
                else
                {
                    m_eState = _KeepStatus;
                }
                return _changeStatus;
            }

            public override void ResetTree(Return _state = Return.Evaluate)
            {
                m_eState = _state;
            }
            public override VSBTNode LoadAI(List<VSBTNode> _list, VSBTNode _parent = null) => LoadBT(_list, _parent);

        }

    }
}