﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class VSBTEditorInit : MonoBehaviour
{
    public static void LoadingMenu()
    {
        #region Composite
        BT.VSBTEditor.RegistMenuItem<BT.Composite.VSBTSequence>("Sequence");
        BT.VSBTEditor.RegistMenuItem<BT.Composite.VSBTSelection>("Selection");
        BT.VSBTEditor.RegistMenuItem<BT.Composite.VSBTProcedure>("Procedure");
        #endregion
        #region AI

        #region Condition
        BT.VSBTEditor.RegistMenuItem<BT.Task.Condition.VSBTIsInRange>("CdtInTargetRange");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Condition.VSBTComparison>("CdtDestCompScr");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Condition.VSBTDeath>("cdtDeath");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Condition.VSBTDamaged>("cdtDamaged");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Condition.VSBTIsLook>("cdtIsLook");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Condition.VSBTCoolTime>("ActCoolTime");
        #endregion
        #region Action
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTTargetTurn>("ActTargetTurn");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTTargetMove>("ActTargetMove");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTDirMove>("ActDirMove");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTBoolSwitch>("ActBoolSwitch");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTDebugText>("ActDebugText");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTAnimation>("ActAnimation");;
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTJustTargetTurn>("ActJusTargetTurn");
        BT.VSBTEditor.RegistMenuItem<BT.Task.Action.VSBTWait>("ActWait");
        
        #endregion
        #endregion//AI
        #region BattleArea
        BT.VSBTEditor.RegistMenuItem<BT.BA.Cdt.VSBAComparison>("cdtComparison");
        BT.VSBTEditor.RegistMenuItem<BT.BA.Cdt.VSBAClearCnt>("cdtCrearCnt");
        BT.VSBTEditor.RegistMenuItem<BT.BA.Spon.VSBASponning>("cdtSponning");
        BT.VSBTEditor.RegistMenuItem<BT.BA.Cdt.VSBAAllEvent>("cdtAllEvent");
        #endregion

    }
}
