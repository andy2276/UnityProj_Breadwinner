﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BT
{
    namespace EditItem
    {
        public class ChildSlot : ScriptableObject
        {
            public BT.Virtual.VSBTNode Node = null;
            public Rect Slot = new Rect();
            public Rect PixSlot = new Rect();
            public Color lineColor = new Color();
        }
    }
}