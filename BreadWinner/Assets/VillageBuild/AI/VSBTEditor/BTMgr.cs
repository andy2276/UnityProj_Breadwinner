﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTMgr : MonoBehaviour
{
    // Start is called before the first frame update
    static private BTMgr m_SingInst = new BTMgr();
    static public BTMgr Inst { get { return m_SingInst; } }
    [SerializeField] private GameObject m_player = null;
    public GameObject Player { get { return m_player; } set { m_player = value; } }

    private void Awake()
    {
        DontDestroyOnLoad(m_SingInst);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
