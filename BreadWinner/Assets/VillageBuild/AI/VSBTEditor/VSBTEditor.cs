﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;
using BT.Enum;

namespace BT
{
    namespace EditItem
    {
        public delegate Virtual.VSBTNode MakeNodeFunc(object e);
 
    }

    public class VSBTEditor : EditorWindow
    {
        #region Variable

        static List<BT.Virtual.VSBTNode> g_lstNodes = new List<BT.Virtual.VSBTNode>();
        List<BT.Virtual.VSBTNode> m_lstLoadNodes = new List<BT.Virtual.VSBTNode>();
        static List<BT.EditItem.SaveMenuItem> g_lstMenuItems = new List<BT.EditItem.SaveMenuItem>();
        static int m_classId = 0;
        public static Color[] g_colorArray = { Color.red, Color.green, Color.blue, Color.magenta, Color.cyan, Color.yellow };
        public static Color[] g_nodeColors = {  new Color(204* 0.0039f, 114 * 0.0039f,61 * 0.0039f), new Color(255f * 0.0039f, 126f * 0.0039f, 126f * 0.0039f),
                                                new Color(237* 0.0039f, 210 * 0.0039f,0f * 0.0039f), new Color(134f * 0.0039f, 229f * 0.0039f, 127f * 0.0039f),
                                                new Color(79* 0.0039f, 201f * 0.0039f,222f * 0.0039f), new Color(67f * 0.0039f, 116f * 0.0039f, 217f * 0.0039f),
                                                new Color(108* 0.0039f, 195 * 0.0039f,210f * 0.0039f),new Color(35* 0.0039f, 164 * 0.0039f,26 * 0.0039f),
                                                new Color(99* 0.0039f, 36 * 0.0039f,189 * 0.0039f)
                                             };
        List<BT.Virtual.VSBTNode> m_lstSaveNodes = new List<Virtual.VSBTNode>();
        Rect m_wndViewRect;
        Rect m_wndFullRect;

        Vector2 m_v2RectSize;
        Vector2 m_v2MousePos;
        Vector2 m_v2ScrollPos;
        Vector2 m_v2ScrollMousePos;
        BT.Virtual.VSBTNode m_selectNode;
        BT.Virtual.VSBTNode m_drawNode;
        bool m_bIsChild = false;    //  true : child, false : parent
        bool m_bDrawLine = false;
        bool m_bClickWnd = false;
        static bool once = false;
        //  기능 스위치
        bool m_bTestTree = false;
        bool m_bSaveAsset = false;
        bool m_bRepaint = false;
        bool m_bDelAll = false;
        #region EditorLayoutOptions
       
        BT.Virtual.VSBTNode         m_root = null;
        Object                      m_loadAsset = null;
        AnimBool m_ShowExtraFields;

        private static float m_fCurveForce = 50.0f;
        public static float CurveForce { get { return m_fCurveForce; } }
        private static string m_strBTName = "New BT Title";
        public static string BTName { get { return m_strBTName; } }
        private static string m_strFullPath = "Assets/Behaivor/";
        private string m_strFilePath = "Assets/VillageBuild/";
        private string m_strFolderPath = "AI/Behavior/";
        public static string BTPath { get { return m_strFullPath; } }
        bool m_bLoadAsset = false;
        string m_strLoadPath = "";
        #endregion
        #region Enums

        public enum ModifyOrder
        {
            //  GetParent , SetChild
            SetRoot, DrawLine_SetParent, DrawLine_SetChild, Copy, Delete
        }
        public enum DrawOrder
        {
            cancel
        }
        #endregion
        #endregion
        #region Init
        [MenuItem("Window/VisualBTEditor")]
        static void Init()
        {
            VSBTEditor vSBTEditor = (VSBTEditor)EditorWindow.GetWindow(typeof(VSBTEditor));
            vSBTEditor.minSize = new Vector2(800, 600);
           
        }
        #endregion
        #region StaticFunc
        public static int RegistNodeClassID()// 메뉴 아이디를 가져오는 것
        {
            return m_classId++;
        }
        private static void RegistMenuItem(NodeCategory _cat,NodeTag _tag, string _name, System.Type _type, BT.EditItem.MakeNodeFunc _MakeFunc)// 내부적으로 만드는것
        {
            string cat = "";
            switch (_cat)
            {
                case NodeCategory.Composite:
                    cat = "Composite/";
                    break;
                case NodeCategory.Task:
                    cat = "Task/";
                    switch (_tag)
                    {
                        case NodeTag.Default:
                            break;
                        case NodeTag.Seq:
                            break;
                        case NodeTag.Sel:
                            break;
                        case NodeTag.Prc:
                            break;
                        case NodeTag.Cdt:
                            cat += "Ai/Condition/";
                            break;
                        case NodeTag.Act:
                            cat += "Ai/Action/";
                            break;
                        case NodeTag.BatCdt:
                            cat += "Battel/Condition/";
                            break;
                        case NodeTag.BatSpon:
                            cat += "Battel/Spon/";
                            break;
                        case NodeTag.BatCenema:
                            cat += "Battel/Cenema/";
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    cat = "NewNode/";
                    break;
            }
            cat += _name;
            if(g_lstMenuItems.Find(n => n.NodePath == cat) != null)
            {
                Debug.Log("RegistMenuItem : <" + cat + "> is overlab");
                return;
            }
            BT.EditItem.SaveMenuItem s = new BT.EditItem.SaveMenuItem(_cat, cat, _type, _MakeFunc);
            g_lstMenuItems.Add(s);
        }

        public static void RegistNodeWnd(BT.Virtual.VSBTNode _node)
        {
            if(g_lstNodes.Find(n => n == _node))
            {
                Debug.Log("RegistNodeWnd is" + _node.Title + " overLap");
                return;
            }
            g_lstNodes.Add(_node);
        }
        public static void ApplyBehavior(GameObject _target)
        {
            BehaviorAI ai = _target.GetComponent<BehaviorAI>();
            if(ai == null)
            {
                ai = _target.AddComponent<BehaviorAI>();
            }
        }
        public static void RegistMenuItem<T>(string _title) where T : BT.Virtual.VSBTNode   
        {
            T temp = CreateInstance<T>();
            temp.Title = _title;
            RegistMenuItem(temp.Category, temp.NodeTag, temp.Title, temp.GetType(), temp.MakeNode);
        }//  외부적으로 메뉴아이템 등록
        public static BT.Virtual.VSBTNode LoadBTAsset(Object _bt,List<BT.Virtual.VSBTNode> _lst)
        {
            string loadPath = "";
            BT.Virtual.VSBTNode root = null;
            _lst.Clear();
            loadPath = AssetDatabase.GetAssetPath(_bt);
            Object[] lstObjs = AssetDatabase.LoadAllAssetsAtPath(loadPath);
            List<BT.Virtual.VSBTNode> lst = lstObjs.OfType<BT.Virtual.VSBTNode>().ToList();
            lst.Reverse();

            for(int i = 0; i < lst.Count;++i)
            {
                if(lst[i].IsRoot)
                {
                    root = lst[i].LoadAI(_lst);
                    if(!_lst.Contains(root))
                    {
                        lst.Insert(0, root);
                    }
                    break;
                }
            }
            return root;
        }
        #endregion
        #region GUI
        #region BagicFunc
        //  다음에 해야하는거는 이걸 토대로 렉트를 만들어내고 그 를 이동시키거나 하는것이다!
        private void OnEnable()
        {
            m_v2RectSize = new Vector2(300 , 200);
            m_wndViewRect = new Rect(0, 0, m_v2RectSize.x, m_v2RectSize.y);
            m_wndFullRect = new Rect(0, 0, m_v2RectSize.x * 50, m_v2RectSize.y * 50);
            m_ShowExtraFields = new AnimBool(true);
            VSBTEditorInit.LoadingMenu();
        }
        private void OnGUI()
        {
            Event e = Event.current;
            DrawRepaint();
            m_v2MousePos = e.mousePosition;
            UserInput(e);
            DrawWindow();
            NodeEditorLayout();
        }
        #endregion
        public Color NodeColor(BT.Virtual.VSBTNode _node)
        {
            switch (_node.Category)
            {
                case NodeCategory.Composite:
                    switch (_node.NodeTag)
                    {
                        case NodeTag.Default:return Color.grey;
                        case NodeTag.Seq:return g_nodeColors[0];
                        case NodeTag.Sel:return g_nodeColors[1];
                        case NodeTag.Prc:return g_nodeColors[2];
                        case NodeTag.Cdt:return g_nodeColors[3];
                        case NodeTag.Act:return g_nodeColors[4];
                        case NodeTag.BatCdt: return g_nodeColors[6];
                        case NodeTag.BatSpon: return g_nodeColors[7];
                        case NodeTag.BatCenema: return g_nodeColors[8];
                        default:return          g_nodeColors[5];
                    }
                    
                case NodeCategory.Task:
                    switch (_node.NodeTag)
                    {
                        case NodeTag.Default: return Color.grey;
                        case NodeTag.Seq: return g_nodeColors[0];
                        case NodeTag.Sel: return g_nodeColors[1];
                        case NodeTag.Prc: return g_nodeColors[2];
                        case NodeTag.Cdt: return g_nodeColors[3];
                        case NodeTag.Act: return g_nodeColors[4];
                        case NodeTag.BatCdt: return g_nodeColors[6];
                        case NodeTag.BatSpon: return g_nodeColors[7];
                        case NodeTag.BatCenema: return g_nodeColors[8];
                        default: return g_nodeColors[5];
                    }

                default:return Color.white;
            }

        }
        void DrawWindow()// 윈도우를 그리는 것
        {
            m_v2ScrollPos = GUI.BeginScrollView(new Rect(0, 0, position.width, position.height), m_v2ScrollPos, m_wndFullRect);
            BeginWindows();
            Color color = new Color();
            //  라인을 그린다
            for (int i = 0; i < g_lstNodes.Count; ++i)
            {
                color = NodeColor(g_lstNodes[i]);
                GUI.color = color;
                g_lstNodes[i].NodeRect = GUI.Window(i, g_lstNodes[i].NodeRect, DrawNode, g_lstNodes[i].Title);
                GUI.color = Color.white;
            }
            foreach (BT.Virtual.VSBTNode n in g_lstNodes)
            {
                n.DrawCurve();
            }
            //  노드 렉트를 그린다
            EndWindows();
            GUI.EndScrollView();
            m_v2ScrollMousePos = m_v2MousePos + m_v2ScrollPos;
           
        }
        void DrawNode(int _id)
        {
            g_lstNodes[_id].DrawWindow();
            GUI.DragWindow();
        }
        void NodeEditorLayout()//   editor창에 표시되는 정보
        {
            EditorGUILayout.LabelField("Path : " + m_strFullPath);
            EditorGUILayout.BeginHorizontal();
            m_strFilePath = EditorGUILayout.TextField(m_strFilePath, GUILayout.Width(100));
            m_strFolderPath = EditorGUILayout.TextField(m_strFolderPath, GUILayout.Width(100));
            m_strFullPath = m_strFilePath + m_strFolderPath;
            EditorGUILayout.EndHorizontal();
            m_loadAsset = EditorGUILayout.ObjectField("LoadAsset", m_loadAsset, typeof(Object), true,GUILayout.Width(200));
            EditorGUILayout.LabelField("Behaivor Title : " + m_strBTName);
            m_strBTName = EditorGUILayout.TextField(m_strBTName, GUILayout.Width(200));
            m_ShowExtraFields.target = EditorGUILayout.ToggleLeft("Behavior 정보", m_ShowExtraFields.target,GUILayout.Width(100));
            if (m_bDelAll)
            {
                ResetEditor();
            }
            else if(m_bSaveAsset)
            {
                SaveAsset();
            }
            else if(m_bLoadAsset)
            {
                LoadAsset();
            }
            if (EditorGUILayout.BeginFadeGroup(m_ShowExtraFields.faded))
            {
                EditorGUILayout.BeginVertical();
                if(m_root != null)
                {
                    EditorGUILayout.LabelField("Root : "+m_root.Title,GUILayout.Width(300));
                }
                if(m_bTestTree)
                {
                    TestTree();
                }
                EditorGUILayout.LabelField("라인 곡선 강도");
                m_fCurveForce = EditorGUILayout.FloatField(m_fCurveForce, GUILayout.Width(200));
                EditorGUILayout.BeginHorizontal();
                m_bSaveAsset = GUILayout.Button("SaveAsset", GUILayout.Width(90));
                m_bLoadAsset = GUILayout.Button("LoadAsset", GUILayout.Width(90));
             //   m_bApplyObj = GUILayout.Button("ApplyObject", GUILayout.Width(90));
                EditorGUILayout.EndHorizontal();
                // m_bTestTree = GUILayout.Button("TestTree", GUILayout.Width(90));
                m_bDelAll = GUILayout.Button("DeleteAll", GUILayout.Width(90));
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndFadeGroup();
        }
        void TestTree()
        {
            if(m_root != null)
            {
                m_root.TestTreeDebug(0);
                
            }
        }
        void SaveAsset()//  세이브 에셋
        {
            if (m_root != null)
            {
                m_lstSaveNodes.Clear();
                m_root.SaveBT(m_lstSaveNodes,null);
            }
        }
        void LoadAsset()//  에셋로드
        {
            if(m_loadAsset != null)
            {
                m_lstLoadNodes.Clear();
                m_strLoadPath = AssetDatabase.GetAssetPath(m_loadAsset);
                Object[] lstObjs = AssetDatabase.LoadAllAssetsAtPath(m_strLoadPath);
                List<BT.Virtual.VSBTNode> lst = lstObjs.OfType<BT.Virtual.VSBTNode>().ToList();
                lst.Reverse();
                foreach(var n in lst)
                {
                    if(n.IsRoot)
                    {
                        n.LoadBT(m_lstLoadNodes);
                        break;
                    }
                }
            }
            foreach(var n in m_lstLoadNodes)
            {
                RegistNodeWnd(n);
            }
        }
        void ResetEditor()//    에디터 리셋, 문제가 많음
        {
            m_root = null;
            m_loadAsset = null;
            for(int i = g_lstNodes.Count -1; i >= 0;--i)
            {
                Modify_DeleteNode(g_lstNodes[i]);
            }
        }
        void ApplyObject()//    사용안함
        {
           
        }
        void UserInput(Event e)//   유저의 입력을 처리하는 함수
        {
            if (!m_bDrawLine)
            {
                if (e.button == 1)
                {
                    if (e.type == EventType.MouseDown)
                    {
                        ClickEditRight(e);
                    }
                }
                else if (e.button == 0)
                {
                    if (e.type == EventType.MouseDown)
                    {
                        ClickEditLeft(e);
                    }
                }
            }
            else    //  Drawing
            {
                if (e.button == 1 && e.type == EventType.MouseDown)
                {
                    m_bRepaint = false;
                    //  그리는 도중 옵션
                    ClickDrawRight(e);
                }
                else if (e.button == 0 && e.type == EventType.MouseDown)
                {
                    m_bRepaint = false;
                    if (e.type == EventType.MouseDown)
                    {
                        //  그걸 선택
                        ClickDrawLeft(e);
                    }
                }
                else
                {
                    m_bRepaint = true;
                }

            }
        }
        void DrawRepaint()
        {
            if (m_bRepaint)
            {
                DrawCurveLine();
            }
            Repaint();
        }
        bool SearchSelectNode() //  마우스가 위치한 노드를 찾는거
        {
            m_selectNode = null;
            for (int i = 0; i < g_lstNodes.Count; ++i)
            {
                if (g_lstNodes[i].NodeRect.Contains(m_v2ScrollMousePos))
                {
                    m_selectNode = g_lstNodes[i];
                    return true;
                }
            }
            return false;
        }
        void ClickEditRight(Event e)//  에딧중 오른쪽 클릭을 했을 때
        {
            m_bClickWnd = SearchSelectNode();
            if (!m_bClickWnd)
            {
                OnMenu_AddNewNodeType(e);
            }
            else
            {
                if (m_selectNode.Category == NodeCategory.Composite)
                {
                    OnMenu_ModifyCompositeNode(e);
                }
                else if (m_selectNode.Category == NodeCategory.Task)
                {
                    OnMenu_ModifyTaskNode(e);
                }
            }

        }
        void ClickEditLeft(Event e)//   에딧중 왼쪽을 클릭 했을때
        {
            m_bClickWnd = SearchSelectNode();
        }
        void ClickDrawRight(Event e)//  드로우중 오른쪽 클릭을 햇을때
        {
            OnMenu_DrawingNodeLine(e);
        }
        void ClickDrawLeft(Event e)//   드로우중 왼쪽클릭을 햇을때 
        {
            if (m_drawNode != null)
            {
                if (m_bIsChild)
                {
                    RegistParent();
                }
                else
                {
                    RegistChild();
                }
                m_bDrawLine = false;
            }
        }
        void RegistParent()//   부모를 등록할때 사용
        {
            bool bIsIn = SearchSelectNode();
            if (m_drawNode == m_selectNode) return;
            if (bIsIn)
            {
                BT.Virtual.VSBTComposite composite = (BT.Virtual.VSBTComposite)m_selectNode;
                composite.AddChild(m_drawNode, m_v2ScrollMousePos);
                m_drawNode = null;
                m_selectNode = null;
            }

        }
        void RegistChild()//    자식을 등록할때 사용
        {
            bool bIsIn = SearchSelectNode();
            if (m_drawNode == m_selectNode) return;
            if (bIsIn)
            {
                BT.Virtual.VSBTComposite composite = (BT.Virtual.VSBTComposite)m_drawNode;
                UnconnectedNode(m_selectNode);
                composite.AddEditChild(m_selectNode);
                m_drawNode = null;
                m_selectNode = null;
            }

        }
        void OnMenu_AddNewNodeType(Event e)//   wnd를 오른쪽 클릭시 나오는 메뉴
        {
            GenericMenu menu = new GenericMenu();
            //
            menu.AddSeparator("");
            for (int i = 0; i < g_lstMenuItems.Count; ++i)
            {
                menu.AddItem(new GUIContent(g_lstMenuItems[i].NodePath), false, CallbackAddNodeContext, g_lstMenuItems[i]);
            }

            menu.ShowAsContext();
            e.Use();
        }
        void OnMenu_ModifyCompositeNode(Event e)//  composite 노드를 오른쪽 클릭시 나오는 메뉴
        {
            GenericMenu menu = new GenericMenu();
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("SetRoot"), false, CallbackModifyNodeContext, ModifyOrder.SetRoot);
            menu.AddItem(new GUIContent("DrawLine_SetParent"), false, CallbackModifyNodeContext, ModifyOrder.DrawLine_SetParent);
            menu.AddItem(new GUIContent("DrawLine_SetChild"), false, CallbackModifyNodeContext, ModifyOrder.DrawLine_SetChild);
            menu.AddItem(new GUIContent("Copy"), false, CallbackModifyNodeContext, ModifyOrder.Copy);
            menu.AddItem(new GUIContent("Delete"), false, CallbackModifyNodeContext, ModifyOrder.Delete);
            menu.ShowAsContext();
            e.Use();

        }
        void OnMenu_ModifyTaskNode(Event e)//   task 노드를 오른쪽 클릭시 나오는 메뉴
        {
            GenericMenu menu = new GenericMenu();
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("DrawLine_SetParent"), false, CallbackModifyNodeContext, ModifyOrder.DrawLine_SetParent);
            menu.AddItem(new GUIContent("Copy"), false, CallbackModifyNodeContext, ModifyOrder.Copy);
            menu.AddItem(new GUIContent("Delete"), false, CallbackModifyNodeContext, ModifyOrder.Delete);
            menu.ShowAsContext();
            e.Use();

        }
        void OnMenu_DrawingNodeLine(Event e)//  노드 라인을 그리고 있을때 오른쪽 클릭시 나오는 메뉴
        {
            GenericMenu menu = new GenericMenu();
            menu.AddSeparator("");
            menu.AddItem(new GUIContent("Cancel"), false, CallbackDrawNodeContext, DrawOrder.cancel);
            menu.ShowAsContext();
            e.Use();
        }

        void DrawCurveLine()//   노드 라인을 그리라는 명령어
        {
            Color color = Color.black;
            Rect select = m_selectNode.NodeRect;
            float dir = 1;
            if (select.x + select.width * 0.5f > m_v2MousePos.x)
            {
                dir = -1;
            }
            Vector3 startPos = new Vector3((select.x + select.width * 0.5f) - m_v2ScrollPos.x, (select.y + select.height * 0.5f) - m_v2ScrollPos.y);
            Vector3 endPos = new Vector3(m_v2MousePos.x, m_v2MousePos.y, 0);
            Vector3 startTan = startPos + Vector3.left * m_fCurveForce * dir;
            Vector3 endTan = endPos + Vector3.right * m_fCurveForce * dir;
            Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 3);
        }
        void CallbackAddNodeContext(object e)// 노드를 추가 할때 나오는 메뉴
        {

            EditItem.SaveMenuItem type = (EditItem.SaveMenuItem)e;
            BT.Virtual.VSBTNode n = null;
            n = type.NodeFunc(e);
            if (n != null)
            {
                g_lstNodes.Add(n);
                n.Title = n.GetType().ToString();

                if (n.NodeRect.width == 0 || n.NodeRect.height == 0)
                {
                    n.NodeRect = new Rect(m_v2ScrollMousePos.x, m_v2ScrollMousePos.y, m_wndViewRect.width, m_wndViewRect.height);
                }
                else
                {
                    n.NodeRect = new Rect(m_v2ScrollMousePos.x, m_v2ScrollMousePos.y, n.NodeRect.width, n.NodeRect.height);
                }
               
                n.BTObj = null;
                n.Category = type.Category;
            }
        }
        void CallbackModifyNodeContext(object e)
        {
            ModifyOrder o = (ModifyOrder)e;
            switch (o)
            {
                case ModifyOrder.SetRoot:
                    SearchSelectNode();
                    if(m_root != null)
                    {
                        m_root.IsRoot = false;
                        m_root = null;
                    }
                    m_root = m_selectNode;
                    m_root.IsRoot = true;
                    break;
                //  부모를 찾는것 자신은 자식이된다
                case ModifyOrder.DrawLine_SetParent:
                    SearchSelectNode();
                    Modify_SetParent(m_selectNode);
                    break;
                //  자식을 찾는것, 자신이 부모가 된다.
                case ModifyOrder.DrawLine_SetChild:
                    SearchSelectNode();
                    Modify_SetChild(m_selectNode);
                    break;
                case ModifyOrder.Copy:
                    SearchSelectNode();
                    Modify_CopyNode(m_selectNode);
                    break;
                case ModifyOrder.Delete:
                    SearchSelectNode();
                    Modify_DeleteNode(m_selectNode);
                    m_selectNode = null;
                    break;
                default:
                    break;
            }
        }
        void Modify_SetParent(BT.Virtual.VSBTNode _target)
        {
            UnconnectedNode(_target);
            m_bDrawLine = true;
            m_bIsChild = true;
            m_drawNode = m_selectNode;
        }
        void Modify_SetChild(BT.Virtual.VSBTNode _target)
        {
            UnconnectedNode(_target);
            m_bDrawLine = true;
            m_bIsChild = false;
            m_drawNode = m_selectNode;
        }
        void Modify_DeleteNode(BT.Virtual.VSBTNode _target)
        {
            UnconnectedNode(_target);
            g_lstNodes.Remove(_target);
            DestroyImmediate(_target);
        }
        void Modify_CopyNode(BT.Virtual.VSBTNode _target)
        {
            List<BT.Virtual.VSBTNode> lst = new List<Virtual.VSBTNode>();
            _target.CopyNode(lst);
            foreach(var n in lst)
            {
                n.Title = "Copy_" + n.Title;
                RegistNodeWnd(n);
            }

        }
        void CallbackDrawNodeContext(object e)
        {
            DrawOrder o = (DrawOrder)e;
            switch (o)
            {
                case DrawOrder.cancel:
                    m_bDrawLine = false;
                    break;
                default:
                    break;
            }

        }
        void UnconnectedNode(BT.Virtual.VSBTNode _target)
        {
            if (_target.Parent != null)
            {
                BT.Virtual.VSBTComposite p = (BT.Virtual.VSBTComposite)_target.Parent;
                p.DelChild(_target);
            }
        }
        #endregion



    }

}
