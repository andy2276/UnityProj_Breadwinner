﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BT.EditItem;

namespace BT
{
    namespace Enum
    {
        enum DefaultRange
        {
            SearchRange, AttackRange, NearRange
        }
        enum StatusVariable
        {
            fHp, fAttackDamage, fMoveSpeed, fAttackSpeed, fSerachRange, fAttackRange, fNearRange, End
        }
        enum DstCompScr
        {
            //  <        >       <=      =>      ==      !=
            Under, Over, less, More, Same, Not
        }
    }
    namespace EditItem
    {
        [System.Serializable]
        public class CoolTime
        {
            public string m_name = "new Skill";
            private bool m_bOnSkill = false;
            public float m_fCoolTime = 0;
            private float m_fCurTime = 0;
            public bool IsOnSkill { get => m_bOnSkill; set => m_bOnSkill = value; }
            public void CoolTimeTicToc()
            {
                if(m_fCurTime < m_fCoolTime)
                {
                    m_fCurTime += Time.deltaTime;
                }
                else
                {
                    m_bOnSkill = true;
                }
            }
            public void UseSkill()
            {
                if(m_bOnSkill)
                {
                    m_fCurTime = 0;
                    m_bOnSkill = false;
                }
            }
            public CoolTime CopyNew()
            {
                CoolTime time = new CoolTime();
                time.m_name = m_name;
                time.m_bOnSkill = m_bOnSkill;
                time.m_fCoolTime = m_fCoolTime;
                return time;
            }
        }
    }
}

[CreateAssetMenu(fileName = "AiData", menuName = "AiData", order = int.MaxValue)]
public class AIStatus : ScriptableObject
{
    #region Variable
    [SerializeField] public string m_strName = "New";
    [SerializeField] public float m_fHp = 100;
    [SerializeField] public float m_fAttackDamage = 20;
    [SerializeField] public float m_fMoveSpeed = 10;
    [SerializeField] public float m_fRotateSpeed = 5;
    [SerializeField] public float m_fAttackSpeed = 1;
    [SerializeField] public float m_fSerachRange = 150;
    [SerializeField] public float m_fAttackRange = 50;
    [SerializeField] public float m_fNearRange = 10;
    [SerializeField] private List<BT.EditItem.CoolTime> m_lstCoolTime;
    
    #endregion
    #region Interface
    public string AiName { get { return m_strName; } set { m_strName = value; } }
    public float AiHp { get { return m_fHp; } set { m_fHp = value; } }
    public float AiDamage { get { return m_fAttackDamage; } set { m_fAttackDamage = value; } }
    public float AiMoveSpeed { get { return m_fMoveSpeed; } set { m_fMoveSpeed = value; } }
    public float AiAttackSpeed { get { return m_fAttackSpeed; } set { m_fAttackSpeed = value; } }
    public float AiSearchRange { get { return m_fSerachRange; } set { m_fSerachRange = value; } }
    public float AiAttackRange { get { return m_fAttackRange; } set { m_fAttackRange = value; } }
    public float AiNearRange { get { return m_fNearRange; } set { m_fNearRange = value; } }

    public List<CoolTime> ListCoolTime { get => m_lstCoolTime; }
    public float RotateSpeed { get => m_fRotateSpeed; set => m_fRotateSpeed = value; }

    #endregion
    #region Func
    void Init()
    {
        if (m_lstCoolTime == null) return;
    }
    void Start()
    {
        Init();
    }
    public AIStatus CloneStatus()
    {
        AIStatus clone = CreateInstance<AIStatus>();
        clone.m_strName = m_strName;
        clone.m_fHp = m_fHp;
        clone.m_fAttackDamage = m_fAttackDamage;
        clone.m_fMoveSpeed = m_fMoveSpeed;
        clone.m_fAttackSpeed = m_fAttackSpeed;
        clone.m_fSerachRange = m_fSerachRange;
        clone.m_fAttackRange = m_fAttackRange;
        clone.m_fNearRange = m_fNearRange;
        if (clone.m_lstCoolTime == null)
        {
            clone.m_lstCoolTime = new List<CoolTime>();
        }
        for (int i = 0; i < m_lstCoolTime.Count;++i)
        {
           
            clone.m_lstCoolTime.Add(m_lstCoolTime[i].CopyNew());
        }
        return clone;
    }
    public static AIStatus operator +(AIStatus _lhs,AIStatus _rhs)
    {
        _lhs.m_strName += _rhs.m_strName;
        _lhs.m_fHp += _rhs.m_fHp;
        _lhs.m_fAttackDamage += _rhs.m_fAttackDamage;
        _lhs.m_fMoveSpeed += _rhs.m_fMoveSpeed;
        _lhs.m_fAttackSpeed += _rhs.m_fAttackSpeed;
        _lhs.m_fSerachRange += _rhs.m_fSerachRange;
        _lhs.m_fAttackRange += _rhs.m_fAttackRange;
        _lhs.m_fNearRange += _rhs.m_fNearRange;

        return _lhs;
    }
    public void AddState(AIStatus _rhs)
    {
       m_strName += _rhs.m_strName;
       m_fHp += _rhs.m_fHp;
       m_fAttackDamage += _rhs.m_fAttackDamage;
       m_fMoveSpeed += _rhs.m_fMoveSpeed;
       m_fAttackSpeed += _rhs.m_fAttackSpeed;
       m_fSerachRange += _rhs.m_fSerachRange;
       m_fAttackRange += _rhs.m_fAttackRange;
       m_fNearRange += _rhs.m_fNearRange;
    }
    public void AddTime(AIStatus _rhs)
    {
        if(_rhs.ListCoolTime.Count == 0)
        {
            return;
        }
        else if(_rhs.ListCoolTime.Count == m_lstCoolTime.Count)
        {
            return;
        }
        for (int i = 0; i < m_lstCoolTime.Count; ++i)
        {
            m_lstCoolTime[i].m_fCoolTime += _rhs.ListCoolTime[i].m_fCoolTime;
        }
    }

    #endregion
}
