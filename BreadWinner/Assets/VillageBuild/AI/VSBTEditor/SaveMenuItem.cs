﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BT.EditItem;
using BT.Enum;

namespace BT
{
    namespace EditItem
    {
      

        public class SaveMenuItem
        {
            private NodeCategory m_eCat;
            public NodeCategory Category { get { return m_eCat; } set { m_eCat = value; } }
            private string m_path;
            public string NodePath { get { return m_path; } set { m_path = value; } }
            private System.Type m_type;
            public System.Type NodeType { get { return m_type; } set { m_type = value; } }
            private MakeNodeFunc m_makeFunc;
            public MakeNodeFunc NodeFunc { get { return m_makeFunc; } set { m_makeFunc = value; } }

            public SaveMenuItem(NodeCategory _cat, string _path, System.Type _type, MakeNodeFunc _func)
            {
                m_eCat = _cat;
                m_path = _path;
                m_type = _type;
                m_makeFunc = _func;

                
            }
        }
    }
}