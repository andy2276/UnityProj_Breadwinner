﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BT.Enum;
namespace BT
{
    namespace Composite
    {
        public class VSBTSequence : BT.Virtual.VSBTComposite
        {
            public VSBTSequence()
            {
                NodeTag = NodeTag.Seq;
            }

            #region Behavior
            public override Return Evaluate()
            {
                BT.Return r = BT.Return.Success;
                bool bIsRunnig = false;
                for (int i = 0; i < m_lstChild.Count; ++i)
                {
                    m_curNode = m_lstChild[i];
                    r = m_curNode.Evaluate();
                    switch (r)
                    {
                        case Return.Evaluate:
                            break;
                        case Return.Success:
                            break;
                        case Return.Fail:
                            m_eState = BT.Return.Fail;
                            return m_eState;
                        case Return.Running:

                            if (m_aiProperty != null && m_aiProperty.CurRunningNode == null)
                            {
                                m_aiProperty.CurRunningNode = m_curNode;
                            }
                            m_aiProperty.CurRunningNode = m_curNode;
                            bIsRunnig = true;
                            break;
                        default:
                            break;
                    }
                }
                m_eState = (bIsRunnig) ? BT.Return.Running : BT.Return.Success;
                return m_eState;
            }


            #endregion

            #region GUI
            public override BT.Virtual.VSBTNode MakeNode(object e)
            {
                return CreateInstance<VSBTSequence>();
            }

            public override void DrawCurve()
            {
                NodeCurve_SlotCurve();
            }
            public override void DrawWindow()
            {
                //  이거는 반드시 써줘야함!
                NodeWindow_Default();
                NodeWindow_AddChildSlot();
            }

            #endregion
        }
    }
}
