﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BT.Enum;
namespace BT
{
    namespace Composite
    {
        public class VSBTSelection : BT.Virtual.VSBTComposite
        {
         
            #region Behavior
            public VSBTSelection()
            {
                NodeTag = NodeTag.Sel;
            }
            public override Return Evaluate()
            {
                
                BT.Return r = BT.Return.Success;

                for (int i = 0; i < m_lstChild.Count; ++i)
                {
                    m_curNode = m_lstChild[i];
                    r = m_curNode.Evaluate();
                    switch (r)
                    {
                        case Return.Evaluate:
                            break;
                        case Return.Success:
                            m_eState = BT.Return.Success;
                            return m_eState;
                        case Return.Fail:
                            break;
                        case Return.Running:
                            m_eState = BT.Return.Evaluate;
                            if (m_aiProperty != null && m_aiProperty.CurRunningNode == null)
                            {
                                m_aiProperty.CurRunningNode = m_curNode;
                            }
                            return Return.Running;
                        default:
                            break;
                    }
                }
                m_eState = BT.Return.Fail;
                return m_eState;
            }
            #endregion

            #region GUI
            public override BT.Virtual.VSBTNode MakeNode(object e)
            {
                return CreateInstance<VSBTSelection>();
            }
            public override void DrawCurve()
            {
                NodeCurve_SlotCurve();
            }
            public override void DrawWindow()
            {
                NodeWindow_Default();
                NodeWindow_AddChildSlot();
            }

            #endregion
        }
    }
}