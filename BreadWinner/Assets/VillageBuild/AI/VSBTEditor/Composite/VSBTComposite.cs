﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace Virtual
    {
        public abstract class VSBTComposite : VSBTNode
        {
            //  아이디어로는 노드 에디터와 계산용 노드를 분리해야한다

            static protected Rect m_slotRect = new Rect(0, 0, 20, 20);
            static protected GUILayoutOption[] m_slotOptions = new[] { GUILayout.Width(m_slotRect.width), GUILayout.Height(m_slotRect.height) };
            [SerializeField] protected List<BT.Virtual.VSBTNode> m_lstChild;
            [SerializeField] public List<BT.EditItem.ChildSlot> m_lstSlot;
            private bool m_bAddSlot = false;
            private bool m_bDelSlot = false;
            private bool m_bAllDelSlot = false;
            protected BT.Virtual.VSBTNode m_curNode = null;
            public VSBTNode CurNode { get => m_curNode; }

            // public abstract void AddChild( BT.Virtual.VSBTNode _node, int _idx = -1);
            public VSBTComposite()
            {
                m_lstSlot = new List<BT.EditItem.ChildSlot>();
                m_lstChild = new List<BT.Virtual.VSBTNode>();
                m_wndNodeRect.width = 300;
                m_wndNodeRect.height = 160;
               //   정리된리스트를 그냥 여기서 가지고 비헤비어는 AI는 재끼자
            }
            public void AddChild(VSBTNode _node)
            {
                if (m_lstChild.Find(f => f == _node) == null)
                {
                    _node.Parent = this;
                    m_lstChild.Add(_node);
                }
            }
            public void AddEditChild(VSBTNode _node)
            {
                if (m_lstSlot.Find(f => f.Node == _node) == null)
                {
                    for(int i = 0; i < m_lstSlot.Count; ++i)
                    {
                        if (m_lstSlot[i].Node == null)
                        {
                            m_lstSlot[i].Node = _node;
                            
                            _node.Parent = this;
                            break;
                        }
                    }
                }
            }
            public void AddChild(VSBTNode _node,Vector2 _mousePos)
            {
                Vector2 fixMouse = new Vector2(_mousePos.x - m_wndNodeRect.x, _mousePos.y - m_wndNodeRect.y);
                for (int i = 0; i < m_lstSlot.Count;++i)
                {
                    if (m_lstSlot[i].Slot.Contains(fixMouse))
                    {
                        if(ExistNodeInSlot(i))
                        {
                            SwapSlot(i,m_lstSlot.Count,_node);
                            return;
                        }
                        else
                        {
                            m_lstSlot[i].Node = _node;
                            _node.Parent = this;
                        }
                        
                        return;
                    }
                }
                AddEditChild(_node);
            }
            public bool ExistNodeInSlot(int _idx)// 존재하는가?
            {
                return (m_lstSlot[_idx].Node != null)?true:false;
            }
            public void SwapSlot(int _cur ,int _max,VSBTNode _insertNode)
            {
                
                int next = _cur + 1;
                if (next < _max)
                {
                    if (ExistNodeInSlot(next))
                    {
                        SwapSlot(next, _max, m_lstSlot[_cur].Node);
                        _insertNode.Parent = this;
                         m_lstSlot[_cur].Node = _insertNode;
                        return;
                    }
                    else
                    {
                        m_lstSlot[next].Node = m_lstSlot[_cur].Node;
                        _insertNode.Parent = this;
                        m_lstSlot[_cur].Node = _insertNode;
                        return;
                    }

                }
                else if(next >= _max)
                {
                    AddSlot();
                    m_lstSlot[next].Node = m_lstSlot[_cur].Node;
                    _insertNode.Parent = this;
                    m_lstSlot[_cur].Node = _insertNode;
                    return;
                }
                

            }
            public void DelChild(VSBTNode _child)
            {
                BT.EditItem.ChildSlot t = m_lstSlot.Find(d => d.Node == _child);
                if(t != null)
                {
                    t.Node = null;
                }
                _child.Parent = null;
            }

            protected void NodeCurve_ChildCurve()// 이거 안씀 원래 용도는 자식을 그리는것
            {
                int slotCnt = 0;
                slotCnt = m_lstSlot.Count;
                float colorCover = 1.0f / slotCnt;
                float colorN = 0.0f;
                float coverX = m_wndNodeRect.width / (slotCnt + 1);
                float n = 0;
                for (int i = 0; i < slotCnt; ++i)
                {
                    if (m_lstSlot[i].Node == null) continue;
                    
                    colorN = n * colorCover;
                    Color color = Color.green;
                    Rect child = m_lstSlot[i].Node.NodeRect;

                    Vector3 startPos = new Vector3(m_wndNodeRect.x + (coverX * i), m_wndNodeRect.y + m_wndNodeRect.height, 0.0f);
                    Vector3 endPos = new Vector3(child.x + (child.width * 0.5f), child.y, 0);

                    Vector3 startTan = startPos + Vector3.right * VSBTEditor.CurveForce;
                    Vector3 endTan = endPos + Vector3.left * VSBTEditor.CurveForce;

                    Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 3);
                }
            }
            protected void NodeCurve_SlotCurve()//  슬롯과 연결된 라인을 그린다.
            {
                int slotCnt = m_lstSlot.Count;
                float colorCover = 1.0f / slotCnt;
                
                float coverX = m_wndNodeRect.width / (slotCnt + 1);
                Color color = Color.black;
                for (int i = 0; i < slotCnt; ++i)
                {
                    if (m_lstSlot[i].Node == null) continue;
                    Rect slotRect = m_lstSlot[i].PixSlot;
                   
                    slotRect.x += m_wndNodeRect.x;
                    slotRect.y += m_wndNodeRect.y;
                    Rect childRect = m_lstSlot[i].Node.NodeRect;
                    int colorNum = i % VSBTEditor.g_colorArray.Length ;
                    color = VSBTEditor.g_colorArray[colorNum];
                    m_lstSlot[i].lineColor = color;
                    Vector3 startPos = new Vector3(slotRect.x +(slotRect.width * 0.5f) , slotRect.y + slotRect.height, 0.0f);
                    Vector3 endPos = new Vector3(childRect.x + (childRect.width * 0.5f), childRect.y, 0);

                    Vector3 startTan = startPos + Vector3.left *VSBTEditor.CurveForce;
                    Vector3 endTan = endPos + Vector3.right * VSBTEditor.CurveForce;
                    
                    Handles.DrawBezier(startPos, endPos, startTan, endTan, m_lstSlot[i].lineColor, null, 3);
                }

            }
            protected void AddSlot()
            {
                BT.EditItem.ChildSlot s = CreateInstance<BT.EditItem.ChildSlot>();
                s.Slot = m_slotRect;
                int a = m_lstSlot.Count;

                s.Slot.x = (a * m_slotRect.width);
                s.Slot.y = m_wndNodeRect.height - m_slotRect.height;
                if (s.Slot.x > m_wndNodeRect.width- m_slotRect.width)
                {
                    m_wndNodeRect.width = m_wndNodeRect.width + m_slotRect.width;
                }

                s.PixSlot = s.Slot;
                s.PixSlot.width = s.Slot.width - 2;
                s.PixSlot.height = s.Slot.height - 2;

                m_lstSlot.Add(s);
            }
            protected void AddSlotAndChild(BT.Virtual.VSBTNode _node)// 슬롯을 추가하면서 노드를 만드는 방법
            {
                BT.EditItem.ChildSlot s = CreateInstance<BT.EditItem.ChildSlot>();
                s.Slot = m_slotRect;
                int a = m_lstSlot.Count;
                s.Node = _node;
                s.Slot.x = (a * m_slotRect.width);
                s.Slot.y = m_wndNodeRect.height - m_slotRect.height;
                if (s.Slot.x > m_wndNodeRect.width - m_slotRect.width)
                {
                    m_wndNodeRect.width = m_wndNodeRect.width + m_slotRect.width;
                }

                s.PixSlot = s.Slot;
                s.PixSlot.width = s.Slot.width - 2;
                s.PixSlot.height = s.Slot.height - 2;

                m_lstSlot.Add(s);
            }
            protected void DelSlot()
            {
                int cnt = m_lstSlot.Count;
                if(cnt > 0 )
                {
                    if(m_lstSlot[cnt - 1].Node != null)
                    {
                        Virtual.VSBTComposite p = (Virtual.VSBTComposite)m_lstSlot[cnt - 1].Node.Parent;
                        if (p != null)
                        {
                            p.DelChild(m_lstSlot[cnt - 1].Node);
                        }
                        m_lstSlot[cnt - 1].Node = null;
                    }
                    BT.EditItem.ChildSlot target = m_lstSlot[cnt - 1];
                    m_lstSlot.Remove(target);
                    DestroyImmediate(target);
                }
            }
            
            public override void SortSlot()
            {
                m_lstChild.Clear();
                for(int i = 0; i < m_lstSlot.Count;++i)
                {
                    if(m_lstSlot[i].Node == null)
                    {
                        continue;
                    }
                    m_lstChild.Add(m_lstSlot[i].Node);
                }
            }
            //  무조건 아래에
            protected void DebugChildCnt()
            {
                int cnt = 0;
                foreach(var s in m_lstSlot)
                {
                    if(s.Node != null)
                    {
                        ++cnt;
                    }
                }
                Debug.Log("Child Cnt = " + cnt);
            }
            protected void NodeWindow_AddChildSlot()
            {
                // GUILayout.FlexibleSpace();
                
                GUILayout.Label("Slot Cnt : " + m_lstSlot.Count.ToString(), GUILayout.Width(100));
                GUILayout.BeginHorizontal();
                m_bAddSlot =  GUILayout.Button("Add Slot",GUILayout.Width(80));
                m_bDelSlot = GUILayout.Button("Del Slot", GUILayout.Width(80));
                m_bAllDelSlot = GUILayout.Button("AllDel Slot", GUILayout.Width(80));
                GUILayout.EndHorizontal();
                if (m_bAddSlot)
                {
                    AddSlot();
                }
                if(m_bDelSlot)
                {
                    DelSlot();
                }
                if(m_bAllDelSlot)
                {
                    for(int i = 0; i < m_lstSlot.Count * 10;++i)
                    {
                        DelSlot();
                    }
                }
                GUILayout.BeginHorizontal();
                Color color = Color.black;

                for (int i = 0; i < m_lstSlot.Count; ++i)
                {
                    if(m_lstSlot[i].Node == null)
                    {
                        color = Color.black;
                    }
                    else
                    {
                        int colorNum = i % VSBTEditor.g_colorArray.Length ;
                        color = VSBTEditor.g_colorArray[colorNum];
                    }
                    EditorGUI.DrawRect(m_lstSlot[i].PixSlot, color);
                }
               //  w 23 임
                GUILayout.EndHorizontal();
               // DebugChildCnt();
            }
            public override BT.Virtual.VSBTNode CopyNewNode(BT.Virtual.VSBTNode _parent = null)
            {
                BT.Virtual.VSBTComposite clone = (Virtual.VSBTComposite)MakeNode(this);
                clone.m_aiProperty = m_aiProperty;
                clone.m_gameObj = m_gameObj;
                clone.m_parent = _parent;
                clone.m_eNodeCat = m_eNodeCat;
                clone.m_strTitle = m_strTitle;
                clone.m_wndNodeRect = m_wndNodeRect;
                
                BT.Virtual.VSBTNode child = null;
                for(int i = 0; i < m_lstSlot.Count;++i)
                {
                    if (m_lstSlot[i].Node == null) continue;
                    child = m_lstSlot[i].Node.CopyNewNode(clone);
                    clone.AddSlotAndChild(child);
                }
                return clone;
            }
            protected BT.Virtual.VSBTNode LoadNewNode(List<VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
            {
                BT.Virtual.VSBTComposite clone = (Virtual.VSBTComposite)MakeNode(this);
                clone.m_aiProperty = m_aiProperty;
                clone.m_gameObj = m_gameObj;
                clone.m_parent = _parent;
                clone.m_eNodeCat = m_eNodeCat;
                clone.m_strTitle = m_strTitle;
                clone.m_wndNodeRect = m_wndNodeRect;
                
                BT.Virtual.VSBTNode child = null;
                for (int i = 0; i < m_lstSlot.Count; ++i)
                {
                    if (m_lstSlot[i].Node == null)
                    {
                        continue;
                    }
                    else
                    {
                        child = m_lstSlot[i].Node.LoadBT(_list, clone);
                        clone.AddSlotAndChild(child);
                    }
                }
                return clone;
            }
            public override VSBTNode CopyNode(List<BT.Virtual.VSBTNode> _list,VSBTNode _parent = null)
            {
                VSBTComposite clone = (VSBTComposite)CopyNewNode(_parent);
                _list.Add(clone);
                BT.Virtual.VSBTNode child = null;
                for (int i = 0; i < m_lstSlot.Count; ++i)
                {
                    if (m_lstSlot[i].Node == null)
                    {
                        continue;
                    }
                    else
                    {
                        child = m_lstSlot[i].Node.CopyNode(_list);
                        clone.AddChild(child);
                    }
                }
                return clone;
            }
            public override BT.Virtual.VSBTNode SaveBT(List<VSBTNode> _list,VSBTNode _parent = null)
            {
                //  저장은 child에다가 
                //  로드는 slot으로
                BT.Virtual.VSBTComposite clone = (BT.Virtual.VSBTComposite)MakeNode(this);
                clone.IsRoot = m_bRoot;
                clone.m_aiProperty = m_aiProperty;
                clone.m_gameObj = m_gameObj;
                clone.m_parent = _parent;
                clone.m_eNodeCat = m_eNodeCat;
                clone.m_strTitle = m_strTitle;
                clone.m_wndNodeRect = m_wndNodeRect;
                
                if (_parent != null)
                {
                    BT.Virtual.VSBTComposite parent = (BT.Virtual.VSBTComposite)_parent;
                    clone.BTPath = parent.BTPath;
                    parent.AddChild(clone);
                    clone.hideFlags = HideFlags.None;
                    AssetDatabase.AddObjectToAsset(clone, parent.BTPath);
                }
                else
                {
                    clone.BTPath = VSBTEditor.BTPath + clone.Title + ".asset";
                    clone.hideFlags = HideFlags.None;
                    AssetDatabase.CreateAsset(clone, clone.BTPath);
                }
                BT.Virtual.VSBTNode child = null;
                for(int i = 0; i < m_lstSlot.Count;++i)
                {
                    
                    if(m_lstSlot[i].Node == null)
                    {
                        continue;
                    }
                    else
                    {
                        child = m_lstSlot[i].Node.SaveBT(_list, clone);
                        clone.AddChild(child);
                    }
                }
                return clone;
            }
            public override BT.Virtual.VSBTNode LoadBT(List<VSBTNode> _list, BT.Virtual.VSBTNode _parent = null)
            {
                BT.Virtual.VSBTComposite clone = (Virtual.VSBTComposite)MakeNode(this);
                clone.IsRoot = m_bRoot;
                clone.m_aiProperty = m_aiProperty;
                clone.m_gameObj = m_gameObj;
                clone.m_parent = _parent;
                clone.m_eNodeCat = m_eNodeCat;
                clone.m_strTitle = m_strTitle;
                clone.m_wndNodeRect = m_wndNodeRect;
                
                _list.Add(clone);
                VSBTEditor.RegistNodeWnd(clone);
                BT.Virtual.VSBTNode child = null;
                for (int i = 0; i < m_lstChild.Count; ++i)
                {
                    if(m_lstChild[i] == null)
                    {
                        continue;
                    }
                    else
                    {
                        child = m_lstChild[i].LoadBT(_list, clone);
                        clone.AddSlotAndChild(child);
                    } 
                }
                return clone;
            }
            public override VSBTNode LoadAI(List<VSBTNode> _list, VSBTNode _parent = null)
            {
                VSBTComposite clone = (BT.Virtual.VSBTComposite)MakeNode(this);
                clone.IsRoot = m_bRoot;
                clone.m_aiProperty = m_aiProperty;
                clone.m_gameObj = m_gameObj;
                clone.m_parent = _parent;
                clone.m_eNodeCat = m_eNodeCat;
                clone.m_strTitle = m_strTitle;
                _list.Add(clone);
                VSBTNode child = null;
                for (int i = 0; i < m_lstChild.Count; ++i)
                {
                    if (m_lstChild[i] == null)
                    {
                        continue;
                    }
                    else
                    {
                        child = m_lstChild[i].LoadAI(_list, clone);
                        clone.AddChild(child);
                    }
                }
                return clone;
            }
            public override void TestTreeDebug(int _floor)
            {
                Debug.Log(_floor.ToString() + " " + m_strTitle);
                int f = ++_floor;
                foreach (var n in m_lstSlot)
                {
                    if(n.Node != null)
                    {
                        n.Node.TestTreeDebug(f);
                    }
                }


            }
            public override void ResetTree(Return _state = Return.Evaluate)
            {
                m_eState = _state;
                foreach (var n in m_lstChild)
                {
                    n.ResetTree(_state);
                }
            }
            public override void CurrentNode()
            {
                Debug.Log(m_curNode.name);
             }

        }
    }
}
