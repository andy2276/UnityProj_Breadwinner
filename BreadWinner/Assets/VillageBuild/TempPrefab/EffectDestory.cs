﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDestory : MonoBehaviour
{
    ParticleSystem m_particle = null;
    
    // Start is called before the first frame update
    void Start()
    {
        m_particle = gameObject.GetComponent<ParticleSystem>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if(m_particle != null)
        {
            if(!m_particle.IsAlive())
            {
                Destroy(gameObject);
            }
        }
        
    }
}
