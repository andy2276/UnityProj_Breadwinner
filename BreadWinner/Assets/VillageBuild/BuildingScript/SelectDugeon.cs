﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectDugeon : VillageBase
{
    [SerializeField] BT.Enum.AreaType m_eAreaType = BT.Enum.AreaType.End;
    // Start is called before the first frame update
    public override void BuildingClick()
    {


    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseUpAsButton()
    {
        if (BT.Enum.AreaType.End == m_eAreaType)
        {
            VillageSceneMgr.Inst.CancelBattleArea();
            SceneMgr.Inst.BattleData.AreaType = m_eAreaType;
        }
        else
        {
            Debug.Log("ClickDown" + m_eAreaType.ToString());
            VillageSceneMgr.Inst.SetAreaType(m_eAreaType);
            VillageSceneMgr.Inst.OnBattleUI();
        }
    }
}
