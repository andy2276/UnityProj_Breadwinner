﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : VillageBase
{
    delegate void PortalEvent();
    PortalEvent m_portalEvent = null;
    float m_fMaxTime = 2;
    float m_fCurTime = 0;

    //  진입점
    public override void BuildingClick()
    {
       
        m_portalEvent = ClickEvent;
    }

    void ClickEvent()
    {
       
        m_portalEvent = WaitPortal;
        return;
    }
    void WaitPortal()
    {
        
        if (m_fCurTime < m_fMaxTime)
        {
            m_fCurTime += Time.deltaTime;
        }
        else
        {
            m_fCurTime = 0;
            m_portalEvent = OnPortalMap;
        }
    }
    void OnPortalMap()
    {
       
        m_portalEvent = null;
        VillageSceneMgr.Inst.ChangePortal();
    }

    // Update is called once per frame
    void Update()
    {
        m_portalEvent?.Invoke();
    }
}
