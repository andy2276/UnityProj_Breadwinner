﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[CreateAssetMenu(fileName = "BuilInfo", menuName = "BuildInfoData", order = int.MaxValue)]
public class BuildInfo : ScriptableObject
{
    [SerializeField] private string m_strPath = "Assets/VillageBuild/VillageData/";
    [SerializeField] private string m_strFileName = "NewBuildInfo";
    private List<List<int>> m_lst2BuildInfo = new List<List<int>>();
    public List<List<int>> List2BuildInfo { get => m_lst2BuildInfo; set => m_lst2BuildInfo = value; }
    private void Awake()
    {
        Init();
    }
    void Init()
    {
        for (int i = 0; i < m_lst2BuildInfo.Count; ++i)
        {
            m_lst2BuildInfo[i] = new List<int>();
        }
    }

     void ClearList()
    {
        for (int i = 0; i < m_lst2BuildInfo.Count; ++i)
        {
            m_lst2BuildInfo[i].Clear();
        }
    }

    public void MakeLst2BuildType(int _y)
    {
        ClearList();
        for(int i = 0; i < _y; ++i)
        {
            m_lst2BuildInfo.Add(new List<int>());
        }

    }

    public void LoadInfo()
    {
        FileInfo f = new FileInfo(m_strPath + m_strFileName + ".txt");
        string[] v;

        if (f.Exists)
        {
            v = File.ReadAllLines(m_strPath + m_strFileName + ".txt");
            if (v.Length > 0)
            {
                for (int y = 0; y < v.Length; ++y)
                {
                    m_lst2BuildInfo.Add(new List<int>());
                    for (int x = 0; x < v[y].Length; ++x)
                    {
                        char t = v[y][x];
                        if (t != ',')
                        {
                            m_lst2BuildInfo[y].Add((int)char.GetNumericValue(t));
                        }
                    }
                }
            }
        }
    }
    public void WriteData(int _y,int _x,int _data)
    {
        m_lst2BuildInfo[_y][_x] = _data;
    }
    public void WriteData(Vector2Int _coord, int _data) => WriteData(_coord.y, _coord.x, _data);
    public void SaveInfo()
    {
        string path = m_strPath + m_strFileName + ".txt";
        FileStream f = new FileStream(path, FileMode.Create, FileAccess.Write);
        StreamWriter w = new StreamWriter(f, System.Text.Encoding.Unicode);
        string t = "";
        for (int y = 0; y < m_lst2BuildInfo.Count; ++y)
        {
            for (int x = 0; x < m_lst2BuildInfo[y].Count; ++x)
            {
                t += m_lst2BuildInfo[y][x].ToString() + ",";
            }
            w.WriteLine(t);
            t = "";
        }
        
        w.Flush();
        w.Close();
        f.Close();
    }



}