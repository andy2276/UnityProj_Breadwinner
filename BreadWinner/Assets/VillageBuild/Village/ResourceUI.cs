﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceUI : MonoBehaviour
{
    [SerializeField] string g_strResName = "InsertName";
    [SerializeField] GameObject g_UIObj = null;
    [SerializeField] int g_nIdx = 0;
    [SerializeField] float g_fResCnt = 0.0f;
    Text m_UIText;

    private void Awake()
    {
        m_UIText = g_UIObj.GetComponent<Text>();
    }

    // Start is called before the first frame update

    public void SetResIdx(int _idx)
    {
        g_nIdx = _idx;
    }
    
    public float GetResCnt()
    {
        return g_fResCnt;
    }
    public void AddResCnt(float _value = 1.0f)
    {
        g_fResCnt += _value;
    }
    public void SubResCnt(float _value = 1.0f)
    {
        g_fResCnt -= _value;
    }
    private void SetCntText()
    {
        int off = (int)g_fResCnt;
        m_UIText.text = g_strResName + " : " + off.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        SetCntText();
    }
}
