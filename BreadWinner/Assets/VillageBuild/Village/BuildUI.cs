﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BuildUI : MonoBehaviour
{
   public int g_UIStatus = 0;
    int m_nBuildLevel = 1;
    [SerializeField] private Text m_buildTitle = null;
    [SerializeField] private Text m_buildRes = null;
    private void OnEnable()
    {
        OnResInfo();

    }
    private void OnDisable()
    {
        
    }
    public string GetTitle(int _type)
    {
        string title = "NoTitle";
        switch (_type)
        {
            case 0: title = "특수건물"; break;
            case 1: title = "주민 집"; break;
            case 2: title = "대장간"; break;
            case 3: title = "상점"; break;
            case 4: title = "병사집"; break;
            case 5: title = "길"; break;
            default:
                break;
        }
        return title;
    }
    public void OnResInfo()
    {
        if (m_buildTitle != null)
        {
            if(BuildMgr.Inst.WaitBuildSlot != null)
            {
                m_buildTitle.text = GetTitle(BuildMgr.Inst.WaitBuildSlot.BuildType);
            }
            
        }
        if(m_buildRes != null)
        {

        }
    }
    public void OnResInfo(string _info,bool _fin = false)
    {
        m_buildRes.text = _info;

    }
 
    public void OkBuild()
    {
        g_UIStatus = 1;
        BuildMgr.Inst.BuildUIOkBuild();
        BuildMgr.Inst.OffBuildUI();
    }
    public void OnCancel()
    {
        g_UIStatus = 2;
        BuildMgr.Inst.InitWaitBuild();
        BuildMgr.Inst.OffBuildUI();
    }
    public void ResetUI()
    {
        g_UIStatus = 0;
    }
}
