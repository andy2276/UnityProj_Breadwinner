﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ResourceMgr : MonoBehaviour
{
    public struct tResUISaveData
    {
        public int nIdx;
        public float fValue;
    }
    public static ResourceMgr singleInst;

    //  여기에는 resourceUI를 담고 ResourceUI 에는 객체 값을 담자!
    [SerializeField] GameObject[] g_UIObjs;

    

    ResourceUI[] m_nResUIs;
    tResUISaveData[] m_tSaveDatas;
   
    int m_nUIObjCnt;
   
    private void Awake()
    {
        singleInst = this;
        m_nUIObjCnt = g_UIObjs.Length;
        m_nResUIs = new ResourceUI[m_nUIObjCnt];
        m_tSaveDatas = new tResUISaveData[m_nUIObjCnt];
        for (int i = 0; i < m_nUIObjCnt; ++i)
        {
            m_nResUIs[i] = g_UIObjs[i].GetComponent<ResourceUI>();
        }

    }
    // Start is called before the first frame update
    public void AddCnt(int _idx,float _value)
    {
        if(_idx >= m_nUIObjCnt)
        {
            return;
        }
        m_nResUIs[_idx].AddResCnt(_value);
    }
    public void SubCnt(int _idx, float _value)
    {
        if (_idx >= m_nUIObjCnt)
        {
            return;
        }
        m_nResUIs[_idx].AddResCnt(_value);
    }

    public void SaveValue(int _idx, float _value)
    {
        //  이거 스트럭쳐로 묶자.

        if (_idx >= m_nUIObjCnt)
        {
            return;
        }
        m_tSaveDatas[_idx].nIdx = _idx;
        m_tSaveDatas[_idx].fValue = _value;

    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
