﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildCamMover : MonoBehaviour
{
    [SerializeField] float m_fMoveSpeed = 10;
    [SerializeField] float m_fTurnSpeed = 2;

    Vector3 m_v3TargetPos = new Vector3();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * m_fMoveSpeed * time;
        }
        else if(Input.GetKey(KeyCode.S))
        {
            transform.position += transform.forward * -1 * m_fMoveSpeed * time;
        }
        //  회전
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += transform.right * -1 * m_fMoveSpeed * time;
          //  transform.Rotate(0, m_fTurnSpeed * Time.deltaTime, 0);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * m_fMoveSpeed * time;
           // transform.Rotate(0, m_fTurnSpeed * Time.deltaTime * -1, 0);
        }

        if (Input.GetMouseButton(1))
        {
            Vector3 m = Input.mousePosition;
            Vector3 dir = (m_v3TargetPos - transform.position).normalized;
            m.z = Vector3.Distance(Camera.main.transform.position, transform.position);
            m_v3TargetPos = Camera.main.ScreenToWorldPoint(m);
            m_v3TargetPos.y = transform.position.y;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), time * m_fTurnSpeed * 0.5f);
        }


    }
}
