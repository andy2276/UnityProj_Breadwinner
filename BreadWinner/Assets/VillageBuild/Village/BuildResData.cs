﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct tResInfo
{
    public string strName;
    public float fCnt;
}
public enum E_Res_List { Wood,Gold, }

[CreateAssetMenu(fileName = "BuildRes",menuName = "Scriptable object/BuildResData",order = int.MaxValue)]
public class BuildResData : ScriptableObject
{
    [SerializeField]
    private string strBuildingName;
    [SerializeField]
    int nLevel = 0;
    [SerializeField]
    tResInfo[] tResInfos;
}
