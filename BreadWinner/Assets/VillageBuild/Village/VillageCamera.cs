﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageCamera : MonoBehaviour
{
    //[SerializeField] Vector3 g_v3InitPos = new Vector3(70.0f, 70.0f, 0.0f);
    //[SerializeField] Vector3 g_v3InitRot = new Vector3(45.0f, 270.0f, 0.0f);
    //[SerializeField] GameObject g_mainPlane;
    //[SerializeField] float g_fFarDist = 150.0f;
    //[SerializeField] Vector2 g_fDistRange = new Vector2(50.0f, 250.0f);
    //[SerializeField] float g_fDistSpeed = 15.0f;
    //[SerializeField] float g_fRotSpeed = 1.0f;
    //
    //Vector3 m_f3PrePos, m_f3CurPos;
    //float m_fPreAngle, m_fCurAngle;
    //
    //  새롭게 시작


    #region CamUpdateVariable
    #region Common
    delegate void BuildCamUpdate();
    [Header("Common")]
    BuildCamUpdate m_delUpdate = null;
    [SerializeField] GameObject m_target = null;

    #endregion
    #region FullShot
    [Header("FullShot")]
    [Range(10,50)]
    [SerializeField] float m_fFullShotOffsetDist = 50;
    [Range(10, 50)]
    [SerializeField] float m_fFullShotOffsetY = 50;
    //  [SerializeField] float m_fFullShotFollowDist = 5;
    [Range(1, 20)]
    [SerializeField] float m_fFullShotCamMoveSpeed = 10;
    [Range(1, 20)]
    [SerializeField] float m_fFullShotCamTurnSpeed = 1;
    [Range(0, 90)]
    [SerializeField] float m_fFullShotAngle = 90;

    [SerializeField] Vector2 m_v2FullShotWheelDist = new Vector2(10, 50);
    [SerializeField] Vector2 m_v2FullShotWheelY = new Vector2(10, 50);
    [SerializeField] Vector2 m_v2FullShotWheelAdd = new Vector2(1, 1);

    Vector3 m_v3TargetPos = Vector3.zero;

    Vector3 m_v3CamPos = Vector3.zero;
    Vector3 m_v3CamPosDir = Vector3.zero;
    #endregion
    #region Zoom
    [Header("Zoom")]
    [SerializeField] float m_fMaxTime = 0;
    GameObject m_zoomTarget = null;
    Vector3 m_zoomTargetPos = Vector3.zero;
    Vector3 m_zoomCamTargetPos = Vector3.zero;

    [Range(1,20)]
    [SerializeField] float m_fZoomSpeed = 1;

    [SerializeField] Vector3 m_zoomOffset = new Vector3(1, 3, 3);


    float m_zoomOffsetDist = 0;
    #endregion



    #endregion
    
    void OnMoveTarget()
    {
        if(m_target.activeSelf == false)
        {
            m_target.SetActive(true);
        }
    }
    void OffMoveTarget()
    {
        if (m_target.activeSelf == true)
        {
            m_target.SetActive(false);
        }
    }
    
    // Start is called before the first frame update
    private void Awake()
    {
        m_fFullShotAngle = Mathf.Cos(m_fFullShotAngle * Mathf.Deg2Rad);
        m_delUpdate = InBuildUpdate;
    }
    void InBuildUpdate()
    {
        Vector3 pos = m_target.transform.position;
        m_target.transform.position = new Vector3(pos.x, pos.y + 5, pos.z);
       m_delUpdate = FullShotCamUpdate;
    }
    void FullShotInUpdate()
    {

    }
    void FullShotCamUpdate()
    {
        if(BuildMgr.Inst.IsOnBuildUI())
        {
            m_zoomTarget = BuildMgr.Inst.WaitBuildSlot.gameObject;
            m_target.transform.LookAt(m_zoomTarget.transform.position,Vector3.up);
            m_zoomTargetPos = m_zoomTarget.transform.position;
            m_zoomCamTargetPos = m_zoomTargetPos + m_zoomOffset;
            m_zoomOffsetDist = Vector3.Distance(Vector3.zero, m_zoomOffset);
            OffMoveTarget();
            m_delUpdate = ZoomInCamUpdate;
            return;
        }

        if(m_target != null)
        {
            Vector2 mouseWheel = Input.mouseScrollDelta;
            
            if (mouseWheel.y > 0)
            {
               if(m_v2FullShotWheelDist.x <= m_fFullShotOffsetDist)
               {
                   m_fFullShotOffsetDist -= m_v2FullShotWheelAdd.x;
               }
                if (m_v2FullShotWheelY.x <= m_fFullShotOffsetY)
                {
                    m_fFullShotOffsetY -= m_v2FullShotWheelAdd.y;
                }
            }
            else if (mouseWheel.y < 0)
            {
                if (m_v2FullShotWheelDist.y > m_fFullShotOffsetDist)
                {
                    m_fFullShotOffsetDist += m_v2FullShotWheelAdd.x;
                }
                if (m_v2FullShotWheelY.y > m_fFullShotOffsetY)
                {
                    m_fFullShotOffsetY += m_v2FullShotWheelAdd.y;
                }
            }
            float time = Time.deltaTime;
            Transform targetTrans = m_target.transform;
            Vector3 targetLook = targetTrans.forward;
            m_v3TargetPos = targetTrans.position;

            m_v3CamPos = m_v3TargetPos + (targetLook * -1 * m_fFullShotOffsetDist);
            float camToTargetDist = Vector3.Distance(m_v3CamPos, targetTrans.position);
            m_v3CamPos.y += m_fFullShotOffsetY;

            m_v3CamPosDir = (m_v3CamPos - transform.position).normalized;

            float targetDist = Vector3.Distance(m_v3TargetPos, transform.position);
            transform.position = m_v3CamPos;
            //if (m_fFullShotOffsetDist < targetDist)
            //{
            //    transform.position += m_v3CamPosDir * m_fFullShotCamMoveSpeed * time;
            //}
            float angle = Vector3.Dot(targetLook, transform.forward);
            if(angle < m_fFullShotAngle)
            {
                Vector3 dir = (targetTrans.position - transform.position).normalized;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), time * m_fFullShotCamTurnSpeed) ;
            }
         //   transform.LookAt(m_v3TargetPos);
        }
    }
    bool TargettingForZoom()
    {
        float time = Time.deltaTime;
        float dist = Vector3.Distance(transform.position, m_zoomCamTargetPos);
        if (dist > m_zoomOffsetDist)
        {
            transform.position = Vector3.Lerp(transform.position, m_zoomCamTargetPos, m_fZoomSpeed * time);
         
            transform.LookAt(m_zoomTargetPos);
        
            return false;
        }
        else
        {
            return true;
        }
    }

    void ZoomInCamUpdate()
    {
        if(!BuildMgr.Inst.IsOnBuildUI())
        {
        //    m_zoomTarget = BuildMgr.Inst.WaitBuildSlot.gameObject;
            m_zoomTargetPos = m_zoomTarget.transform.position;

            //  원래카메라 위치로
            Vector3 pos = m_target.transform.forward * -1 * m_fFullShotOffsetDist;
            pos.y = m_fFullShotOffsetY;

            m_zoomCamTargetPos = pos;
            m_zoomOffsetDist = Vector3.Distance(Vector3.zero, m_zoomOffset);
            OffMoveTarget();
            m_delUpdate = ZoomOutCamUpdate;
            return;
        }
       
        TargettingForZoom();


    }
    void ZoomOutCamUpdate()
    {
        if(TargettingForZoom())
        {
            m_delUpdate = InBuildUpdate;
            OnMoveTarget();
        }
       
    }
    
    // Update is called once per frame
    void Update() => m_delUpdate?.Invoke();
}
