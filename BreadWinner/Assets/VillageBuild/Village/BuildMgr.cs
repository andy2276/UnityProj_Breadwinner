﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct tSlotInfo
{
    public bool _bBuildPossible;
    public int _buildType;
    public Vector2Int _n2Idx;
}

public class BuildMgr : MonoBehaviour
{
    // Start is called before the first frame update
    private static BuildMgr m_Inst = null;

    [SerializeField] Vector2Int g_v2FullCellCnt = new Vector2Int(5, 5);
    [SerializeField] Vector2Int g_v2ColliderCellCnt = new Vector2Int(4, 4);
    [SerializeField] Vector3 g_v3BuildSlotSize = new Vector3(10.0f, 0.0f, 10.0f);
    [SerializeField] Vector3 g_v3BuildingScale = new Vector3(1, 1, 1);
    [SerializeField] Vector3 g_v3BuildCenter = new Vector3(0.0f, 0.0f, 0.0f);
    [SerializeField] Vector3 g_v3BuildStartPos = new Vector3(-30.0f, 0.0f, -30.0f);
    [SerializeField] Vector3 g_v3BuildGap = new Vector3(10.0f, 0.0f, 10.0f);
    [SerializeField] Vector2 g_v2MapWidHei = new Vector2(80.0f, 80.0f);
    [SerializeField] GameObject m_colliderWall = null;
    [SerializeField] GameObject m_inventory = null;

    [SerializeField] GameObject g_originBuildSlot = null;
    [SerializeField] GameObject g_buildUI = null;

    [SerializeField] BuildInfo g_BuildTypeBase = null;      //  베이스 타입을 설정해주기 위해서 존재
    [SerializeField] BuildInfo g_BuildLevelData = null;      //  세이브된 레벨 데이터들을 로드하기 위해서 존재 후에는 이게 세이브 데이터에 덮어 씌워지게 된다.

    [SerializeField] List<GameObject> g_lstUnique = new List<GameObject>();
    [SerializeField] List<GameObject> g_lstHouse = new List<GameObject>();
    [SerializeField] List<GameObject> g_lstBlackSmith = new List<GameObject>();
    [SerializeField] List<GameObject> g_lstShop = new List<GameObject>();
    [SerializeField] List<GameObject> g_lstSoldier = new List<GameObject>();
    [SerializeField] List<GameObject> g_lstRoad = new List<GameObject>();
    bool m_buildMenuOn = true;

    List<List<GameObject>> m_2lstSlots = new List<List<GameObject>>();
    BuildUI m_buildUIScript = null;

    int m_nWaitBuildType = -1;
    Vector3 m_v3WaitBuildPos = new Vector3();
    Quaternion m_qutWaitBuildRot = new Quaternion();
    BuildSlot m_waitBuildSlot = null;

    RaycastHit m_rayHit = new RaycastHit();

    Vector2 m_v2WidHeiHigh = Vector2.zero;
    Vector2 m_v2WidHeiLow = Vector2.zero;

    bool m_bOnBuildUI = false;
    bool m_bAtFirst = true;
    int m_nPrefabCnt;
    bool m_bBuildOn = false;

    public static BuildMgr Inst {
        get
        {
            if (m_Inst == null)
            {
                m_Inst = new BuildMgr();
            }
            return m_Inst;
        }
    }
    public static bool IsInstance()
    {
        return (m_Inst != null);
    }

    public BuildSlot WaitBuildSlot { get => m_waitBuildSlot; }
    public bool BuildOn { get => m_bBuildOn; set => m_bBuildOn = value; }
    public Inventory Inventory { get => m_inventory.GetComponent<Inventory>(); }

    private void Start()
    {
        if (m_Inst == null)
        {
            m_Inst = this;
         //   DontDestroyOnLoad(this);
        }
        g_buildUI.SetActive(false);
        m_buildUIScript = g_buildUI.GetComponent<BuildUI>();
        if (g_BuildTypeBase != null) g_BuildTypeBase.LoadInfo();
        if (g_BuildLevelData != null) g_BuildLevelData.LoadInfo();
       
    }
    #region SaveLoadToChangeScene

    public void StopBuildMgr()
    {
        SaveCurSlots();
        ClearSlots();
    }
    
    private void SaveCurSlots()
    {
        g_BuildLevelData.SaveInfo();
    }
    private void ClearSlots()
    {
        GameObject target = null;
        if(m_2lstSlots.Count != 0)
        {
            for (int i = m_2lstSlots.Count - 1; 0 <= i; --i)
            {
                for(int j = m_2lstSlots[i].Count -1; 0 <= j;--j)
                {
                    if(m_2lstSlots[i][j] != null)
                    {
                        target = m_2lstSlots[i][j];
                        m_2lstSlots[i][j] = null;
                        Destroy(target);
                        target = null;
                    }
                }
                m_2lstSlots[i].Clear();
            }
            m_2lstSlots.Clear();
        }
    }

    public void OffBuildMgr()
    {
        if(gameObject.activeSelf == true)
        {
            gameObject.SetActive(false);
        }
    }
    public void PlayBuildMgr()
    {
        CreateSlots();
        LoadSaveSlots();
    }
   
    private void CreateSlots()
    {
        Vector3 gaps = g_v3BuildGap * 0.5f;
        GameObject slot = null;
        for (int i = 0; i < g_v2ColliderCellCnt.y; ++i)
        {
            m_2lstSlots.Add(new List<GameObject>());
            for (int j = 0; j < g_v2ColliderCellCnt.x; ++j)
            {
                Vector3 pos = new Vector3
                {
                    x = g_v3BuildCenter.x + g_v3BuildStartPos.x + ((g_v3BuildSlotSize.x + gaps.x) * j),
                    y = g_v3BuildCenter.y,
                    z = g_v3BuildCenter.z + g_v3BuildStartPos.z + ((g_v3BuildSlotSize.z + gaps.z) * i)
                };
                if(m_v2WidHeiHigh.x < pos.x)
                {
                    m_v2WidHeiHigh.x = pos.x;
                }
                if(m_v2WidHeiLow.x > pos.x)
                {
                    m_v2WidHeiLow.x = pos.x;
                }

                slot = Instantiate(g_originBuildSlot, pos, Quaternion.identity);
                slot.transform.localScale = g_v3BuildSlotSize;
                slot.GetComponent<BuildSlot>().ListCoord = new Vector2Int(j,i);
                m_2lstSlots[i].Add(slot);
            }
            if (m_v2WidHeiHigh.y < slot.transform.position.z)
            {
                m_v2WidHeiHigh.y = slot.transform.position.z;
            }
            if (m_v2WidHeiLow.y > slot.transform.position.z)
            {
                m_v2WidHeiLow.y = slot.transform.position.z;
            }
        }
        CreateWall();

        Debug.Log("High : " + m_v2WidHeiHigh.ToString());
        Debug.Log("Low  : " + m_v2WidHeiLow.ToString());
    }
    void CreateWall()
    {
        if(m_colliderWall == null)
        {
            return;
        }
        float scaleX = Mathf.Abs(m_v2WidHeiHigh.x) + Mathf.Abs(m_v2WidHeiLow.x) + 10;
        float scaleZ = Mathf.Abs(m_v2WidHeiHigh.y) + Mathf.Abs(m_v2WidHeiLow.y) + 10;
        float normal = 2;
        float scaleY = 5;

        Vector3 pos = Vector3.zero;
        Vector3 scale = Vector3.zero;
        GameObject wall = null;
        //  up
        pos.x = 0;pos.y = scaleY * 0.5f; pos.z = m_v2WidHeiHigh.y + 2;
        scale.x = scaleX;scale.y = scaleY; scale.z = normal;
        wall = Instantiate(m_colliderWall, pos, Quaternion.identity);
        wall.transform.localScale = scale;
        //  right
        pos.x = m_v2WidHeiHigh.x+2; pos.y = scaleY*0.5f; pos.z = 0;
        scale.x = normal; scale.y = scaleY; scale.z = scaleZ;
         wall = Instantiate(m_colliderWall, pos, Quaternion.identity);
        wall.transform.localScale = scale;
        //  down
        pos.x = 0; pos.y = scaleY * 0.5f; pos.z = m_v2WidHeiLow.y -2;
        scale.x = scaleX; scale.y = scaleY; scale.z = normal;
         wall = Instantiate(m_colliderWall, pos, Quaternion.identity);
        wall.transform.localScale = scale;
        //  left
        pos.x = m_v2WidHeiLow.x - 2; pos.y = scaleY * 0.5f; pos.z = 0;
        scale.x = normal; scale.y = scaleY; scale.z = scaleZ;
        wall = Instantiate(m_colliderWall, pos, Quaternion.identity);
        wall.transform.localScale = scale;
    }
    private void LoadSaveSlots()
    {
        InitLoadBaseType();
        InitLoadLevelData();
    }
    public void OnBuildMgr()
    {
        if (gameObject.activeSelf == false)
        {
            gameObject.SetActive(true);
           
        }
    }
    private void InitLoadBaseType()//   슬롯의 베이스 정보를 로드한다
    {
        for (int y = 0; y < m_2lstSlots.Count; ++y)
        {
            for (int x = 0; x < m_2lstSlots[y].Count; ++x)
            {
                int idx = g_BuildTypeBase.List2BuildInfo[y][x];

                BuildSlot slot = m_2lstSlots[y][x].GetComponent<BuildSlot>();
                slot.BuildType = idx;
            }

        }
    }
    private void InitLoadLevelData()//   슬롯의 세이브 데이터를 로드한다
    {
        for (int y = 0; y < m_2lstSlots.Count; ++y)
        {
            for (int x = 0; x < m_2lstSlots[y].Count; ++x)
            {
                int type = g_BuildTypeBase.List2BuildInfo[y][x];
                int level = g_BuildLevelData.List2BuildInfo[y][x];

                GameObject prep = GetBuildPrefab(type, level);
                if (prep == null)
                {
                    Debug.LogError("InitLoadSaveData Err!! level is overflow");
                    return;
                }
                BuildSlot slot = m_2lstSlots[y][x].GetComponent<BuildSlot>();
                slot.BuildBuilding(prep, type, level, g_v3BuildingScale);
            }
        }
        InitWaitBuild();
    }
    private void OnEnable()
    {
        OnBuildSlot();
    }
    private void OnDisable()
    {
        OffBuildSlot();
    }
    #endregion//SaveLoadToChangeScene

    //  이하는 거의 안쓸것
    private void InitBuildSlot()
    {
        Vector3 gaps = g_v3BuildGap * 0.5f;

        GameObject slot = null;
        for (int i = 0; i < g_v2ColliderCellCnt.y; ++i)
        {
            m_2lstSlots.Add(new List<GameObject>());
            for (int j = 0; j < g_v2ColliderCellCnt.x; ++j)
            {
                Vector3 pos = new Vector3
                {
                    x = g_v3BuildCenter.x + g_v3BuildStartPos.x + ((g_v3BuildSlotSize.x + gaps.x) * j),
                    y = g_v3BuildCenter.y,
                    z = g_v3BuildCenter.z + g_v3BuildStartPos.z + ((g_v3BuildSlotSize.z + gaps.z) * i)
                };
                slot = Instantiate(g_originBuildSlot, pos, Quaternion.identity);
                slot.transform.localScale = g_v3BuildSlotSize;
                m_2lstSlots[i].Add(slot);
            }
        }
    }
    #region BuildUI

    public bool IsOnBuildUI()
    {
        return m_bOnBuildUI;
    }
    public void OnBuildUI()
    {
        m_bOnBuildUI = true;
        g_buildUI.SetActive(true);
        GameObject prep = GetBuildPrefab(m_waitBuildSlot, m_waitBuildSlot.NextBuildLevel);
        if(prep == null)
        {
            g_buildUI.GetComponent<BuildUI>().OnResInfo("-Final-", true);
            return;
        }
        BuildResource r = prep.GetComponent<BuildResource>();
        if(r != null)
        {
            g_buildUI.GetComponent<BuildUI>().OnResInfo(r.ResInfoText());
        }
        else
        {
            g_buildUI.GetComponent<BuildUI>().OnResInfo("-Final-", true);
        }

    }
    public void OffBuildUI()
    {
        m_bOnBuildUI = false;
        g_buildUI.SetActive(false);
    }
    public int WaitBuild()
    {
        return m_buildUIScript.g_UIStatus;
    }
    public void InitWaitBuild()
    {
        m_nWaitBuildType = -1;
        m_v3WaitBuildPos = Vector3.zero;
        m_qutWaitBuildRot = Quaternion.identity;
        m_waitBuildSlot = null;
    }
    bool CheckBuildPossible(GameObject _prep)
    {
        BuildResource info = _prep.GetComponent<BuildResource>();
        if(info == null)
        {
            return false;
        }
        if(info.IsBuildPossible)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

   

    public void BuildUIOkBuild()//    빌드유아이에서 출력할녀석
    {
        GameObject prep = GetBuildPrefab(m_waitBuildSlot, m_waitBuildSlot.NextBuildLevel);
        if (prep == null || !CheckBuildPossible(prep)) return;

        BuildBuilding(m_waitBuildSlot, prep, m_waitBuildSlot.BuildType, m_waitBuildSlot.NextBuildLevel);
        prep.GetComponent<BuildResource>().SubResource(m_inventory.GetComponent<Inventory>());
        //  왜냐하면 BuildBuilding 이걸 거치면 level이 늘어나기 때문에 현재의 레벨을 기록하면 된다.
        g_BuildLevelData.WriteData(m_waitBuildSlot.ListCoord, m_waitBuildSlot.BuildLevel);
    }
    public void BuildBuilding(BuildSlot _slot, GameObject _prep, int _type, int _level) => _slot.BuildBuilding(_prep, _type, _level, g_v3BuildingScale);
    #endregion
    void PrintBuildInfo()
    {
        switch (m_waitBuildSlot.BuildType)
        {
            case 0:Debug.Log("BuildType : Unique");break;
            case 1: Debug.Log("BuildType : House"); break;
            case 2: Debug.Log("BuildType : BlackSmith"); break;
            case 3: Debug.Log("BuildType : Shop"); break;
            case 4: Debug.Log("BuildType : Soldier"); break;
            case 5: Debug.Log("BuildType : Road"); break;
            default:
                break;
        }
        Debug.Log("Build Level : "+m_waitBuildSlot.BuildLevel.ToString());
    }
    public void SetWaitBuildInfo(BuildSlot _slot)
    {
        m_waitBuildSlot = _slot;
        PrintBuildInfo();
    }
    
    public void OffBuildSlot()
    {
        if(m_buildMenuOn)
        {
            m_buildMenuOn = false;
            for (int i = 0; i < m_2lstSlots.Count; ++i)
            {
                for (int j = 0; j < m_2lstSlots[i].Count; ++j)
                {
                    if (m_2lstSlots[i][j] != null && m_2lstSlots[i][j].activeSelf == true)
                    {
                        m_2lstSlots[i][j].SetActive(false);
                    }

                }
            }
        }

    }
    public void OnBuildSlot()
    {
        if (!m_buildMenuOn)
        {
            m_buildMenuOn = true;
            for (int i = 0; i < m_2lstSlots.Count; ++i)
            {
                for (int j = 0; j < m_2lstSlots[i].Count; ++j)
                {
                    if (m_2lstSlots[i][j].activeSelf == false)
                    {
                        m_2lstSlots[i][j].SetActive(true);
                    }
                }
            }
        }
    }
    public GameObject GetBuildPrefab(BuildSlot _slot, int _level) => GetBuildPrefab(_slot.BuildType, _level);
    public GameObject GetBuildPrefab(int _buildType, int _buildLevel)
    {
        GameObject _obj = null;
        switch (_buildType)
        {
            case (int)VillageBuild.Enum.BuildType.Unique:
                if (g_lstUnique.Count > _buildLevel)
                {
                    _obj = g_lstUnique[_buildLevel];
                }
                break;
            case (int)VillageBuild.Enum.BuildType.House:
                if (g_lstHouse.Count > _buildLevel)
                {
                    _obj = g_lstHouse[_buildLevel];
                }
                break;
            case (int)VillageBuild.Enum.BuildType.BlackSmith:
                if (g_lstBlackSmith.Count > _buildLevel)
                {
                    _obj = g_lstBlackSmith[_buildLevel];
                }
                break;
            case (int)VillageBuild.Enum.BuildType.Shop:
                if (g_lstShop.Count > _buildLevel)
                {
                    _obj = g_lstShop[_buildLevel];
                }
                break;
            case (int)VillageBuild.Enum.BuildType.Soldier:
                if (g_lstSoldier.Count > _buildLevel)
                {
                    _obj = g_lstSoldier[_buildLevel];
                }
                break;
            case (int)VillageBuild.Enum.BuildType.Road:
                if (g_lstRoad.Count > _buildLevel)
                {
                    _obj = g_lstRoad[_buildLevel];
                }
                break;
        }
        if(_obj == null)
        {
            Debug.Log("overFlow");
        }
        return _obj;
    }

    void Update()
    {
        if(!m_bBuildOn)
        {
            return;
        }
        if (m_buildMenuOn && !m_bOnBuildUI && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out m_rayHit))
            {
                if (m_rayHit.collider.CompareTag("BuildSlot"))
                {
                    GameObject obj = m_rayHit.collider.gameObject;
                    BuildSlot slot = obj.GetComponent<BuildSlot>();
                    if (slot == null) return;
                    if (!slot.BuildPossible) return;
                    SetWaitBuildInfo(slot);
                    //SetWaitBuilInfo(slot.BuildType, obj.transform.position, obj.transform.rotation, slot);
                    OnBuildUI();
                }
                
            }
        }



    }
}
