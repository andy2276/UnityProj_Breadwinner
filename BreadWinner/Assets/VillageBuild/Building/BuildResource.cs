﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
class BuildNeedRes
{
    public string m_strResName = "";
    public int m_nCnt = 0;

}


public class BuildResource : MonoBehaviour
{
    [SerializeField] List<BuildNeedRes> m_lstNeedRes = new List<BuildNeedRes>();
    bool m_bBuildPossible = false;

    public bool IsBuildPossible { get => m_bBuildPossible; }

    public string ResInfoText()
    {
        int num = 0;
        string regist = "";
        for(int i = 0; i < m_lstNeedRes.Count; ++i)
        {
            num = BuildMgr.Inst.Inventory.FindItem(m_lstNeedRes[i].m_strResName);
            if(m_lstNeedRes[i].m_nCnt <= num )
            {
                m_bBuildPossible = true;
            }
            else
            {
                m_bBuildPossible = false;
            }
            regist += m_lstNeedRes[i].m_strResName + " " +
                num.ToString() + " " + m_lstNeedRes[i].m_nCnt.ToString() + "\n";
        }
        return regist;
    }
    public void SubResource(Inventory _inven)
    {
        for (int i = 0; i < m_lstNeedRes.Count; ++i)
        {
            BuildMgr.Inst.Inventory.AddItemCnt(m_lstNeedRes[i].m_strResName,-m_lstNeedRes[i].m_nCnt);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
