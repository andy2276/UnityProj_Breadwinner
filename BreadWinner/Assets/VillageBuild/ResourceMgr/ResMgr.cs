﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Res.Enum;
namespace Res
{
    namespace Enum
    {
        public enum Type
        {
            Rope,Lumber,Wood,Coal,Quartz,Stone,Wool,Lead,Bronze,End
        }
    }

   [System.Serializable]
    class ResInfo
    {
        public Res.Enum.Type m_eResType = Enum.Type.End;
        public Texture2D m_texImage = null;
        public int m_nCount = 0;
    }


}


public class ResMgr : MonoBehaviour
{
    static ResMgr m_Inst = new ResMgr();
    private string m_imagePath = "Assets/";
    private Res.Enum.Type[] m_arrResType = { Type.Rope, Type.Lumber, Type.Wood, Type.Coal, Type.Quartz, Type.Stone, Type.Wool, Type.Lead, Type.Bronze };
    private List<Res.ResInfo> m_lstResInfo = new List<Res.ResInfo>();

    public static ResMgr Inst
    {
        get
        {
            if(m_Inst == null)
            {
                m_Inst = new ResMgr();
            }
            return m_Inst;
        }


    }
    
    void Init()
    {
       

    }

    // Start is called before the first frame update
    void Start()
    {
        if(m_Inst == null)
        {
            m_Inst = this;
            DontDestroyOnLoad(this);
        }
       
    }
   

    // Update is called once per frame
    void Update()
    {
        
    }
}
