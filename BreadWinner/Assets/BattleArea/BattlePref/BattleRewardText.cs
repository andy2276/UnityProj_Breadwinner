﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleRewardText : MonoBehaviour
{
    [SerializeField] Text m_type = null;
    [SerializeField] Text m_num = null;
    // Start is called before the first frame update
    void Start()
    {
        if(m_type == null)
        {
            m_type = transform.GetChild(0).GetComponent<Text>();
        }
        if (m_num == null)
        {
            m_num = transform.GetChild(1).GetComponent<Text>();
        }
    }
    public void WriteText(string _name,int _cnt)
    {
        m_type.text = _name;
        m_num.text = _cnt.ToString();
    }
}
