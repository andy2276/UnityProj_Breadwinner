﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BattleReward : MonoBehaviour
{
    [SerializeField] ScrollRect m_scrollRect = null;
    [SerializeField] RectTransform m_content = null;
    [SerializeField] GameObject m_Item = null;
    List<GameObject> m_lstContext = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
       if(m_scrollRect == null)
        {
            m_scrollRect = transform.Find("Scroll View").gameObject.GetComponent<ScrollRect>();
        }
       if(m_content == null)
        {
            m_content = transform.Find("Content").gameObject.GetComponent<RectTransform>();
        }
    }
    void ClearReward()
    {
        for(int i = m_lstContext.Count -1; 0 <= i;--i )
        {
            Destroy(m_lstContext[i]);
            m_lstContext[i] = null;
        }
        m_lstContext.Clear();
    }
    public void OnReward(Inventory _inven)
    {
        GameObject obj = null;
        Vector3 pos = Vector3.zero;
        // 보상과 배율이 적용된 변경된 값을 여기다가 들고와라
        ClearReward();
        foreach (var i in _inven.BattleStorage)
        {
            obj = Instantiate(m_Item, pos, Quaternion.identity);
            obj.transform.SetParent(m_content);
            obj.GetComponent<BattleRewardText>().WriteText(i.Key, i.Value);
            m_lstContext.Add(obj);
            pos.y -= 200;
            obj = null;
        }
    }
}
