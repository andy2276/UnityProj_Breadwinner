﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpper : MonoBehaviour
{
    [SerializeField] private TextMesh m_texMesh = null;
    [SerializeField] private float m_fStartSize = 0.1f;
    private float m_fCurSize = 0;
    [SerializeField] private float m_fEndSize = 1;
    [SerializeField] private float m_fLifeTime = 2;
    private float m_fCurLifeTime = 0;
    [SerializeField] private float m_fSpeed = 1;
    [SerializeField] private Vector2 m_v2SpeedSeed = new Vector2(20,30 );
    private string m_strDamage = "";
    private Vector3 m_v3Dir = Vector3.up;
    private Vector3 m_v3Target = Vector3.zero;
    // Start is called before the first frame update
    void Awake()
    {
        m_fCurSize = m_fStartSize;
        if (m_texMesh == null)
        {
            m_texMesh = GetComponent<TextMesh>();
        }
        if(m_texMesh == null)
        {
            EndUpper();
        }
        float randomX = Random.Range(0, 1);
        float randomZ = Random.Range(0, 1);
        m_v3Dir = Vector3.up;
        m_v3Dir.x = randomX;
        m_v3Dir.z = randomZ;
        m_fSpeed = Random.Range(m_v2SpeedSeed.x, m_v2SpeedSeed.y);
    }
    void EndUpper()
    {
        Destroy(gameObject);
    }
    public void SetTarget(Vector3 _pos)
    {
        m_v3Target = _pos;
        transform.LookAt(m_v3Target);
    }
    public void SetFont(float _Damage,Color _color)
    {
        m_strDamage = _Damage.ToString();
        m_texMesh.text = m_strDamage;
        m_texMesh.color = _color;
    }
   
    // Update is called once per frame
    void Update()
    {
        if(m_fEndSize <= m_fCurSize || m_fLifeTime <= m_fCurLifeTime)
        {
            EndUpper();
            return;
        }
        else
        {
            float time = Time.deltaTime;
            m_fCurLifeTime += time;
            float fNormalLife = m_fCurLifeTime / m_fLifeTime;
            m_fCurSize = Mathf.Lerp(m_fStartSize, m_fEndSize, fNormalLife);
            m_texMesh.characterSize = m_fCurSize;
            
            transform.position += m_v3Dir * (m_fSpeed  * (1 - fNormalLife)) * time;
        }
        
    }
}
