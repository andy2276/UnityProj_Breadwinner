﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace BT
{
    namespace Virtual
    {
        public abstract class VSBASpon : Virtual.VSBATask
        {
            [SerializeField] protected BT.Enum.MonsterType m_eMonsterType = Enum.MonsterType.End;
            [SerializeField] protected int m_nSponCnt = 0;
            [SerializeField] protected int m_nSponPoint = 0;

            protected int m_nCurSpon = 0;
            public VSBASpon()
            {
                SetBATag();
            }
            protected override void SetBATag()
            {
                m_eNodeTag = Enum.NodeTag.BatSpon;
            }
            protected void DefaultCopySpon(VSBASpon _clone)
            {
                _clone.m_eMonsterType = m_eMonsterType;
                _clone.m_nSponCnt = m_nSponCnt;
                _clone.m_nSponPoint = m_nSponPoint;
            }
            protected void NodeWindow_DefaultBASponWnd()
            {
                EditorGUILayout.BeginVertical();
                m_eMonsterType =(Enum.MonsterType)EditorGUILayout.EnumPopup(m_eMonsterType, GUILayout.Width(100)) ;
                m_nSponPoint = EditorGUILayout.IntField("SponPoint", m_nSponCnt, GUILayout.Width(200));
                m_nSponCnt = EditorGUILayout.IntField("SponCnt", m_nSponCnt, GUILayout.Width(200));
                EditorGUILayout.EndVertical();
            }
            protected bool IsSponCntPossible() => (m_nCurSpon < m_nSponCnt);
            protected void ResetSponCnt() => m_nCurSpon = 0;
            protected void AddSponCnt() => ++m_nCurSpon;

            protected void SponMonster(BT.Enum.MonsterType _type)
            {
                Object prep = BattleAreaMgr.Inst.GetMonsterPrefab(_type);
                if (prep == null)
                {
                    Debug.LogError("Prefab non!!");
                    return;
                }
               
                m_battleArea.RegistPlayMonster(_type, m_nSponPoint);
                AddSponCnt();
            }
            protected void SponMonster()
            {
                Object prep = BattleAreaMgr.Inst.GetMonsterPrefab(m_eMonsterType);
                if (prep == null)
                {
                    Debug.LogError("Prefab non!!");
                    return;
                }

                m_battleArea.RegistPlayMonster(m_eMonsterType, m_nSponPoint);
                AddSponCnt();
            }

        }
    }
  
}
