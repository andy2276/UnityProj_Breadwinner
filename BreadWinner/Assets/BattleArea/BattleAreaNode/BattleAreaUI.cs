﻿using System.Collections;
using System.Collections.Generic;
using BT.Enum;
using UnityEngine;
using UnityEngine.UI;

public class BattleAreaUI : MonoBehaviour
{
    private bool m_bSelect = false;
    private BT.Enum.AreaType m_eAreaType = BT.Enum.AreaType.End;
    private int m_nAreaLevel = 0;
    [SerializeField] Image m_sumnailImage = null;
    [SerializeField] List<Texture2D> m_lstSumnailImage;

    public bool Select { get => m_bSelect; set => m_bSelect = value; }
    public AreaType AreaType { get => m_eAreaType; set => m_eAreaType = value; }
    public int AreaLevel { get => m_nAreaLevel; set => m_nAreaLevel = value; }



    // Start is called before the first frame update
    void Start()
    {
       

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SelectButton()
    {
        m_bSelect = true;

    }
    public void LeftMapButton()
    {
       
       
    }
    public void RightMapButton()
    {
        
    }
}
