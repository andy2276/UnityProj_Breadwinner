﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;
namespace BT
{
    namespace BA
    {
        namespace Cdt
        {
            public class VSBAClearCnt : Virtual.VSBACondition
            {
                [SerializeField] int m_nSuccessMonsterCnt = 0;
                [SerializeField] float m_fEndTimeMinute = 30;//  30부
                float m_fCurEndTimeSecond = 0;

                public VSBAClearCnt()
                {
                    m_fEndTimeMinute *= 60;
                    m_wndNodeRect.width = 200;
                    m_wndNodeRect.height = 200;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBAClearCnt clone = (VSBAClearCnt)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultBATaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_nSuccessMonsterCnt = m_nSuccessMonsterCnt;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAClearCnt clone = (VSBAClearCnt)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultBATaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_nSuccessMonsterCnt = EditorGUILayout.IntField("SuccessCnt", m_nSuccessMonsterCnt, GUILayout.Width(200));
                    m_fEndTimeMinute = EditorGUILayout.FloatField("FailMinute", m_fEndTimeMinute, GUILayout.Width(200));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    m_fCurEndTimeSecond += Time.deltaTime;
                    if(m_fEndTimeMinute < m_fCurEndTimeSecond)
                    {
                        m_fCurEndTimeSecond = 0;
                        return Return.Fail;
                    }
                    else if(m_nSuccessMonsterCnt >= m_battleArea.ListPlayMonster.Count)
                    {
                        return Return.Success;
                    }
                    else
                    {
                        return Return.Running;
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAClearCnt clone = (VSBAClearCnt)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBAClearCnt>();

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAClearCnt clone = (VSBAClearCnt)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }
            }

        }

    }
}

