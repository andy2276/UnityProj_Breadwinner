﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;

namespace BT
{
    namespace Virtual
    {
        public abstract class VSBACondition : BT.Virtual.VSBATask
        {
            public VSBACondition()
            {
                SetBATag();
            }
            protected override void SetBATag()
            {
                m_eNodeTag = Enum.NodeTag.BatCdt;
            }
        }

    }
}