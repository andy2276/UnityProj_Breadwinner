﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour
{
    private bool m_bOffFade = false;
    [SerializeField] private float m_fFadeTime = 2;
    [SerializeField] private Image m_curtain = null;
    [SerializeField] private Color m_curtainColor = Color.black;
    [SerializeField] private float m_fLerpScale = 0.5f;

    private float m_fCurFadeTime = 0;
    private float m_fHalfFadeTime = 0;
    private float m_fLerpFadeValue = 0;

    public bool OffFade { get => m_bOffFade; set => m_bOffFade = value; }

    
    // Start is called before the first frame update
    void Start()
    {
        m_bOffFade = false;
        m_fHalfFadeTime = m_fFadeTime * 0.5f;

    }

    // Update is called once per frame
    void Update()
    {
        if(m_curtain != null)
        {
            if(m_fCurFadeTime < m_fFadeTime)
            {
                if(m_fCurFadeTime < m_fHalfFadeTime)
                {
                    m_fLerpFadeValue = Mathf.Lerp(m_fLerpFadeValue, 1, m_fLerpScale);
                }
                else
                {
                    m_fLerpFadeValue = Mathf.Lerp(m_fLerpFadeValue, 0, m_fLerpScale);
                }
                m_curtainColor.a = m_fLerpFadeValue;
                m_fCurFadeTime += Time.deltaTime;
                m_curtain.color = m_curtainColor;
            }
            else
            {
                m_fCurFadeTime = 0;
                m_bOffFade = true;
            }
        }
        else
        {
            gameObject.SetActive(false);
            m_bOffFade = false;
        }
       
    }
}
