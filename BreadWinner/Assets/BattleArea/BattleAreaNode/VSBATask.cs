﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BT.Enum;

namespace BT
{
    namespace Virtual
    {
        public abstract class VSBATask : Virtual.VSBTNode
        {
            [SerializeField] protected bool m_bRepeat = false;
            
            protected void DefaultTastSave(VSBTNode _clone, VSBTNode _parent = null)
            {
                if (_parent != null)
                {
                    VSBTComposite parent = (VSBTComposite)_parent;
                    _clone.BTPath = parent.BTPath;
                    _clone.hideFlags = HideFlags.None;
                    parent.AddChild(_clone);
                    AssetDatabase.AddObjectToAsset(_clone, parent.BTPath);
                }
                //  Task는 테스크 대로 세이브 가능하게 에디터를 고쳐야함!
                //  11/20 아직안함! ㅋㅋㅋㅋ
            }
            public override VSBTNode LoadAI(List<VSBTNode> _list, VSBTNode _parent = null) => LoadBT(_list, _parent);
            protected void DefaultBATaskCopy(VSBATask _task)
            {
                _task.m_bRepeat = m_bRepeat;
                _task.m_battleArea = m_battleArea;
            }
            protected void NodeWindow_DefaultBATaskWnd()
            {
                EditorGUILayout.BeginVertical();
                m_bRepeat = GUILayout.Toggle(m_bRepeat, "ReturnRepeat", GUILayout.Width(200));
                EditorGUILayout.EndVertical();
            }
            protected Return ChangeStatus(BT.Return _KeepStatus, BT.Return _changeStatus)
            {
                if (!m_bRepeat)
                {
                    m_eState = _changeStatus;
                }
                else
                {
                    m_eState = _KeepStatus;
                }
                return _changeStatus;
            }
            public VSBATask()
            {
                m_eNodeCat = NodeCategory.Task;
            }
            protected abstract void SetBATag();
        }
    }
}