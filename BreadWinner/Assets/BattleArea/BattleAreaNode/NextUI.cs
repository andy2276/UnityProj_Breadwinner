﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextUI : MonoBehaviour
{
    // Start is called before the first frame update
    private bool m_bGoNext = false;
    private bool m_bStopNext = false;

    public bool GoNext { get => m_bGoNext;  }
    public bool StopNext { get => m_bStopNext;  }

    public void GoNextButton()
    {
        if(!m_bGoNext)
        {
            m_bGoNext = true;
        }
    }
    public void StopNextButton()
    {
        if(!m_bStopNext)
        {
            m_bStopNext = true;
        }

    }
    public bool IsButtonClick()
    {
        if (m_bGoNext || m_bStopNext) return true;
        else return false;
    }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
