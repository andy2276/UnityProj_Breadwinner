﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;
using BT.Enum;
namespace BT
{
    namespace BA
    {
        namespace Cdt
        {
            public class VSBAComparison : BT.Virtual.VSBACondition
            {
                [SerializeField] private BT.Enum.StatusVariable m_eDest = 0;
                //  <        >       >=      <=      ==      !=
                //  Under,   Over,   More,   less,   Same,   Not
                [SerializeField] private BT.Enum.DstCompScr m_eComp = Enum.DstCompScr.less;
                [SerializeField] private float m_fScr = 0;
                [SerializeField] private GameObject m_target = null;
                public VSBAComparison()
                {
                    m_wndNodeRect.width = 330;
                    m_wndNodeRect.height = 180;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBAComparison clone = (VSBAComparison)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultBATaskCopy(clone);
                    clone.m_parent = _parent;
                    clone.m_eDest = m_eDest;
                    clone.m_eComp = m_eComp;
                    clone.m_fScr = m_fScr;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAComparison clone = (VSBAComparison)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultBATaskWnd();
                    EditorGUILayout.BeginVertical();
                    m_target =(GameObject)EditorGUILayout.ObjectField("TargetObj", m_target, typeof(GameObject),true);
                    string strComp = "";
                    switch (m_eComp)
                    {
                        case Enum.DstCompScr.Under: strComp = " < "; break;
                        case Enum.DstCompScr.Over: strComp = " > "; break;
                        case Enum.DstCompScr.less: strComp = " <= "; break;
                        case Enum.DstCompScr.More: strComp = " >= "; break;
                        case Enum.DstCompScr.Same: strComp = " == "; break;
                        case Enum.DstCompScr.Not: strComp = " != "; break;
                        default: break;
                    }
                    EditorGUILayout.LabelField("if ( " + m_eDest.ToString() + strComp + m_fScr.ToString() + " ) ");
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginHorizontal();
                    m_eDest = (StatusVariable)EditorGUILayout.EnumPopup(m_eDest, GUILayout.Width(120));
                    EditorGUILayout.LabelField("Is", GUILayout.Width(15));
                    m_eComp = (DstCompScr)EditorGUILayout.EnumPopup(m_eComp, GUILayout.Width(70));
                    m_fScr = EditorGUILayout.FloatField(m_fScr, GUILayout.Width(70));
                    EditorGUILayout.EndHorizontal();
                    //  aiProperty 써야함
                }

                public override Return Evaluate()
                {
                    if (BT.Return.Evaluate != m_eState)
                    {
                        return m_eState;
                    }
                    float dest = 0;
                    BreadWinner bw = m_target.GetComponent<BreadWinner>();
                    switch (m_eDest)
                    {
                        case Enum.StatusVariable.fHp: dest = bw.Status.AiHp; break;
                        case Enum.StatusVariable.fAttackDamage: dest = bw.Status.AiDamage; break;
                        case Enum.StatusVariable.fMoveSpeed: dest = bw.Status.AiMoveSpeed; break;
                        case Enum.StatusVariable.fAttackSpeed: dest = bw.Status.AiAttackSpeed; break;
                        case Enum.StatusVariable.fSerachRange: dest = bw.Status.AiSearchRange; break;
                        case Enum.StatusVariable.fAttackRange: dest = bw.Status.AiAttackRange; break;
                        case Enum.StatusVariable.fNearRange: dest = bw.Status.AiNearRange; break;
                        default: break;
                    }
                    bool bBoolean = true;
                    switch (m_eComp)
                    {
                        case Enum.DstCompScr.Under: bBoolean = (dest < m_fScr) ? true : false; break;
                        case Enum.DstCompScr.Over: bBoolean = (dest > m_fScr) ? true : false; break;
                        case Enum.DstCompScr.less: bBoolean = (dest <= m_fScr) ? true : false; break;
                        case Enum.DstCompScr.More: bBoolean = (dest >= m_fScr) ? true : false; break;
                        case Enum.DstCompScr.Same: bBoolean = (dest == m_fScr) ? true : false; break;
                        case Enum.DstCompScr.Not: bBoolean = (dest != m_fScr) ? true : false; break;
                        default: break;
                    }
                    if (bBoolean)
                    {
                        ChangeStatus(Return.Evaluate, Return.Success);
                        return m_eState;
                    }
                    else
                    {
                        ChangeStatus(Return.Evaluate, Return.Fail);
                        return m_eState;
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAComparison clone = (VSBAComparison)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBAComparison>();
                
                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAComparison clone = (VSBAComparison)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }
                
            }

        }

    }

}
