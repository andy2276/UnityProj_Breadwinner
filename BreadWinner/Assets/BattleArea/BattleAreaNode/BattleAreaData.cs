﻿using System.Collections;
using System.Collections.Generic;
using BT.Enum;
using UnityEngine;

namespace BT
{
    namespace Enum
    {
        public enum AreaType
        {
            Grassland,Volcano,Dungeon,End
        }
    }
}

[CreateAssetMenu(fileName = "BattleAreaData",menuName = "BattleArea/BattleAreaData",order = int.MaxValue)]
public class BattleAreaData : ScriptableObject
{
  
    [SerializeField] private BT.Enum.AreaType m_eAreaType = new AreaType();
    [SerializeField] private Object m_AreaMapPref = null;
    [SerializeField] private Object m_BAAsset = null;            //  루트노드를 담고있는 에셋을 들고온다
    [SerializeField] private List<GameObject> m_lstSponPoint = new List<GameObject>();   //  스폰 포인터들을 들고온다
    [SerializeField] private List<AIStatus> m_lstLevelScale = new List<AIStatus>();

    public AreaType AreaType { get => m_eAreaType; }
    public Object AreaPref { get => m_AreaMapPref; }
    public Object BAAsset { get => m_BAAsset;  }
    public List<GameObject> lstSponPoint { get => m_lstSponPoint; }
    public List<AIStatus> LstLevelScale { get => m_lstLevelScale; set => m_lstLevelScale = value; }
}
