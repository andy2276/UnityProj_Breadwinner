﻿using System.Collections;
using System.Collections.Generic;
using BT.BA;
using UnityEngine;

namespace BT
{
    namespace Enum
    {
        public enum MonsterType
        {
           Bat,EvilMage,Golem, Hatchling,Orc,Plant,Skell,Slime,Spider,Turtle, End
        }
        public enum BattleMgrEvent
        {
            Selecting,Starting,Waiting,Playing,Next,Ending,End
        }
    }
    namespace EditItem
    {
        [System.Serializable]
        public class DicMonster
        {
            public BT.Enum.MonsterType m_type = BT.Enum.MonsterType.End;
            public Object m_prefab = null;
        }
        [System.Serializable]
        public class DicList<TKey, TValue>
        {
            public TKey m_Key;
            public TValue m_value;
        }
    }

}


public class BattleAreaMgr : MonoBehaviour
{
    private static BattleAreaMgr m_Inst = null;
    private GameObject m_defaultSponPoint;
    #region BattleEvent
    
    private delegate void BattleEvent();
    BattleEvent m_delBattleEvent = null;

    #endregion
    [SerializeField] private GameObject m_battleUI = null;
    [SerializeField] private GameObject m_nextUI = null;
    [SerializeField] private GameObject m_fadeInOut = null;
    [SerializeField] private float m_fMaxPlayTime = 0;
    [SerializeField] private GameObject m_damageFont = null;
    [SerializeField] private Inventory m_inventory = null;

    #region Test
    [SerializeField] private bool m_bTest = false;
    [SerializeField] private BT.Enum.AreaType m_eTestAreaType = BT.Enum.AreaType.End;
    [SerializeField] private int m_nTestAreaLevel = 0;
    [SerializeField] private GameObject m_player = null;
    [SerializeField] BattleCamera m_battleCamera = null;
    #endregion
    private float m_fCurPlayTime = 0;

    BT.Enum.AreaType m_eSelectArea = BT.Enum.AreaType.End;
    GameObject m_curPlayAreaObj = null;
    BattleArea m_curPlayAreaScript = null;
  
    //  new
    private BattleAreaUI m_battleUIScript = null;
    private NextUI m_nextUiScript = null;
    private FadeInOut m_fadeInOutScript = null;
    private bool m_bBattleUI = false;
    private bool m_bNextUI = false;

    private int m_nAreaLevelScale = 0;
    private BT.Enum.BattleMgrEvent m_ePreMgrEvent;
    private BT.Enum.BattleMgrEvent m_eCurMgrEvent;

    private float m_fCurWaitTime = 0;
    [SerializeField] public float[] m_arrWaitTime = new float[(int)BT.Enum.BattleMgrEvent.End];

    private int m_nCurAreaStep = 0;
    private bool m_bIsSucceess = true;
    private bool m_bGoNext = true;

    private delegate int NextEvent();
    NextEvent m_delNextEvent = null;


    [SerializeField] public List<BT.EditItem.DicList<BT.Enum.MonsterType, Object>> m_lstMonsters = new List<BT.EditItem.DicList<BT.Enum.MonsterType, Object>>();
    [SerializeField] public List<BT.EditItem.DicList<BT.Enum.AreaType, BattleAreaData>> m_lstAreaData = new List<BT.EditItem.DicList<BT.Enum.AreaType, BattleAreaData>>();
    Dictionary<BT.Enum.MonsterType, Object> m_dicMonsterPref;
    Dictionary<BT.Enum.AreaType, List<BattleAreaData>> m_dicAreaData;

    int m_nPreEventNum = 0;
    [SerializeField] float m_fMaxRewardTime = 5;
    float m_fCurRewardTime = 0;
    [SerializeField] GameObject m_clearUI = null;
    [SerializeField] GameObject m_normalRewardUI = null;
    [SerializeField] GameObject m_failUI = null;
    float m_fFailUiMaxTime = 3;
    float m_fFailUiCurTime = 0;

    public static BattleAreaMgr Inst
    {
        get
        {
            if (m_Inst == null)
            {
                m_Inst = new BattleAreaMgr();
            }
            return m_Inst;
        }
    }
    public static bool IsInst()
    {
        return (m_Inst != null);
    }
    public GameObject DefaultSponPoint { get => m_defaultSponPoint; }
    public int AreaLevelScale { get => m_nAreaLevelScale; set => m_nAreaLevelScale = value; }
    public GameObject Player { get => m_player; set => m_player = value; }
    public Inventory Inventory { get => m_inventory;  }

    //  맵선택은 지도에서 할거니깐 일단은 이걸 이렇게 만들고 나중에 바꾸기 쉽게 하자

    #region BagicMgrHelpFunc
    void Init()
    {
        m_defaultSponPoint = new GameObject();
       
      //  m_lstMonsters = new List<BT.EditItem.DicList<BT.Enum.MonsterType, Object>>();
        m_dicMonsterPref = new Dictionary<BT.Enum.MonsterType, Object>();
        m_dicAreaData = new Dictionary<BT.Enum.AreaType, List<BattleAreaData>>();

        if (m_nextUI != null)
        {
            m_nextUI.SetActive( false);
            m_nextUiScript = m_nextUI.GetComponent<NextUI>();
        }
        if(m_battleUI != null)
        {
            m_battleUI.SetActive(false);
            m_battleUIScript = m_battleUI.GetComponent<BattleAreaUI>();
        }
        m_delNextEvent = CheckNext;
        if(m_fadeInOut != null)
        {
            m_fadeInOut.SetActive(false);
            m_fadeInOutScript = m_fadeInOut.GetComponent<FadeInOut>();
        }
        if(m_clearUI != null)
        {
            if(m_clearUI.activeSelf)
            {
                m_clearUI.SetActive(false);
            }
        }
       
        if(m_failUI != null)
        {
            if (m_failUI.activeSelf)
            {
                m_failUI.SetActive(false);
            }
        }
    }
    
    void SortDic()
    {
        foreach (var n in m_lstMonsters)
        {
            m_dicMonsterPref.Add(n.m_Key, n.m_value);
        }
        foreach (var n in m_lstAreaData)
        {
            if (m_dicAreaData.ContainsKey(n.m_Key))
            {
                m_dicAreaData[n.m_Key].Add(n.m_value);
            }
            else
            {
                m_dicAreaData.Add(n.m_Key, new List<BattleAreaData>());
                m_dicAreaData[n.m_Key].Add(n.m_value);
            }
        }
    }
    void SetPlayer()
    {

    }
    #endregion
    #region BagicFunc
    private void Awake()
    {
        if (m_Inst == null)
        {
            m_Inst = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
      //  DontDestroyOnLoad(this);
        Init();
        SortDic();
        m_defaultSponPoint.transform.position = new Vector3(0, 0, 0);
    }
    void Update()
    {
        switch (m_eCurMgrEvent)
        {
            case BT.Enum.BattleMgrEvent.Selecting:
                m_delBattleEvent = MgrEvent_Selecting;
                break;
            case BT.Enum.BattleMgrEvent.Starting:
                m_delBattleEvent = MgrEvent_Starting;
                break;
            case BT.Enum.BattleMgrEvent.Waiting:
                m_delBattleEvent = MgrEvent_Wait;
                break;
            case BT.Enum.BattleMgrEvent.Playing:
                m_delBattleEvent = MgrEvent_Playing;
                break;
            case BT.Enum.BattleMgrEvent.Next:
                m_delBattleEvent = MgrEvent_Next;
                break;
            case BT.Enum.BattleMgrEvent.Ending:
                m_delBattleEvent = MgrEvent_Ending;
                break;
            case BT.Enum.BattleMgrEvent.End:
                m_delBattleEvent = null;
                break;
            default:
                break;
        }
        m_delBattleEvent?.Invoke();
    }
    #endregion
    #region Testing

    void CreatePlayer()
    {
        Vector3 pos = new Vector3(0, 0, 0);
        SceneMgr.Inst.PlayerData.m_playerObj = Instantiate(m_player, pos,Quaternion.identity);
    }

    #endregion
    #region UIFunc
    public bool IsOnBattleUI() => m_bBattleUI;
    public void OnBattleUI()
    {
        if(!m_bBattleUI)
        {
            m_bBattleUI = true;
            m_battleUI.SetActive(true);
        }
    }
    public void OffBattleUI()
    {
        if (m_bBattleUI)
        {
            m_bBattleUI = false;
            m_battleUI.SetActive(false);
        }
    }

    public bool IsOnNextUI() => m_bNextUI;
    public void OnNextUI()
    {
        if (m_nextUI != null && m_nextUI.activeSelf == false)
        {
            m_nextUI.SetActive(true);
        }
    }
    public void OffNextUI()
    {
        if (m_nextUI != null && m_nextUI.activeSelf == true)
        {
            m_nextUI.SetActive(false);
        }
    }
    void PlayerSetting()
    {
        if(m_player.activeSelf != true)
        {
            m_player.SetActive(true);
        }
        //  전달받을 정보 예를들어서 지금 선택된 무기와 체력정도?
        m_player.transform.position = m_curPlayAreaScript.StartPos.transform.position;
        if(m_battleCamera != null)
        {
            m_battleCamera.SetCameraOffset();
        }
    }
    #endregion
    #region MgrEvents
    void MgrEvent_Selecting()
    {
        if(m_bTest)
        {
            if(SceneMgr.Inst != null&& SceneMgr.Inst.BattleData.IsSelectMap)
            {
                Debug.Log("BattleArea type :" +SceneMgr.Inst.BattleData.AreaType.ToString());
                Debug.Log("BattleArea level :" +SceneMgr.Inst.BattleData.Level.ToString());
            }
            
            
         //   SceneMgr.Inst.PlayerData.m_playerObj = m_player;

            m_ePreMgrEvent = m_eCurMgrEvent;
            SetSelectArea(m_eTestAreaType);
            SetAreaLevel(m_nTestAreaLevel);
            m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting;
            m_bTest = false;
            return;
        }
        
        if (SceneMgr.Inst.BattleData.IsSelectMap == true)
        {
            Debug.Log("BattleArea type :" + SceneMgr.Inst.BattleData.AreaType.ToString());
            Debug.Log("BattleArea level :" + SceneMgr.Inst.BattleData.Level.ToString());

            m_ePreMgrEvent = m_eCurMgrEvent;
            SetSelectArea(SceneMgr.Inst.BattleData.AreaType);
            SetAreaLevel(SceneMgr.Inst.BattleData.Level);
            m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting;
            m_battleCamera = Camera.main.GetComponent<BattleCamera>();
        //    VillageSceneMgr.Inst.OffVillageSceneMgr();
        }
        else
        {
            Debug.Log("wait...");
        }
    }
    void MgrEvent_Starting()
    {
        m_ePreMgrEvent = m_eCurMgrEvent;
        LoadingArea();
        PlayerSetting();
        m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting;
    }
    void MgrEvent_Wait()
    {
        //  뭐 페이드 아웃을 할건지 뭐 할건지 선택하는곳임
        //  이전에 무슨 이벤트였나?
        float fScrTime = 0;
        fScrTime = m_arrWaitTime[(int)m_ePreMgrEvent];
        if(WaitTime(fScrTime))
        {
            switch (m_ePreMgrEvent)
            {
                case BT.Enum.BattleMgrEvent.Selecting:
                    m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Starting;
                    break;
                case BT.Enum.BattleMgrEvent.Starting:
                    m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Playing;
                    break;
                case BT.Enum.BattleMgrEvent.Waiting:
                    m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Selecting;
                    break;
                case BT.Enum.BattleMgrEvent.Playing:
                    if(m_bIsSucceess) m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Next;
                    else m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Ending;//  바로엔딩가는거 말고 생각해보자
                    break;
                case BT.Enum.BattleMgrEvent.Next:
                    if(m_bGoNext) m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Starting;
                    else m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Ending;
                    break;
                case BT.Enum.BattleMgrEvent.Ending:
                    m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Selecting;
                    break;
                default:
                    break;
            }
        }
    }
    void MgrEvent_Playing()
    {
        BT.Return re = BT.Return.Evaluate;
        if (m_fCurPlayTime < m_fMaxPlayTime)
        {
            m_fCurPlayTime += Time.deltaTime;
            re = m_curPlayAreaScript.Result;
        }
        else
        {
            m_fCurPlayTime = 0;
            re = BT.Return.Fail;
        }
        switch (re)
        {
            case BT.Return.Evaluate:
                break;
            case BT.Return.Success:
                m_ePreMgrEvent = BT.Enum.BattleMgrEvent.Playing;
                m_bIsSucceess = true;
                m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting; 
                break;
            case BT.Return.Fail:
                m_ePreMgrEvent = BT.Enum.BattleMgrEvent.Playing;
                m_bIsSucceess = false;
                m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting;
                break;
            case BT.Return.Running:
                break;
            default:
                break;
        }
    }
    void MgrEvent_Next()
    {
        //  0   :   논 
        //  1   :   클리어후에 다음으로 넘어갈수 있을때
        //  2   :   완전 클리어(퍼펙트 클리어 퍼텍트 보상)
        //  3   :   클리어 버튼 대기중
        //  4   :   다음으로
        //  5   :   보상으로(현재쌓인거 보상)
        //  6   :   다음단계로 이동
       
        int num = 0;
        if(m_delNextEvent != null)
        {
            num = m_delNextEvent();
            if(num == m_nPreEventNum)
            {
                num = m_nPreEventNum;
                return;
            }
            else
            {
                //  넥스트 애니메이션을 출력?! 단순 페이드인 테이드아웃?!
                switch (num)
                {
                    case 0://  0   :   논 
                        break;
                    case 1: //  1   :   클리어후에 다음으로 넘어갈수 있을때
                        m_delNextEvent = NextUI;
                        break;
                    case 2: //  2   :   완전 클리어(퍼펙트 클리어 퍼텍트 보상)
                        m_delNextEvent = GoHappyReward;
                        break;
                    case 3://  3   :   클리어 버튼 대기중
                        m_delNextEvent = NextUIButton;
                        break;
                    case 4: //  4   :   다음으로 스테이지로
                        m_delNextEvent = GoNextStage;
                        break;
                    case 5: //  5   :   보상으로(일반 보상)
                        m_delNextEvent = GoNormalReward;
                        break;
                    case 6://   6   :    wait 으로 가서 다음 단계로
                        m_ePreMgrEvent = m_eCurMgrEvent;
                        m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting;
                        break;

                    default:
                        break;
                }
            }
            
        }
    }


    void MgrEvent_Ending()
    {
        if(FailOrSuccess())
        {
            OffNextUI();
            ClearPlayAreaObj();
            m_ePreMgrEvent = m_eCurMgrEvent;
            m_eSelectArea = BT.Enum.AreaType.End;
            m_nAreaLevelScale = 0;
            m_nCurAreaStep = 0;
            m_delNextEvent = CheckNext;
            m_eCurMgrEvent = BT.Enum.BattleMgrEvent.Waiting;
            m_fadeInOut.SetActive(false);
            
            GoVillage();
        }
       
    }
    #endregion
    #region SelectArea
    void SetSelectArea(BT.Enum.AreaType _type)
    {
        if( _type != BT.Enum.AreaType.End)
        {
            m_eSelectArea = _type;
        }
    }
    void SetAreaLevel(int _level)   //  이거 안쓸가능성이 높아짐
    {
        if (_level < m_dicAreaData[m_eSelectArea].Count)
        {
            m_nAreaLevelScale = _level;
        }
    }

    #endregion
    #region Waiting
    bool WaitTime(float _maxTime)
    {
        if(m_fCurWaitTime < _maxTime)
        {
            m_fCurWaitTime += Time.deltaTime;
            return false;
        }
        else
        {
            m_fCurWaitTime = 0;
            return true;
        }
       
    }

    #endregion
    #region Starting

    void LoadingArea()
    {
        Object pref = m_dicAreaData[m_eSelectArea][m_nCurAreaStep].AreaPref;
        m_curPlayAreaObj = Instantiate(pref, Vector3.zero, Quaternion.identity) as GameObject;
        m_curPlayAreaScript = m_curPlayAreaObj.GetComponent<BattleArea>();
        m_curPlayAreaScript.AreaData = m_dicAreaData[m_eSelectArea][m_nCurAreaStep];
        m_curPlayAreaScript.Init();

    }

    #endregion
    #region Playing



    #endregion
    #region Next
    void ClearPlayAreaObj()
    {
        if(m_curPlayAreaObj != null)
        {
            m_curPlayAreaScript.ClearBattleArea();
            m_curPlayAreaScript.DeleteSponner();
            Destroy(m_curPlayAreaObj);
            m_curPlayAreaObj = null;
            m_curPlayAreaScript = null;

        }
    }
   
    int CheckNext()
    {
        int nNext = m_nCurAreaStep + 1;
        if (nNext < m_dicAreaData[m_eSelectArea].Count)
        {
            //  다음으로 넘어갈수 있음
            return 1;
        }
        else
        {
            //  다음으로 넘어가는게 아니라 끝남 GoHappyReward
            return 2;
        }
    }
    
    int NextUI()
    {
        if(m_nextUI != null)
        {
            OnNextUI();
            if(m_nextUiScript.IsButtonClick())
            {
                return 3;
            }

        }
        return 0;
    }
    int NextUIButton()
    {
        if (m_nextUiScript.GoNext) return 4;
        if (m_nextUiScript.StopNext) return 5;
        return 0;
    }
   
    int GoNextStage()
    {
        if(!m_fadeInOut.activeSelf)
        {
            m_fadeInOut.SetActive(true);
        }
        if(m_fadeInOutScript.OffFade)
        {
            OffNextUI();
            ClearPlayAreaObj();
            m_fadeInOut.SetActive(false);
            ++m_nCurAreaStep;
            m_bGoNext = true;
            m_delNextEvent = CheckNext;
            return 6;
        }
        return 0;
    }
    //  레벨에 따라서 늘어남 난이도도 해야함
    bool NormalReward()
    {
        if (m_fCurRewardTime <= m_fMaxRewardTime)
        {
            //  이걸 리워드로 바꿀까?
            m_fCurRewardTime += Time.deltaTime;
            if (m_normalRewardUI.activeSelf == false)
            {
                m_normalRewardUI.SetActive(true);
                float add = m_nAreaLevelScale * 0.25f;
                CaculReward(1 + add);
                m_normalRewardUI.GetComponent<BattleReward>().OnReward(m_inventory);

            }
            return false;
        }
        else
        {
            if (m_normalRewardUI.activeSelf == true)
            {
                m_normalRewardUI.SetActive(false);
            }
           
            m_fCurRewardTime = 0;
            return true;
        }
    }
    int GoNormalReward()
    {
        OffNextUI();
        if (NormalReward())
        {
            m_bGoNext = false;
            return 6;
        }
        return 5;
    }
    void CaculReward(float _scale)
    {
        if(m_inventory != null)
        {
            m_inventory.RewardItem(_scale);
        }
    }
    bool HappyReward()
    {
        //  Clear 창이 뜬다.
        //  몇초후 끝남
        if (m_fCurRewardTime <= m_fMaxRewardTime)
        {
            //  이걸 리워드로 바꿀까?
            m_fCurRewardTime += Time.deltaTime;
            if (m_clearUI.activeSelf == false)
            {
                m_clearUI.SetActive(true);
                float add = m_nAreaLevelScale * 0.5f;
                CaculReward(1 + add);
                m_clearUI.GetComponent<BattleReward>().OnReward(m_inventory);
            }
            return false;
        }
        else
        {
            if (m_clearUI.activeSelf == true)
            {
                m_clearUI.SetActive(false);
            }
            m_fCurRewardTime = 0;
            return true;
        }
       
      
    }
    int GoHappyReward()
    {
        OffNextUI();
        if (HappyReward())
        {
            m_bGoNext = false;
            return 6;
        }
        return 2;
    }


    #endregion
    #region Ending
    void GoVillage()
    {
        SceneMgr.Inst.InitBattleData();
 //     VillageSceneMgr.Inst.OnVillageSceneMgr();
        SceneMgr.LoadScene("Village");
    }
    bool OnFailUi()
    {
        if(m_failUI.activeSelf == false)
        {
            m_failUI.SetActive(true);
        }

        if (m_fFailUiCurTime < m_fFailUiMaxTime)
        {
            m_fFailUiCurTime += Time.deltaTime;
            return true;
        }
        else
        {
            m_fFailUiCurTime = 0;
            if (m_failUI.activeSelf == true)
            {
                m_failUI.SetActive(false);
            }
            return false;
        }
    }

    bool FailOrSuccess()
    {
        if (!m_bIsSucceess)
        {
            if(OnFailUi())
            {
                return false;
            }
            else
            {
                float add = m_nAreaLevelScale * 0.1f;
                CaculReward(0.5f + add);
                return true;
            }
        }
        else
        {
            return true;
        }

    }


    #endregion

    // Update is called once per frame

    public Object GetMonsterPrefab(BT.Enum.MonsterType _type)
    {
        if (m_dicMonsterPref.ContainsKey(_type))
        {
            return m_dicMonsterPref[_type];
        }
        return null;
    }
    public void OnBattleAreaMgr()
    {
        if(gameObject.activeSelf == false)
        {
            gameObject.SetActive(true);
        }
    }

    public void OffBattleAreaMgr()
    {
        if (gameObject.activeSelf == true)
        {
            gameObject.SetActive(false);
        }
    }
    public void CreateDamageUpper(Vector3 _pos,float _damage,Color _color)
    {
        GameObject obj = Instantiate(m_damageFont, _pos, Quaternion.identity) as GameObject;
        DamageUpper upper = obj.GetComponent<DamageUpper>();
        upper.SetFont(_damage, _color);
        upper.SetTarget(Camera.main.transform.position);
    }

}
