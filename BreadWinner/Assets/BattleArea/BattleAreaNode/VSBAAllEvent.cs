﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;

namespace BT
{
    namespace EditItem
    {
        public delegate Return BattleEvent();
    }
    namespace Enum
    {
        public enum BattleEventType
        {
            //  현재 몬스터 수가 몇 이하다 
            //  몬스터 스폰
            //  플레이어가 죽었는가
            //  기다리기
            //  전체 몬스터 수가 몇 이하다
            //  다음 탄으로 넘어가라
            cdtCurMonsterLess,
            cdtPlayerDie,
            cdtClear,
            actSpon,
            actSponCnt,
            actEffect,
            actWait,
            actNext,
            actTurnBoolean,
            actTurnBTReturn,
          //  actGameEnd
            
                
        }
    }

    namespace BA
    {
        namespace Cdt
        {
            public class VSBAAllEvent : Virtual.VSBATask
            {
                public EditItem.BattleEvent m_delBattleEvent = null;
                #region saveVariable
                [SerializeField] Enum.BattleEventType   m_eEventType          = Enum.BattleEventType.actNext;
                [SerializeField] Return                 m_eSuccessValue  = Return.Success;
                [SerializeField] Return                 m_eFailValue     = Return.Fail;
                [SerializeField] BT.Enum.MonsterType    m_eMonsterType   = Enum.MonsterType.End;
                [SerializeField] bool                   m_bBoolean0       = false;
                [SerializeField] int                    m_nIntData0      = 0;
                [SerializeField] int                    m_nIntData1         = 0;
                [SerializeField] float                  m_fFloatData0       = 0;
                [SerializeField] float                  m_fFloatData1       = 0;
                [SerializeField] float                  m_fRunningTime      = 0;
                [SerializeField] BT.Return              m_eReturnValue      = Return.Evaluate;
                #endregion
                #region tempVariable

                float m_fCurRunningTime = 0;
                int m_nSponningCnt = 0;
                GUILayoutOption[] m_guiLayout = { GUILayout.Width(300) };
                Player m_player = null;
                Return m_eReturn;
                #endregion
                #region Init
                Return StartFunc()
                {
                    switch (m_eEventType)
                    {
                        case Enum.BattleEventType.cdtCurMonsterLess:
                            m_delBattleEvent = Cdt_CurMonsterLess;
                            break;
                        case Enum.BattleEventType.cdtPlayerDie:
                            m_player = BattleAreaMgr.Inst.Player.GetComponent<Player>();
                            m_delBattleEvent = Cdt_PlayerDie;
                            break;
                        case Enum.BattleEventType.actSpon:
                            m_delBattleEvent = Act_Spon;
                            break;
                        case Enum.BattleEventType.actSponCnt:
                            m_delBattleEvent = Act_SponCnt;
                            break;
                        case Enum.BattleEventType.actEffect:
                            m_delBattleEvent = Act_Effect;
                            break;
                        case Enum.BattleEventType.actWait:
                            m_delBattleEvent = Act_Wait;
                            break;
                        case Enum.BattleEventType.actNext:
                            m_delBattleEvent = Act_Next;
                            break;
                        case Enum.BattleEventType.actTurnBoolean:
                            m_delBattleEvent = Act_TurnBoolean;
                            break;
                        case Enum.BattleEventType.actTurnBTReturn:
                            m_delBattleEvent = Act_TurnBTReturn;
                            break;
                        case Enum.BattleEventType.cdtClear:
                            m_delBattleEvent = Cdt_Clear;
                            break;
                        default:
                            break;
                    }
                    return m_delBattleEvent.Invoke();
                }
                private void Awake()
                {
                    m_delBattleEvent = StartFunc;
                }
                #endregion

                #region BattleEvent


                Return Cdt_CurMonsterLess()//   3개필요    m_nIntData0 m_eSuccessValue m_eFailValue
                {
                    if(m_battleArea.ListPlayMonster.Count < m_nIntData0)
                    {
                        return m_eSuccessValue;
                    }
                    else
                    {
                        return m_eFailValue;
                    }
                }
                Return Cdt_PlayerDie()//    3개필요    m_fFloatData0   m_eSuccessValue m_eFailValue
                {
                    //  플레이어가 들어오기 전에는 이게 계속 걸릴거임.
                  
                    if (m_player.Status.AiHp < m_fFloatData0)
                    {
                        return m_eSuccessValue;
                    }
                    else
                    {
                        return m_eFailValue;
                    }
                }
                Return Cdt_Clear()
                {
                    if(m_battleArea.ListPlayMonster.Count == m_nIntData0)
                    {
                        return m_eSuccessValue;
                    }
                    else
                    {
                        return BT.Return.Running;
                    }

                }
                Return Cdt_HoleMonsterLess()//  삭제예정
                {
                    if (m_battleArea.ListPlayMonster.Count < m_nIntData0)
                    {
                        return m_eSuccessValue;
                    }
                    else
                    {
                        return m_eFailValue;
                    }
                }
                Return Act_Spon()// m_eMonsterType  m_nIntData0 m_eSuccessValue m_eFailValue
                {
                    if(Enum.MonsterType.End != m_eMonsterType)
                    {
                        m_battleArea.RegistPlayMonster(m_eMonsterType, m_nIntData0);
                        return m_eSuccessValue;
                    }
                    else
                    {
                        return m_eFailValue;
                    }
                }
                //  m_nIntData0    m_nIntData1    m_eMonsterType  m_fRunningTime    m_eSuccessValue m_eFailValue
                Return Act_SponCnt()//  int0 : 스포너번호 int1 : 스폰맥스카운트
                {
                    if (Enum.MonsterType.End != m_eMonsterType)//       몬스터가 맞는지 확인
                    {
                        if(m_nSponningCnt < m_nIntData1)//              스폰수를 확인
                        {
                            if (m_fCurRunningTime < m_fRunningTime)//   시간을 확인
                            {
                                m_fCurRunningTime += Time.deltaTime;
                                return Return.Running;
                            }
                            else//  스폰
                            {
                                ++m_nSponningCnt;
                                m_fCurRunningTime = 0;
                                m_battleArea.RegistPlayMonster(m_eMonsterType, m_nIntData0);
                                return BT.Return.Running;
                            }
                        }
                        else
                        {
                            m_fCurRunningTime = 0;
                            m_nSponningCnt = 0;
                            return m_eSuccessValue;
                        }
                    }
                    else
                    {
                        m_fCurRunningTime = 0;
                        m_nSponningCnt = 0;
                        return m_eFailValue;
                    }
                }
                Return Act_Effect()
                {
                    return Return.Success;
                }
                Return Act_Wait()// 2개필요    m_fRunningTime  m_eSuccessValue
                {
                    if(m_fCurRunningTime < m_fRunningTime)
                    {
                        m_fCurRunningTime += Time.deltaTime;
                        return Return.Running;
                    }
                    else
                    {
                        m_fCurRunningTime = 0;
                        return m_eSuccessValue;
                    }
                }
                Return Act_Next()
                {
                    return Return.Success;
                }
                Return Act_TurnBoolean()
                {
                    if(m_bBoolean0)
                    {
                        return m_eSuccessValue;
                    }
                    else
                    {
                        return m_eFailValue;
                    }
                }
                Return Act_TurnBTReturn()
                {
                    return m_eReturnValue;
                }

                #endregion

                public VSBAAllEvent()
                {
                    m_wndNodeRect.width = 330;
                    m_wndNodeRect.height = 390;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBAAllEvent clone = (VSBAAllEvent)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultBATaskCopy(clone);
                    clone.Parent = _parent;

                    clone.m_eEventType = m_eEventType;
                    clone.m_eSuccessValue = m_eSuccessValue;
                    clone.m_eFailValue = m_eFailValue;
                    clone.m_eMonsterType = m_eMonsterType;
                    clone.m_bBoolean0 = m_bBoolean0;
                    clone.m_nIntData0 = m_nIntData0;
                    clone.m_nIntData1 = m_nIntData1;
                    clone.m_fFloatData0 = m_fFloatData0;
                    clone.m_fFloatData1 = m_fFloatData1;
                    clone.m_fRunningTime = m_fRunningTime;
                    clone.m_eReturnValue = m_eReturnValue;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAAllEvent clone = (VSBAAllEvent)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultBATaskWnd();
                    EditorGUILayout.BeginVertical();
                    //EditorGUILayout.LabelField("FuncInfo" +
                    //    "< Cdt_CurMonsterLess >[ m_nIntData0 : 필요개수 ][ m_eSuccessValue :성공시 ][ m_eFailValue : 실패시 ]\n" +
                    //    "< Cdt_PlayerDie      >[ m_fFloatData0 : 체력이하 ][ m_eSuccessValue :성공시 ][ m_eFailValue : 실패시 ]\n" +
                    //    "< Act_Spon           >[ m_eMonsterType : 필요개수 ][ m_nIntData0 :스포너 ][ m_eSuccessValue : 성공시 ]\n" +
                    //    "                      [ m_eFailValue : 실패시 ]\n" +
                    //    "< Act_SponCnt        >[ m_nIntData0 : 스포너 ][ m_nIntData1 :스폰카운트 ][ m_eMonsterType : 몬스터타입 ]\n" +
                    //    "                      [ m_fRunningTime : 기다림시간 ][ m_eSuccessValue : 성공시 ][ m_eFailValue : 실패시 ]\n" +
                    //    "< Act_Wait           >[ m_fRunningTime : 기다림시간 ][ m_eSuccessValue :성공시 ][ m_eFailValue : 실패시 ]\n" ,GUILayout.Width(300));

                    m_eEventType =(Enum.BattleEventType)EditorGUILayout.EnumPopup(new GUIContent("EvntType","이 노드의 실행 함수"), m_eEventType, m_guiLayout);
                    m_eSuccessValue =           (Return)EditorGUILayout.EnumPopup(new GUIContent("Success ","이 노드에서 성공시 리턴"), m_eSuccessValue, m_guiLayout);
                    m_eFailValue =              (Return)EditorGUILayout.EnumPopup(new GUIContent("Fail    ","이 노드에서 실패시 리턴"), m_eFailValue, m_guiLayout);
                    m_eMonsterType =  (Enum.MonsterType)EditorGUILayout.EnumPopup(new GUIContent("MopType ","이 노드에서 스폰시 필요한 몬스터 타입"), m_eMonsterType, m_guiLayout);
                    m_bBoolean0 =                           EditorGUILayout.Toggle(new GUIContent("Bool0  ","이 노드에서 이 불값을 사용합니다"), m_bBoolean0, m_guiLayout); //  이거는 안쓸가능성이 있음
                    m_nIntData0 =                        EditorGUILayout.IntField(new GUIContent("Int0    ","비교할 정수형 0"), m_nIntData0, m_guiLayout);
                    m_nIntData1 =                        EditorGUILayout.IntField(new GUIContent("Int1    ","비교할 정수형 1"), m_nIntData1, m_guiLayout);
                    m_fFloatData0 =                    EditorGUILayout.FloatField(new GUIContent("Float0  ","비교할 실수형 0"), m_fFloatData0, m_guiLayout);
                    m_fFloatData1 =                    EditorGUILayout.FloatField(new GUIContent("Float1  ","비교할 실수형 1"), m_fFloatData1, m_guiLayout);
                    m_fRunningTime =                   EditorGUILayout.FloatField(new GUIContent("RunTime ","기다릴때 필요한 시간"), m_fRunningTime, m_guiLayout);
                    m_eReturnValue      =       (Return)EditorGUILayout.EnumPopup(new GUIContent("BTReturn", "이 노드는 무조건 이걸 리턴"), m_eReturnValue, m_guiLayout);
                    EditorGUILayout.EndVertical();
                }
                public override Return Evaluate()
                {
                    if(Return.Evaluate != m_eState)
                    {
                        return m_eState;
                    }
                    m_eReturn = m_delBattleEvent.Invoke();
                    if(Return.Running == m_eReturn)
                    {
                        return m_eReturn;
                    }
                    else
                    {
                        return ChangeStatus(Return.Evaluate, m_eReturn);
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAAllEvent clone = (VSBAAllEvent)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBAAllEvent>();
                

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBAAllEvent clone = (VSBAAllEvent)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }

                protected override void SetBATag()
                {
                    m_eNodeTag = Enum.NodeTag.Act;
                }
            }

        }
    }
}
