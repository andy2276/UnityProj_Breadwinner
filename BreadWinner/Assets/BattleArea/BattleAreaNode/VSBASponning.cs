﻿using System.Collections;
using System.Collections.Generic;
using BT.Virtual;
using UnityEngine;
using UnityEditor;
namespace BT
{
    namespace BA
    {
        namespace Spon
        {
            public class VSBASponning: Virtual.VSBASpon
            {
                [SerializeField] float m_fDelayTime = 0;
                float m_fCurTime = 0;

                public VSBASponning()
                {
                    m_wndNodeRect.width = 200;
                    m_wndNodeRect.height = 200;
                }

                public override VSBTNode CopyNewNode(VSBTNode _parent = null)
                {
                    VSBASponning clone = (VSBASponning)MakeNode(this);
                    DefaultCopy(clone);
                    DefaultCopySpon(clone);
                    clone.m_parent = _parent;
                    clone.m_fDelayTime = m_fDelayTime;
                    return clone;
                }

                public override VSBTNode CopyNode(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBASponning clone = (VSBASponning)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override void DrawWindow()
                {
                    NodeWindow_Default();
                    NodeWindow_DefaultBATaskWnd();
                    NodeWindow_DefaultBASponWnd();
                    EditorGUILayout.BeginVertical();
                    m_fDelayTime = EditorGUILayout.FloatField("DelayTime", m_fDelayTime, GUILayout.Width(200));
                    EditorGUILayout.EndVertical();
                }

                public override Return Evaluate()
                {
                    if(BT.Return.Evaluate != m_eState)
                    {
                        return m_eState;
                    }
                    if(IsSponCntPossible())
                    {
                        if(m_fCurTime < m_fDelayTime)
                        {
                            m_fCurTime += Time.deltaTime;
                            return Return.Running;
                        }
                        else
                        {
                            SponMonster();
                            return Return.Running;
                        }
                    }
                    else
                    {
                        return Return.Success;
                    }
                }

                public override VSBTNode LoadBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBASponning clone = (VSBASponning)CopyNewNode(_parent);
                    _list.Add(clone);
                    return clone;
                }

                public override VSBTNode MakeNode(object e) => CreateInstance<VSBASponning>();

                public override VSBTNode SaveBT(List<VSBTNode> _list, VSBTNode _parent = null)
                {
                    VSBASponning clone = (VSBASponning)CopyNewNode(_parent);
                    _list.Add(clone);
                    DefaultTastSave(clone, _parent);
                    return clone;
                }
            }

        }
    }
}
