﻿using System.Collections;
using System.Collections.Generic;
using BT;
using UnityEngine;

public class BattleArea : BreadWinner
{
    [SerializeField] private BattleAreaData m_areaData = null;
    [SerializeField] private GameObject m_startPos = null;
    private List<GameObject> m_lstSponner = new List<GameObject>();
    private BT.Virtual.VSBTNode m_BARoot = null;
    private List<BT.Virtual.VSBTNode> m_lstNodes;
    [SerializeField] private int m_nDeathMonsterCnt = 0;
    
    private List<GameObject> m_lstPlayMonster = new List<GameObject>();
    private BT.Return m_eResult = BT.Return.Evaluate;
    private bool m_bIsEventEnd = false;


    private void Awake()
    {
        m_lstNodes = new List<BT.Virtual.VSBTNode>();
    }
    
    public BattleAreaData AreaData { get => m_areaData; set => m_areaData = value; }
    public Return Result { get => m_eResult; }
    public List<GameObject> ListPlayMonster { get => m_lstPlayMonster; }
    public GameObject StartPos { get => m_startPos; set => m_startPos = value; }

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }
    public void Init()
    {
        Debug.Log("init start");
        if (m_areaData != null && m_areaData.BAAsset != null)
        {
            m_BARoot = BT.VSBTEditor.LoadBTAsset(m_areaData.BAAsset, m_lstNodes);
            foreach (var n in m_lstNodes)
            {
                n.BattleArea = this;
                n.BTObj = gameObject;   //  필드일거임
            }
        }
        Debug.Log("init end");
    }
    GameObject GetSponPoint(int _point)
    {
        int cnt = m_areaData.lstSponPoint.Count;
        if (cnt == 0)
        {
            return null;
        }
        else if(cnt > _point)
        {
            return m_areaData.lstSponPoint[_point];
        }
        else
        {
            return m_areaData.lstSponPoint[_point - cnt];
        }

    }
    public void RegistPlayMonster(BT.Enum.MonsterType _type,int _sponPoint)
    {
        Object pref = BattleAreaMgr.Inst.GetMonsterPrefab(_type);
        if(pref == null)
        {
            Debug.LogError("Prefab non!!");
            return;
        }
        AddPlayMonster(pref, _sponPoint);
    }
    void CreateSponner()
    {
        for(int i = 0; i < m_areaData.lstSponPoint.Count;++i)
        {
            m_lstSponner.Add(Instantiate(m_areaData.lstSponPoint[i]));
        }
    }

    void AddPlayMonster(Object _obj,int _sponPoint)
    {
        //
        GameObject spon = GetSponPoint(_sponPoint);
        GameObject mop = Instantiate(_obj, spon.transform.position, spon.transform.rotation) as GameObject;
        BehaviorAI ai = mop.GetComponent<BehaviorAI>();
        if(m_areaData.LstLevelScale.Count != 0)
        {
            int scale = BattleAreaMgr.Inst.AreaLevelScale;
            if (scale < m_areaData.LstLevelScale.Count)
            {
                ai.AddLevel(m_areaData.LstLevelScale[ scale]);
            }
            else
            {
                scale = m_areaData.LstLevelScale.Count - 1;
                ai.AddLevel(m_areaData.LstLevelScale[scale]);
            }
        }
        ai.DefaultInit();
        m_lstPlayMonster.Add(mop);
        return;
    }
    
    private void SortPlayMonster()
    {
        int cnt = m_lstPlayMonster.Count;
        List<GameObject> targetObj = new List<GameObject>();

        for (int i = cnt - 1; i >= 0; --i)
        {
            BehaviorAI ai = m_lstPlayMonster[i].GetComponent<BehaviorAI>();
            if (ai.Death)
            {
                targetObj.Add(m_lstPlayMonster[i]);
                m_lstPlayMonster.RemoveAt(i);
            }
        }
        if (cnt == 0)
        {
            return;
        }
        ClearListMonster(targetObj);
    }
    private void ClearListMonster(List<GameObject> _lst)
    {
        int cnt = _lst.Count;
        GameObject target = null;
        for (int i = 0; i < cnt; ++i)
        {
            target = _lst[i];
            _lst[i] = null;
            Destroy(target);
            target = null;
            //
            ++m_nDeathMonsterCnt;
        }
        _lst.Clear();
    }
    public void ClearBattleArea()
    {
        ClearListMonster(m_lstPlayMonster);
        BT.Virtual.VSBTNode.ClearNodeList(m_lstNodes);
        m_BARoot = null;
    }

    // Update is called once per frame
    private void Update()
    {
        if (m_bIsEventEnd) return;
        m_eResult = m_BARoot.Evaluate();
        switch (m_eResult)
        {
            case Return.Evaluate:
                break;
            case Return.Success:
            case Return.Fail:
                m_bIsEventEnd = true;
                ClearListMonster(m_lstPlayMonster);
                break;
            case Return.Running:
                SortPlayMonster();
                m_bIsEventEnd = false;
                break;
           
        }
    }
    public void DeleteSponner()
    {
        GameObject target = null;
        for(int i =0; i < m_lstSponner.Count;++i)
        {
            target = m_lstSponner[i];
            m_lstSponner[i] = null;
            Destroy(target);
            target = null;
        }
    }
  
}
