﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DamageSystem
{
    public class Attack_Far : Attacker
    {
        [SerializeField] GameObject m_projectlePref = null;
        [SerializeField] AIStatus m_projectleData = null;
        //   [SerializeField] int m_nOnceShootCnt = 0;

        private void Start()
        {
            TriggerGet();
            m_collider.enabled = false;
        }
        public void Shoot(Vector3 _target,Vector3 _pos, Quaternion _dir)
        {
            GameObject obj = null;

            obj = Instantiate(m_projectlePref, _pos,_dir ) as GameObject;
            obj.GetComponent<Attack_Projectle>().SetProjectleData(m_projectleData.CloneStatus());
            obj.GetComponent<Attack_Projectle>().TargetPos = _target;
            
        }


        public override void AttackerStart()
        {
            TrrigerEnble();
        }


        public override void AttackOn()
        {
            Vector3 targetPos = transform.root.GetComponent<BehaviorAI>().Target.transform.position;
            Vector3 dir = (targetPos - gameObject.transform.position).normalized;
            Shoot(targetPos,gameObject.transform.position, Quaternion.LookRotation(dir));
        }

        public override void AttackerEnd()
        {
            TrrigerDisable();
        }
        
        
    }

}
