﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DamageSystem
{
    namespace Enum
    {
        public enum Timing
        {
            AttackStart, AttackOn, AttackEnd
        }
    }
    public class HitEvent : MonoBehaviour
    {
        [SerializeField] float m_fLifeTime = 0;
    //    [SerializeField] Color m_colorOffset = new Color(1, 1, 1, 1);
        float m_fCurTime = 0;
        private void OnDisable()
        {
            Destroy(gameObject);
        }

        void Update()
        {
            if(m_fCurTime < m_fLifeTime)
            {
                m_fCurTime += Time.deltaTime; ;

            }
            else
            {
                m_fCurTime = 0;
                Destroy(gameObject);
            }
        }
    }

}

