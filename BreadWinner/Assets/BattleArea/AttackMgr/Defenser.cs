﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DamageSystem
{
    [System.Serializable]
    public class DamageInput
    {
        public float m_fDamage = 0;
        public Vector3 m_v3AttackDir = new Vector3();

        public void ResetData()
        {
            m_fDamage = 0;
            m_v3AttackDir = Vector3.zero;
        }
    }
  
    public class Defenser : BreadWinner
    {
        [SerializeField] float m_fDamageScale = 1;
        [SerializeField] float m_fShiledScale = 0;
        public delegate void DamagedEvent(DamageInput _data);
        private DamagedEvent m_delDamaged = null;
        private DamageInput m_damageInput = new DamageInput();
        public DamagedEvent DamagedFunc { get => m_delDamaged; set => m_delDamaged = value; }
        // Update is called once per frame
        
        private void Start()
        {
           
            CopyState();
        }

        public virtual void DefenceEnter(Attacker _attacker)
        {
            string a = _attacker.tag;
            string d = gameObject.tag;
            //  
            float dmg = (_attacker.Status.AiDamage * m_fDamageScale) - (Status.AiDamage * m_fShiledScale);
            
            m_damageInput.m_fDamage = (dmg >= 0) ? dmg : 0;
            m_delDamaged?.Invoke(m_damageInput);
            m_damageInput.ResetData();

        }
        

    }
}

