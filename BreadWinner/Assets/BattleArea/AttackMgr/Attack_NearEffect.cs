﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DamageSystem
{
    namespace Enum
    {
        public enum TriggerRange
        {
            StartToEnd,StartToOn,OnToEnd
        }
        public enum TirrgerTiming
        {
            Start,On,End
        }
    }


    public class Attack_NearEffect : Attacker
    {
        [SerializeField] Enum.TriggerRange m_triggerRange = Enum.TriggerRange.StartToEnd;
        [SerializeField] GameObject m_startEffect = null;
        [SerializeField] GameObject m_onEffect = null;
        [SerializeField] GameObject m_endEffect = null;
        [SerializeField] GameObject m_hitEffect = null;
        GameObject m_tempEvent = null;

        void Start()
        {
            CopyState();
            TriggerGet();
            m_collider.enabled = false;
            if (m_hitEffect != null)
            {
                m_hitEffect.SetActive(false);
            }
        }
      
        void TriggerOnOff(Enum.TirrgerTiming _timing)
        {
            switch (m_triggerRange)
            {
                case Enum.TriggerRange.StartToEnd:
                    if(Enum.TirrgerTiming.Start == _timing)
                    {
                        TrrigerEnble();
                    }
                    else if(Enum.TirrgerTiming.End == _timing)
                    {
                        TrrigerDisable();
                    }
                    break;
                case Enum.TriggerRange.StartToOn:
                    if (Enum.TirrgerTiming.Start == _timing)
                    {
                        TrrigerEnble();
                    }
                    else if (Enum.TirrgerTiming.On == _timing)
                    {
                        TrrigerDisable();
                    }
                    break;
                case Enum.TriggerRange.OnToEnd:
                    if (Enum.TirrgerTiming.On == _timing)
                    {
                        TrrigerEnble();
                    }
                    else if (Enum.TirrgerTiming.End == _timing)
                    {
                        TrrigerDisable();
                    }
                    break;
                default:
                    break;
            }
        }
        void OnEvent(Vector3 _pos,GameObject _hitEvent)
        {
            if (_hitEvent != null)
            {
                m_tempEvent = Instantiate(_hitEvent, _pos, Quaternion.identity) as GameObject;
                m_tempEvent.SetActive(true);
                m_tempEvent = null;
            }
        }

        public override void AttackerStart()
        {
            TriggerOnOff(Enum.TirrgerTiming.Start);
            OnEvent(gameObject.transform.position, m_startEffect);

        }
        public override void AttackOn()
        {
            TriggerOnOff(Enum.TirrgerTiming.On);
            OnEvent(gameObject.transform.position, m_onEffect);
        }
        public override void AttackerEnd()
        {
            TriggerOnOff(Enum.TirrgerTiming.End);
            OnEvent(gameObject.transform.position, m_endEffect);
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(m_targetTag))
            {
                if (m_hitEffect != null)
                {
                    OnEvent(other.ClosestPoint(gameObject.transform.position), m_hitEffect);
                }
                // defence
                Defenser defence = other.gameObject.GetComponent<Defenser>();
                if (defence != null)
                {
                    defence.DefenceEnter(this);
                }
            }

        }
    }

}
