﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DamageSystem
{
    public class Attack_Projectle : Attacker
    {
        [SerializeField] GameObject m_hitEvent = null;
        [SerializeField] float m_fLifeTime = 0;
        [SerializeField] Vector2 m_v2StartEndSpeed = Vector2.zero;
        [SerializeField] float m_speedScale = 1;
        private Vector3 m_targetPos = Vector3.zero;
      
        float m_fCurTime = 0;

        private Vector3 v3Dir = new Vector3();
        public Vector3 V3Dir { get => v3Dir; set => v3Dir = value; }
        public Vector3 TargetPos { get => m_targetPos; set => m_targetPos = value; }

        // Start is called before the first frame update
        void Start()
        {
            
            
        }
        public override void AttackerStart()
        {
            TrrigerEnble();
        }
        public override void AttackerEnd()
        {
            TrrigerDisable();
        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag(m_targetTag))
            {
                Defenser defence = other.gameObject.GetComponent<Defenser>();
                if (defence != null)
                {
                    // defence
                    defence.DefenceEnter(this);
                }
                if (m_hitEvent != null)
                {
                   Instantiate(m_hitEvent, other.ClosestPoint(gameObject.transform.position), Quaternion.identity);
                }
                ProjectleEnd();
            }
        }
        
        void ProjectleEnd()
        {
            Destroy(gameObject,Time.deltaTime);
        }
        public void CopyAttack_Projectle(Attack_Projectle _scr)
        {
            g_AiState = _scr.Status.CloneStatus();
        }
        public void SetProjectleData(AIStatus _stat)
        {
            Status = _stat;
            

        }
        private void Update()
        {
            if (m_fCurTime < m_fLifeTime)
            {
                m_fCurTime += Time.deltaTime;
             //   float t = (m_fLifeTime - m_fCurTime) / m_fLifeTime;
                gameObject.transform.position += gameObject.transform.forward * m_speedScale * Status.AiMoveSpeed;
            }
            else
            {
                m_fCurTime = 0;
                ProjectleEnd();
            }

        }
    }

}
