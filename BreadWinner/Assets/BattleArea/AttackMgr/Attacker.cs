﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DamageSystem
{
    public enum AttackType
    {
        NearDist,FarDist,Projectile
    }
   
    public class Attacker : BreadWinner
    {
        //   public delegate void AttackEvent()
        [SerializeField] protected GameObject m_parent = null;
        [SerializeField] protected DamageSystem.AttackType m_eAttackType;
        [SerializeField] protected string m_targetTag = "Player";
        protected Collider m_collider = null;

        private void Awake()
        {
            m_collider = gameObject.GetComponent<Collider>();
        }
       
        public Collider TriggerGet()
        {
            if(m_collider == null)
            {
                m_collider = gameObject.GetComponent<Collider>();
            }
            return m_collider;
        }
        public void TrrigerEnble()
        {
            if (!m_collider.enabled)
            {
                m_collider.enabled = true;
            }
        }
        public void TrrigerDisable()
        {
            if (m_collider.enabled)
            {
                m_collider.enabled = false;
            }
        }

        public virtual void AttackerStart()
        {
            TrrigerEnble();
        }
        public virtual void AttackOn()
        {

        }
        public virtual void AttackerEnd()
        {
            TrrigerDisable();
        }
    }
}
