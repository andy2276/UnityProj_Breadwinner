﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DamageSystem
{
   

    public class Attack_Near : Attacker
    {
        [SerializeField] GameObject m_hitEvent = null;
        GameObject m_tempHitEvent = null;
       
        // Start is called before the first frame update
        void Start()
        {
            CopyState();
            TriggerGet();
            m_collider.enabled = false;
            if (m_hitEvent != null)
            {
                m_hitEvent.SetActive(false);
            }
        }
        void OnHitEvent(Vector3 _closePos)
        {
            if(m_hitEvent != null)
            {
                m_tempHitEvent= Instantiate(m_hitEvent, _closePos, Quaternion.identity)as GameObject;
                m_tempHitEvent.SetActive(true);
                m_tempHitEvent = null;
            }
        }
        public override void AttackerStart()
        {
            ParticleSystem p = gameObject.GetComponent<ParticleSystem>();
            if (p!= null)
            {
                p.Play(true);
            }
            TrrigerEnble();
        }
        public override void AttackerEnd()
        {
            ParticleSystem p = gameObject.GetComponent<ParticleSystem>();
            if (p != null)
            {
                p.Stop(true);
            }
            TrrigerDisable();
        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Monster"))
            {
                Debug.Log("들어와야하는데...");
            }
            if(other.CompareTag(m_targetTag))
            {
                Defenser defence = other.gameObject.GetComponent<Defenser>();
                if (m_hitEvent != null)
                {
                    OnHitEvent(other.ClosestPoint(gameObject.transform.position));
                }
                // defence
                if (defence != null)
                {
                    defence.DefenceEnter(this);
                }
            }
           
        }
        
        

    }

}

