﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BattleAreaInfo
{
    namespace Enum
    {
        public enum CameraFunc
        {
            waitPlayer,LookPlayer,Scenema
        }
    }
    public delegate void CameraFunc();

}

public class BattleCamera : MonoBehaviour
{
    private BattleAreaInfo.Enum.CameraFunc m_eCameraState = BattleAreaInfo.Enum.CameraFunc.waitPlayer;
    private BattleAreaInfo.CameraFunc m_delCameraFunc = null;

    [SerializeField] float m_fOffsetY = 7;
    [SerializeField] float m_fOffsetDist = 10;
    [SerializeField] Vector2 m_v2WheelDown =new Vector2(0.5f,1);
    [SerializeField] Vector2 m_v2WheelX = new Vector2(3, 12);
    [SerializeField] Vector2 m_v2WheelY = new Vector2(5, 9);
    [SerializeField] float m_fNearDist = 2f;
    [SerializeField] float m_fFarDist = 4;
    [SerializeField] float m_fTurnSpeed = 1;
    [SerializeField] float m_fMoveSpeed = 1;
    [SerializeField] float m_fAngle = 45;
    [SerializeField] float m_fAddY = 1;
    
    #region 계산용
    Vector3 m_v3CalculPos = new Vector3(0, 0, 0);
    Vector3 m_v3TargetLook = new Vector3();
    Vector3 m_v3TargetPos = new Vector3();
    Vector3 m_v3CamTargetPos = new Vector3();
    Vector3 m_v3CamTargetDir = new Vector3();
   
    
    #endregion
    GameObject m_lookTarget = null;

    void WaitPlayer()
    {
        m_lookTarget = BattleAreaMgr.Inst.Player;
        if (m_lookTarget != null)
        {
            m_delCameraFunc = LookPlayer;
        }
    }
    public void SetCameraOffset()
    {
        m_delCameraFunc = LookPlayer;
    }
    void LookPlayer()
    {
        m_v3TargetPos = m_lookTarget.transform.position;
        m_v3TargetLook = m_lookTarget.transform.forward;

        m_v3CamTargetPos = m_v3TargetPos + (m_v3TargetLook * m_fOffsetDist * -1);
        m_v3CamTargetPos.y += m_fOffsetY;

    //    float camDist = Vector3.Distance(m_v3CamTargetPos, transform.position);
        float targetDist = Vector3.Distance(m_v3TargetPos, transform.position);
        float time = Time.deltaTime;
        m_v3CamTargetDir = (m_v3CamTargetPos - transform.position).normalized;

        Vector2 mouseWheel = Input.mouseScrollDelta;

        if(mouseWheel.y >0)
        {
            if(m_v2WheelX.x <= m_fOffsetDist )
            {
                m_fOffsetDist -= m_v2WheelDown.x;
            }
            if (m_v2WheelY.x <= m_fOffsetY )
            {
                m_fOffsetY -= m_v2WheelDown.y;
            }

        }
        else if(mouseWheel.y < 0)
        {
            if (m_fOffsetDist <= m_v2WheelX.y)
            {
                m_fOffsetDist += m_v2WheelDown.x;
            }
            if (m_fOffsetY <= m_v2WheelY.y)
            {
                m_fOffsetY += m_v2WheelDown.y;
            }
        }

        if (m_fFarDist < targetDist)
        {
            transform.position += m_v3CamTargetDir * Mathf.Lerp(m_fFarDist, targetDist, time * m_fMoveSpeed) * time;
        }

        float angle = Vector3.Dot(m_v3TargetLook, transform.forward);
        if(angle <m_fAngle)
        {
            Vector3 dir = (m_lookTarget.transform.position - transform.position).normalized;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), time * m_fTurnSpeed);
        }

    }
    public void DisConnectTarget()
    {
        m_lookTarget = null;
        m_delCameraFunc = WaitPlayer;
    }
    // Start is called before the first frame update
    void Start()
    {
        m_fAngle = Mathf.Cos(m_fAngle * Mathf.Deg2Rad);
        m_delCameraFunc = WaitPlayer;

    }
    // Update is called once per frame
    void Update()
    {
        m_delCameraFunc?.Invoke();
    }
}
