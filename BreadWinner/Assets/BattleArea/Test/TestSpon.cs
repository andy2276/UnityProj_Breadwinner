﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpon : MonoBehaviour
{
    [SerializeField] List<Object> monsterPrep = null;

    [SerializeField] Vector3 pos = new Vector3(0, 0, 0);
    [SerializeField] GameObject target = null;
    GameObject obj = null;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 p = pos;
        Vector3 s = new Vector3(3, 0, 3);
        for(int i = 0; i < monsterPrep.Count;++i)
        {
            obj = (GameObject)Instantiate(monsterPrep[i], p +(s * i), Quaternion.identity);
            obj.GetComponent<BehaviorAI>().Target = target;
        }
        
    }
}
