﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextCollider : MonoBehaviour
{
    [SerializeField] Material me;
    bool m_on = false;
    [SerializeField] float m_fHp = 100;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("start");
        me.color = Color.green;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_on)
        {
            me.color = Color.red;
        }
        else
        {
            me.color = Color.green;
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("MonsterWeapon"))
        {
            m_on = true;
            m_fHp -= 10;
            Debug.Log("tri Enter! ");
        }
        

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MonsterWeapon"))
        {
            m_on = false;
            Debug.Log("tri Exit! ");
        }
        
    }
}
